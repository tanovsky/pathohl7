#!/bin/bash

deployEnvironment=productionIPH
appname=patho.war
builddir=target
filename=${builddir}/${appname}

# Generate war file
grails -Dgrails.env=${deployEnvironment} war ${filename}

stty -echo
printf "Repository Password: "
read repositoryPassword
stty echo
printf "\n"

# Upload to file repository
## http://stackoverflow.com/questions/12498304/using-bash-to-display-a-progress-working-indicator
curl -s -u tanovsky:${repositoryPassword} -X POST https://api.bitbucket.org/2.0/repositories/tanovsky/pathohl7/downloads -F files=@${filename} &

pid=$! # Process Id of the previous running command


spin='-\|/'


#printf  "executing ... "

i=0
while kill -0 $pid 2>/dev/null
do
  i=$(( (i+1) %4 ))
  printf "\r${spin:$i:1}"
  sleep .1
done

