package de.uni_heidelberg.iph

class DiagnosticOrder {

   Date dateCreated

   Date lastUpdated

    /* Priority */
    public static final String PRIORITY_ROUTINE = "routine"

    public static final String PRIORITY_URGENT = "urgent"

    public static final List PRIORITY = [PRIORITY_ROUTINE, PRIORITY_URGENT]

    User requesterMedicalDoctor
//    User orderer

    //  To test the regular expression     https://regex101.com/
    // ^[R,S,T,G,N]-\d{5}-\d{2}
    String eNumber = ""

    String block = ""

    String patientName = " "

    static final String TUMOR_TYPE_MESENCHYMAL = "Mesenchymal Tumor"
    static final String TUMOR_TYPE_CARCINOMA = "Carcinoma"
    static final String TUMOR_TYPE_LYMPHOMA = "Lymphoma"
    static final String TUMOR_TYPE_LEUKEMIA = "Leukemia"
    static final String TUMOR_TYPE_OTHER = "Other"

    public static final List TUMOR_TYPE = [
        TUMOR_TYPE_MESENCHYMAL,
        TUMOR_TYPE_CARCINOMA,
        TUMOR_TYPE_LYMPHOMA,
        TUMOR_TYPE_LEUKEMIA,
        TUMOR_TYPE_OTHER
    ]

    String tumorType = TUMOR_TYPE_MESENCHYMAL

//    String sampleAndBlock = " "

    String reason = ""

    String supportingInformation = ""

    String barcode = ""

    /* Status */
    public static final String DRAFT = "draft"

    public static final String REQUESTED = "requested"

    public static final String ACCEPTED = "accepted"

    public static final String SUSPENDED = "suspended"

    /** In case of step non-conformity */
    public static final String PAUSED = "paused"

    public static final String IN_PROGRESS = "in-progress"

    public static final String LABORATORY_PREPARATION = "laboratoryPreparation"

    public static final String REVIEW_LABORATORY_PREPARATION = "reviewLaboratoryPreparation"

    public static final String REVIEW_DIAGNOSTIC_EVALUATION = "reviewDiagnosticEvaluation"

    public static final String REPORT_SIGNED = "reportSigned"

    public static final String REPORT_SENT = "reportSent"

    public static final String COMPLETED = "completed"

//    public static final String NON_CONFORMITY_DETECTED = "nonConformityDetected"

// Not implemented because there is not interest in suspend anything..
// If is not finished sucessfully or the patient dies, it is not paid..
//    public static final String SUSPENDED = "suspended"

    public static final String FAILED = "failed"

    // If the status is FAILED on some attempts, it may be set to REJECTED (not billed, in passnet)
    public static final String REJECTED = "rejected"

    public static final String CANCELLED = "cancelled"

    public static final String NON_CONFORMITY = "nonconformity"

    public static final List STATUS = [
//        DRAFT,
        REQUESTED,
        IN_PROGRESS,
//        ACCEPTED,
//        REVIEW_LABORATORY_PREPARATION,
//        REVIEW_DIAGNOSTIC_EVALUATION,
//        REJECTED,
//        FAILED,
//        REPORT_SIGNED,
//        REPORT_SENT,
        NON_CONFORMITY,
        COMPLETED,
        CANCELLED
    ]

//    //proposed | draft | planned | requested | received | accepted | in-progress | review | completed | cancelled | suspended | rejected | failed
////    String status = "requested"
//    String status = STATUS_REVIEW_LABORATORY_PREPARATION
String status = REQUESTED


//Code	Display	Definition
//routine	Routine	The order has a normal priority .
//urgent	Urgent	The order should be urgently.
//stat	Stat	The order is time-critical.
//asap	ASAP	The order should be acted on as soon as possible.
    String priority = PRIORITY_ROUTINE

    String note = " "

//    static hasMany = [Item: DiagnosticOrderItem]
//    static hasMany = [requestItem: RequestItem]

    static hasMany = [items: DiagnosticOrderItem,
//                      diagnosticOrderStatusEvent: DiagnosticOrderStatusEvent
                      events: DiagnosticOrderStatusEvent
                  ]

    static constraints = {
        tumorType(inList: TUMOR_TYPE)
        barcode(widget: 'textfield', blank: true, nullable: true)
        priority(inList: PRIORITY)
        status(inList: STATUS)
        note(widget: 'textarea', blank: true, nullable: true)
        reason(widget: 'textarea', blank: true, nullable: true)
        supportingInformation(widget: 'textarea', blank: true, nullable: true)
//        requesterMedicalDoctor(nullable: false)
        requesterMedicalDoctor(nullable: true)
//        assistantMedicalDoctor(nullable: false)
//        medicalDoctor(nullable: true)
    }

    static mapping = {
        events cascade:"all-delete-orphan", lazy:false
        items cascade:"all-delete-orphan", lazy:false
    }
}
