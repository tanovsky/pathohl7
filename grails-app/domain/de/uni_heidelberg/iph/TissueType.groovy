package de.uni_heidelberg.iph

class TissueType {

    String code

    String name

    String description

    static constraints = {
        name(inList: ["SolideTumoren", "HematologicTumoren", "SoftTumoren"])
        code()
        description()
    }

    static hasMany = [probe: Probe]
}
