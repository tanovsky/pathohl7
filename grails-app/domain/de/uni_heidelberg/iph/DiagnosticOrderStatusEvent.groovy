package de.uni_heidelberg.iph

class DiagnosticOrderStatusEvent {

    /* Status */
    public static final String REQUESTED = "requested"

    public static final String ACCEPTED = "accepted"

    public static final String SUSPENDED = "suspended"

    /** In case of step non-conformity */
    public static final String PAUSED = "paused"

    public static final String IN_PROGRESS = "in-progress"

    public static final String REVIEW_LABORATORY_PREPARATION = "reviewLaboratoryPreparation"

    public static final String REVIEW_DIAGNOSTIC_EVALUATION = "reviewDiagnosticEvaluation"

    public static final String REPORT_SIGNED = "reportSigned"

    public static final String REPORT_SENT = "reportSent"

    public static final String COMPLETED = "completed"

//    public static final String NON_CONFORMITY_DETECTED = "nonConformityDetected"

// Not implemented because there is not interest in suspend anything..
// If is not finished sucessfully or the patient dies, it is not paid..
//    public static final String SUSPENDED = "suspended"

    public static final String FAILED = "failed"

    // If the status is FAILED on some attempts, it may be set to REJECTED (not billed, in passnet)
    public static final String REJECTED = "rejected"

    public static final String CANCELLED = "cancelled"

//    public static final String NON_CONFORMITY = "nonconformity"

    public static final List STATUS = [
        REQUESTED,
        ACCEPTED,
        REVIEW_LABORATORY_PREPARATION,
        REVIEW_DIAGNOSTIC_EVALUATION,
        REJECTED,
        FAILED,
        REPORT_SIGNED,
        REPORT_SENT,
        COMPLETED,
        CANCELLED
    ]

    //proposed | draft | planned | requested | received | accepted | in-progress | review | completed | cancelled | suspended | rejected | failed
//    String status = "requested"
    String status = REQUESTED

//    String comment

    String description

    String actor
    
    Date dateTime

//    Date date = new Date()

    static belongsTo = [
            diagnosticOrder : DiagnosticOrder
    ]


    static constraints = {
        status(inList: STATUS)
        description(widget: 'textarea', blank: true, nullable: true)
        actor(nullable: true)
        dateTime(nullable: true)
//        date()
    }
}
