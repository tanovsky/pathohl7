package de.uni_heidelberg.iph

import org.grails.databinding.BindingFormat

class DiagnosticOrderItem {

    Date dateCreated

    Date lastUpdated

    Probe probe

    boolean readyForAnalysis = false

    // Fields required by technician
    String laboratoryPreparationComment = ""

    String probeLoteNumber = ""

    @BindingFormat('dd/MM/yyyy')
    Date probeExpirationDate
    //= new Date().format("yyyy-MM-dd")

    // TODO should be Enum
    String change = SPLIT_CHANGE

    long nucleousCount = 0

    long positiveNucleousCount = 0

    double percentageNotCountableNucleous = 0

    double percentageNucleousCountPositive = 0

    long centromerCount = 0

    long locusSpecificProbeCount = 0

    // LocusSpecificProbeCount/CentromerCount * 100
    double ratio = 0

    String result = RESULT_STATUS_INDETERMINED

    String comment = ""

    boolean repeated = false

    static hasMany = [
        uploadedFiles: DiagnosticOrderItemUploadedFile,
        events: DiagnosticOrderItemStatusEvent
    ]

    boolean fotoDocumented = false

    boolean processed = false

    static final String RESULT_STATUS_INDETERMINED = "Indetermined"

    static final String RESULT_STATUS_POSITIVE = "Positive"

    static final String RESULT_STATUS_NEGATIVE = "Negative"

    static List RESULT_STATUS = [RESULT_STATUS_INDETERMINED,
                                 RESULT_STATUS_POSITIVE,
                                 RESULT_STATUS_NEGATIVE]

    /* Split is the same as "Break apart" */
    static final String SPLIT_CHANGE = "Break apart"

    static final String FUSION_CHANGE = "Fusion"

    static final String AMPLIFICATION_CHANGE = "Amplification"

    static final String DELETION_CHANGE = "Deletion"

    static def CHANGE_STATUS = [SPLIT_CHANGE, FUSION_CHANGE, AMPLIFICATION_CHANGE, DELETION_CHANGE]

    static belongsTo = [
            diagnosticOrder : DiagnosticOrder
    ]

    static constraints = {
        laboratoryPreparationComment(widget: 'textfield', blank: true, nullable: true)
        probeLoteNumber(widget: 'textfield', blank: true, nullable: false)
        probeExpirationDate(widget: 'textfield', nullable: true)
        change(inList: CHANGE_STATUS)
        result(inList: RESULT_STATUS)
    }

    static mapping = {
        sort id: "desc"
        events cascade:"all-delete-orphan"
        uploadedFiles cascade:"all-delete-orphan"
    }

    /* TO DELETE */
    String status = "completed"
}
