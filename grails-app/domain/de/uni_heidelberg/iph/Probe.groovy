package de.uni_heidelberg.iph

class Probe {

    public static final String HYBRIDIZATION_PROCEDURE_CISH = "CISH"
    public static final String HYBRIDIZATION_PROCEDURE_FISH = "FISH"
    public static final String HYBRIDIZATION_PROCEDURE_SPLIT_CISH = "split-CISH"
    public static final String HYBRIDIZATION_PROCEDURE_SPLIT_FISH = "split-FISH"
    public static final String HYBRIDIZATION_PROCEDURE_FUSION_FISH = "fusion-FISH"

    String name
    String code
    String description
//    List tissueType = ["Solid Tumoren", "Haemotolgie", "Weichteiltumoren"]
//    String cost

    String procedure
    String note

    Boolean available = true
    Boolean enabled = true

    Gene gene
    TissueType tissueType
    Manufacturer manufacturer

    // Mappings
    static belongsTo = [
            gene : Gene,
            tissueType: TissueType,
            manufacturer: Manufacturer
    ]

//    static def COMMENT = ["Backup bei Problemen mit CISH",
//                      "Backup bei Problemen mit RT-PCR",
//                      "Kopienzahl bei sol. Tumoren",
//                      "Burkitt-Lymphom u.a."]

    static def HIBRIDIZATION_PROCEDURE = [HYBRIDIZATION_PROCEDURE_CISH,
                                          HYBRIDIZATION_PROCEDURE_FISH,
                                          HYBRIDIZATION_PROCEDURE_SPLIT_CISH,
                                          HYBRIDIZATION_PROCEDURE_SPLIT_FISH,
                                          HYBRIDIZATION_PROCEDURE_FUSION_FISH]

    static constraints = {
        name()
        code(nullable: false)
        description(widget: 'textarea')
//        comment(inList: COMMENT)
        procedure(inList: HIBRIDIZATION_PROCEDURE)
        note(widget: 'textarea', nullable: true)
        available()
        enabled()
        gene()
        tissueType(nullable: true)
        manufacturer()
    }

    String toString() {
        return name + " - " + code
    }
}
