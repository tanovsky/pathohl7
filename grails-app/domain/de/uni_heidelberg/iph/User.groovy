package de.uni_heidelberg.iph

class User {

    public static final String REQUESTER_MEDICAL_DOCTOR = "RequesterMedicalDoctor"
    public static final String TECHNICAL_ASSISTANT = "TechnicalAssistant"
    public static final String MEDICAL_DOCTOR = "MedicalDoctor"
    public static final String ASSISTANT_MEDICAL_DOCTOR = "AssistantMedicalDoctor"
    public static final String MANAGER = "Manager"
    public static final String ACCOUNTANT = "Accountant"
    public static final String ADMINISTRATOR = "Administrator"

    String login
    String password
    String role = MEDICAL_DOCTOR

    String name
    String mobile
    String tel
    String email
    String address

    static constraints = {
		login(blank: false, size: 3..20, unique:true)
		password(blank: false, size: 4..20, password:true)
                role(inList:[
                        REQUESTER_MEDICAL_DOCTOR,
                        TECHNICAL_ASSISTANT,
                        MEDICAL_DOCTOR,
                        ASSISTANT_MEDICAL_DOCTOR,
                        MANAGER,
                        ACCOUNTANT,
                        ADMINISTRATOR
                    ])
		name()
		mobile()
		tel(nullable: true)
                email(email:true, blank:false)
		address(nullable: true)
    }

    String toString(){
            name + " (" +role + ")"
    }

   static mapping = {
      table 'users'
      password column: '`password`'
   }
}
