package de.uni_heidelberg.iph

class Gene {
    
    String name
    String alias
    String locus
//    String description

    static hasMany = [probe: Probe]

    static constraints = {
        name blank: false, nullable: false
        alias blank: true, nullable: true
        locus blank: false, nullable: false
//        description(widget:'textarea')
    }
}
