package de.uni_heidelberg.iph

class DiagnosticOrderItemUploadedFile {

    String name = " "
    String filepath = " "
    String fileType = " "

    static belongsTo = [
            diagnosticOrderItem : DiagnosticOrderItem
    ]

    static constraints = {
    }
}
