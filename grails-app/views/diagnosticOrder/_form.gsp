<%@ page import="de.uni_heidelberg.iph.DiagnosticOrder" %>

<script>



function jump(ev, next) {
    if ((ev.keyCode == 13)) {
        document.getElementById(next).value = barcodeCache.replace("<", "");
        document.getElementById(next).focus();
        ev.target.value = barcodeCache;
        return false;
    } else {
        barcodeCache = barcodeCache + String.fromCharCode(ev.keyCode);
        return true;
    }

};

var barcodeCache;

function loadImage(ev) {
    if ((ev.keyCode == 13)) {
        alert("Image is loaded");
        ev.target.value = barcodeCache;
        document.getElementById('eNumber').focus();


//        document.getElementById('barcodeform').focus();
//        document.barcodeform.eNumber.focus();
        return false;
    } else {

        return true;
    }
}

</script>

<script>
function clean(el){
//alert("opsofds");
	var textfield = document.getElementById(el);
	var regex = /</gi;
//        onkeyup="clean(this.id)" onkeydown="clean(this.id)"
/**/
//        alert(textfield.value.search(regex));
	if(textfield.value.search(regex) > -1) {
		textfield.value = textfield.value.replace(regex, "");
        }

//        textfield.value = textfield.value.replace(regex, "");
};

$("#eNumber").bind("keypress", function (event) {
alert("ola");
//    var regex = new RegExp("^[a-zA-Z0-9]+$");
//    var regex = new RegExp("^[<]");
    var regex = /</gi;
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});

</script>
<!--
<div class="fieldcontain ${hasErrors(bean: diagnosticOrderInstance, field: 'eNumber', 'error')} required">
	<label for="barcode">
		<g:message code="diagnosticOrder.barcode.label" default="Barcode" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField autofocus readonly="readonly" onfocus="this.focus();this.select();barcodeCache='';this.value=barcodeCache;" onkeypress="return jump(event, 'eNumber');" id="barcode" name="barcode" size="40" required="" value="${diagnosticOrderInstance?.barcode}" pattern="^<*"/>
</div>
-->

<!-- to test.. println "valid : " + ("R-12345-12" =~ "^[R,S,T,G,N]-\\d{5}-\\d{2}").matches()​ -->
<div class="fieldcontain ${hasErrors(bean: diagnosticOrderInstance, field: 'eNumber', 'error')} required">
	<label for="eNumber">
		<g:message code="default.eNumber" default="eNumber" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField id="eNumber" name="eNumber" size="40" required="" value="${diagnosticOrderInstance?.eNumber}" pattern="^[E,R,S,T,G,N]/\\d{2}/\\d{6}" style="text-transform: uppercase;"/>
</div>

<div class="fieldcontain ${hasErrors(bean: diagnosticOrderInstance, field: 'patientName', 'error')} required">
	<label for="patientName">
		<g:message code="default.patient.name" default="PatientName" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="patientName" size="40" required="" value="${diagnosticOrderInstance?.patientName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: diagnosticOrderInstance, field: 'block', 'error')} required">
	<label for="block">
		<g:message code="default.block" default="Block" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="block" size="40" required="" value="${diagnosticOrderInstance?.block}"/>

</div>


<div class="fieldcontain ${hasErrors(bean: diagnosticOrderInstance, field: 'tumorType', 'error')} required">
	<label for="tumorType">
		<g:message code="default.tumor.type" default="tumorType" />
		<span class="required-indicator">*</span>
	</label>
        <g:select name="tumorType" from="${diagnosticOrderInstance.constraints.tumorType.inList}" required="" value="${diagnosticOrderInstance?.tumorType}" valueMessagePrefix="diagnosticOrder.tumorType"/>
</div>

<div class="fieldcontain ${hasErrors(bean: diagnosticOrderInstance, field: 'priority', 'error')} required">
	<label for="priority">
		<g:message code="default.priority" default="Priority" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="priority" from="${diagnosticOrderInstance.constraints.priority.inList}" required="" value="${diagnosticOrderInstance?.priority}" valueMessagePrefix="diagnosticOrder.priority"/>

</div>

<!--

<div class="fieldcontain ${hasErrors(bean: diagnosticOrderInstance, field: 'note', 'error')} required">
	<label for="note">
		<g:message code="diagnosticOrder.note.label" default="Note" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="note" required="" value="${diagnosticOrderInstance?.note}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: diagnosticOrderInstance, field: 'reason', 'error')} required">
	<label for="reason">
		<g:message code="diagnosticOrder.reason.label" default="Reason" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="reason" cols="40" rows="5" required="" value="${diagnosticOrderInstance?.reason}"/>

</div>

-->

<div class="fieldcontain ${hasErrors(bean: diagnosticOrderInstance, field: 'supportingInformation', 'error')} required">
	<label for="supportingInformation">
		<g:message code="default.provisional.diagnostic" default="Provision Diagnosis" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="supportingInformation" cols="40" rows="5" required="" value="${diagnosticOrderInstance?.supportingInformation}"/>

</div>


<!--
<div class="fieldcontain ${hasErrors(bean: diagnosticOrderInstance, field: 'item', 'error')} ">
	<label for="item">
		<g:message code="diagnosticOrder.item.label" default="Item" />

	</label>

<ul class="one-to-many">
<g:each in="${diagnosticOrderInstance?.items?}" var="i">
    <li><g:link controller="diagnosticOrderItem" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="diagnosticOrderItem" action="create" params="['diagnosticOrder.id': diagnosticOrderInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'diagnosticOrderItem.label', default: 'DiagnosticOrderItem')])}</g:link>
</li>
</ul>


</div>

-->

