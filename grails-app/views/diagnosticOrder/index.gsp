<%@ page import="de.uni_heidelberg.iph.DiagnosticOrder" %>
<%@ page import="de.uni_heidelberg.iph.User" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main">
        <g:set var="entityName" value="${message(code: 'diagnosticOrder.label', default: 'DiagnosticOrder')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-diagnosticOrder" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <g:ifUserHasRole roles="${User.REQUESTER_MEDICAL_DOCTOR}">
                <li><g:link class="create" action="create"><g:message code="default.diagnostic.order.create" default="Create Diagnostic Order" /></g:link></li>
                </g:ifUserHasRole>
            </ul>
        </div>
        <div style="padding-left: 100px;padding-right: 100px;" id="list-diagnosticOrder" class="content scaffold-list" role="main">
           <h1>${message(code: 'default.diagnostic.order.list', default: 'Diagnostic Orders list')}</h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
                <table>
                    <thead>
                        <tr>
                            <g:sortableColumn property="eNumber" title="${message(code: 'default.eNumber', default: 'eNumber')}" />
                            <g:sortableColumn property="eNumber" title="${message(code: 'default.block', default: 'Block')}" />
                            <g:sortableColumn property="priority" title="${message(code: 'default.priority', default: 'Priority')}" />
                            <g:sortableColumn property="dateCreated" title="${message(code: 'default.date.created', default: 'Date Created')}" />
                            <g:sortableColumn property="lastUpdated" title="${message(code: 'default.date.last.update', default: 'Last Updated')}" />
<!--
                                                <g:sortableColumn property="note" title="${message(code: 'diagnosticOrder.note.label', default: 'Note')}" />

                                                <g:sortableColumn property="reason" title="${message(code: 'diagnosticOrder.reason.label', default: 'Reason')}" />

                                                <g:sortableColumn property="supportingInformation" title="${message(code: 'diagnosticOrder.supportingInformation.label', default: 'Supporting Information')}" />
-->
                            <g:sortableColumn property="supportingInformation" title="${message(code: 'default.provisional.diagnostic', default: 'Provision Diagnosis')}" />
                            <th><g:message code="diagnosticOrder.requester.label" default="Requester" /></th>
                            <g:sortableColumn property="status" title="${message(code: 'default.status', default: 'Status')}" />
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${diagnosticOrderInstanceList}" status="i" var="diagnosticOrderInstance">
                            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}, clickable-row" data-href="${createLink(controller: "diagnosticOrder", action:'items', params: [id: diagnosticOrderInstance.id])}">
                                <td>${fieldValue(bean: diagnosticOrderInstance, field: "eNumber")}</td>
                                <td>${fieldValue(bean: diagnosticOrderInstance, field: "block")}</td>
                                <td>${fieldValue(bean: diagnosticOrderInstance, field: "priority")}</td>
                                <td><g:formatDate date="${diagnosticOrderInstance.dateCreated}" type="datetime" style="LONG" timeStyle="SHORT"/></td>
                                <td><g:formatDate date="${diagnosticOrderInstance.lastUpdated}" type="datetime" style="LONG" timeStyle="SHORT"/></td>
<!--
                                <td>${fieldValue(bean: diagnosticOrderInstance, field: "note")}</td>
                                <td>${fieldValue(bean: diagnosticOrderInstance, field: "supportingInformation")}</td>
-->
                                <td>${fieldValue(bean: diagnosticOrderInstance, field: "supportingInformation")}</td>
                                <td>${fieldValue(bean: diagnosticOrderInstance, field: "requesterMedicalDoctor.login")}</td>
                                <td><g:message code="default.fish.status.${diagnosticOrderInstance?.status}" /></td>
                                
                            </tr>
                        </g:each>
                    </tbody>
                </table>
            <div class="pagination">
                <g:paginate total="${diagnosticOrderInstanceCount ?: 0}" />
            </div>
            <div style="min-height: 400px;min-height: 400px;height: 400px;"></div>
        </div>
    </body>
</html>
