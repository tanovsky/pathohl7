
<%@ page import="de.uni_heidelberg.iph.DiagnosticOrder" %>
<%@ page import="de.uni_heidelberg.iph.User" %>
<!DOCTYPE html>
<html>
	<head>

  <script>
    function showhide(caller, myObjectId) {
        var div = document.getElementById(myObjectId);
        if (div.style.display !== "none") {
            caller.value = "Show comment";
            div.style.display = "none";
        } else {
            caller.value = "Hide comment";
            div.style.display = "block";
        }
    }

    function percentageNucleousCountPositive(id){
        var positiveNucleousCount = document.getElementById("positiveNucleousCount"+ "_" + id);
        var nucleousCount = document.getElementById("nucleousCount"+ "_" + id);
        var percentageNucleousCountPositive = document.getElementById("percentageNucleousCountPositive"+ "_" + id);

        calculatePercentage(positiveNucleousCount, nucleousCount, percentageNucleousCountPositive);
/*
        if (nucleousCount.value > 0) {
            var percentageNucleousCountPositiveValue = (positiveNucleousCount.value/nucleousCount.value) * 100;
            percentageNucleousCountPositive.value = percentageNucleousCountPositiveValue;
        } else {
            positiveNucleousCount.value = 0;
            nucleousCount.value = 0;
            percentageNucleousCountPositive.value = 0;
        }
*/
    }

    function ratioCentromerProbeCount(id){
        var centromerCount = document.getElementById("centromerCount"+ "_" + id);
        var locusSpecificProbeCount = document.getElementById("locusSpecificProbeCount"+ "_" + id);
        var ratio = document.getElementById("ratio"+ "_" + id);

        calculatePercentage(locusSpecificProbeCount, centromerCount, ratio);

        /*
        if (centromerCount.value > 0) {
            var ratioValue = (locusSpecificProbeCount.value/centromerCount.value) * 100;
            ratio.value = ratioValue;
        } else {
            centromerCount.value = 0;
            locusSpecificProbeCount.value = 0;
            ratio.value = "0";
        }
        */
    }


    function calculatePercentage(numeratorObject, denominatorObject, resultObject) {
        if (denominatorObject.value > 0) {
            var resultValue = (numeratorObject.value/denominatorObject.value) * 100;
            resultObject.value = resultValue;
        } else {
            numeratorObject.value = 0;
            denominatorObject.value = 0;
            resultObject.value = 0;
        }
    }








    function statusChange(id) {
//        alert("statusChange called for id : " + id);
        var divAmplificationElement = document.getElementById("amplification_" + id);
//        alert("aki0");
        var divNoAmplificationElement = document.getElementById("noAmplification_" + id);
//alert("aki1");

        var statusElement = document.getElementById("change_" + id);

/*
        if (statusElement.value == "Amplification") {
            divAmplificationElement.style.display = "block";
            divNoAmplificationElement.style.display = "none";
        } else {
            divAmplificationElement.style.display = "none";
            divNoAmplificationElement.style.display = "block";
        }
*/
        var isAmplificationSelected = (statusElement.value == "Amplification");
        divAmplificationElement.style.display =  isAmplificationSelected ? "block":"none";
        divNoAmplificationElement.style.display = isAmplificationSelected ? "none":"block";

//        var centromerCount = document.getElementById("centromerCount"+ "_" + id);
//        var locusSpecificProbeCount = document.getElementById("locusSpecificProbeCount"+ "_" + id);
//        var ratio = document.getElementById("ratio"+ "_" + id);
//        var positiveNucleousCount = document.getElementById("positiveNucleousCount"+ "_" + id);
//        var nucleousCount = document.getElementById("nucleousCount"+ "_" + id);
//        var percentageNucleousCountPositive = document.getElementById("percentageNucleousCountPositive"+ "_" + id);
//
//        if ()
//        centromerCount = 0
//        locusSpecificProbeCount = 0
//        ratio = 0
//        positiveNucleousCount = 0
//        nucleousCount = 0
//        percentageNucleousCountPositive = 0
    }

    function updateAllStatus() {
        var listOfGenes = ${diagnosticOrderInstance?.items?.collect{it.id}};
        for (i in listOfGenes){
            statusChange(listOfGenes[i]);
        }
    }

  </script>

<style>
  .thumb {
    height: 75px;
    border: 1px solid #000;
    margin: 10px 5px 0 0;
  }

.fish-image { width:100px;height:100px;float:left;position:relative;background-size:100px 100px;cursor: pointer;}

.images-container { overflow:auto; }
//a.delete { display:none;position:absolute;top:0;right:0;width:15px;height:15px;text-indent:-999px; background-image:url(http://sanmarco.bg/eshop/themes/prestashop/img/icon/delete.png)}
a.delete { display:none;position:absolute;top:0;right:0;width:15px;height:15px;text-indent:-999px; background-image:url(${createLinkTo(dir:'images',file:'delete.png')})}



.fish-image:hover a.delete { display:block; }

.test {
    float:left;
    position:relative;
    border: transparent 5px;
    border-style: solid;
}

.image-upload > input
{
    display: none;
}

.image-upload img
{
    width: 30px;
    cursor: pointer;
}

tr {
//            pointer-events : none;
}


tbody tr:hover {
          background-color: inherit!important;   /* or whatever color you want */
}

</style>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'diagnosticOrder.label', default: 'DiagnosticOrder')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body onload="updateAllStatus();">

		<a href="#show-diagnosticOrder" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-diagnosticOrder" class="content scaffold-show" role="main">

			<g:form name="uau" id="uau" url="[resource:diagnosticOrderInstance, action:'update']" method="PUT" >

<!--
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
-->
			<h1>Diagnostic Order</h1>

                        <div style="float: right; text-align:right;padding-right:50px;padding-top:40px;">
                            <g:if test="${diagnosticOrderInstance?.barcode}">
                                <img src="${createLink(controller:'diagnosticOrder', action:'barcode')}?data=${diagnosticOrderInstance?.barcode}&width=300&height=75"/>
                                <div style="text-align:bottom;padding-right:20px;">
                                    ${diagnosticOrderInstance?.barcode}
                                </div>
                            </g:if>
                            <g:else>
                                                            <div style="text-align:bottom;padding-right:20px;">
                                No Barcode available
                            </div>
                                
                            </g:else>
                        </div>

<div style="padding-left: 20px;width: 700px;">   <!-- fields diag order -->

			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list diagnosticOrder">
				<g:if test="${diagnosticOrderInstance?.requesterMedicalDoctor}">
                                    <b>
				<li class="fieldcontain">
					<span id="medicalDoctor-label" class="property-label"><g:message code="diagnosticOrder.medicalDoctor.label" default="RequesterMedicalDoctor" /></span>
                                        <span class="property-value" aria-labelledby="medicalDoctor-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="requesterMedicalDoctor.name"/></span>
				</li>
                                </b>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.eNumber}">
                                    <b>
				<li class="fieldcontain">
					<span id="eNumber-label" class="property-label"><g:message code="diagnosticOrder.eNumber.label" default="eNumber" /></span>
                                        <span class="property-value" aria-labelledby="eNumber-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="eNumber"/></span>
				</li>
                                </b>
				</g:if>
				<g:if test="${diagnosticOrderInstance?.block}">
                                    <b>
				<li class="fieldcontain">
					<span id="block-label" class="property-label"><g:message code="diagnosticOrder.block.label" default="Block" /></span>
                                        <span class="property-value" aria-labelledby="block-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="block"/></span>
				</li>
                                </b>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.priority}">
                                    <b>
				<li class="fieldcontain">
					<span id="priority-label" class="property-label"><g:message code="diagnosticOrder.priority.label" default="Priority" /></span>

						<span class="property-value" aria-labelledby="priority-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="priority"/></span>
				</li>
                                </b>
				</g:if>
                                
                                <g:if test="${diagnosticOrderInstance?.status}">
                                    <b>
				<li class="fieldcontain">
					<span id="priority-label" class="property-label"><g:message code="diagnosticOrder.status.label" default="Status" /></span>

						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="status"/></span>
				</li>
                                </b>
				</g:if>
<!--
				<g:if test="${diagnosticOrderInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="diagnosticOrder.status.label" default="Status" /></span>

						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="status"/></span>

				</li>
				</g:if>
-->

<!--
				<g:if test="${diagnosticOrderInstance?.note}">
				<li class="fieldcontain">
					<span id="note-label" class="property-label"><g:message code="diagnosticOrder.note.label" default="Note" /></span>

						<span class="property-value" aria-labelledby="note-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="note"/></span>

				</li>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.reason}">
				<li class="fieldcontain">
					<span id="reason-label" class="property-label"><g:message code="diagnosticOrder.reason.label" default="Reason" /></span>

						<span class="property-value" aria-labelledby="reason-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="reason"/></span>

				</li>
				</g:if>

-->
				<g:if test="${diagnosticOrderInstance?.supportingInformation}">
                                    <b>
				<li class="fieldcontain">
					<span id="supportingInformation-label" class="property-label"><g:message code="diagnosticOrder.supportingInformation.label" default="Supporting Information" /></span>
<!--
						<span class="property-value" aria-labelledby="supportingInformation-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="supportingInformation"/></span>
-->
                                                <span class="property-value" aria-labelledby="supportingInformation-label"><g:textArea readonly="readonly" name="supportingInformation" style="width:100%; height:50%" value="${fieldValue(bean: diagnosticOrderInstance, field: "supportingInformation")}"/></span>



				</li>
                                </b>
				</g:if>
<!--
				<g:if test="${diagnosticOrderInstance?.items}">
				<li class="fieldcontain">
					<span id="item-label" class="property-label"><g:message code="diagnosticOrder.item.label" default="Item" /></span>

						<g:each in="${diagnosticOrderInstance.items}" var="i">
						<span class="property-value" aria-labelledby="item-label"><g:link controller="diagnosticOrderItem" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></span>
						</g:each>

				</li>
				</g:if>
-->



</div>   <!-- fields diag order -->

<%
if (diagnosticOrderInstance?.status in [ "requested", "in-progress"]) {
%>
				<g:if test="${diagnosticOrderInstance?.items}">
				<li class="fieldcontain">
					<span id="item-label" class="property-label"><g:message code="diagnosticOrder.item.label" default="Item" /></span>

						<g:each in="${diagnosticOrderInstance.item.sort { it.id }}" var="i">
						<span class="property-value" aria-labelledby="item-label">
                                                    <g:link controller="gene" action="show" id="${i.id}">${i.probe.gene.name?.encodeAsHTML()} - ${i.probe.name?.encodeAsHTML()} (${i.probe.procedure?.encodeAsHTML()})</g:link>
                                                </span>
						</g:each>

				</li>
				</g:if>

<%
} else {
%>

<br/><br/>


    <g:each in="${diagnosticOrderInstance?.items?.sort { it.id }}" status="i" var="personInstance">

<!--
<div class="container">
    <div class="row">
        <div class="col-md-6">

           <div class="form-group">
                   <label for="inputEmail3" class="col-md-6 control-label">Email</label>
                   <div class="col-md-6">
                           <input type="email" class="form-control" id="inputEmail3" placeholder="Email" />
                   </div>
           </div>
           <div class="form-group">
                   <label for="inputPassword3" class="col-md-6 control-label">Password</label>
                   <div class="col-md-6">
                           <input type="password" class="form-control" id="inputPassword3" placeholder="Password" />
                   </div>
           </div>
    </div>
        <div class="col-md-6">
            Container Right
        </div>
    </div>
</div>
-->

    <!--
        <div class="container">
            <div class="row col-md-4">
                <form>
                    <fieldset class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                        <small class="text-muted">We'll never share your email with anyone else.</small>
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </fieldset>
            </div>
            <div class="row col-md-8">
                <div class="col-md-12" style="background-color:red">imagesns<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></div>
            </div>
        </div>
    -->

        <div class="container">

            <h3>${fieldValue(bean: personInstance.probe.gene, field: "name")}</h3>
            <br/>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label" for="changeStatus">Change status</label>
                        <div class="">
                            <g:select onchange="statusChange(${personInstance.id});" class="form-control" title="Change status" name="change_${personInstance.id}" from="${personInstance.CHANGE_STATUS}"  value="${fieldValue(bean: personInstance, field: "change")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderInstance.status=="completed")}"/>
                        </div>
                    </div>

                    <div id="noAmplification_${personInstance.id}" style="display:block">

                        <div class="form-group">
                            <label class="control-label" for="nucleousCount">Nucleous count</label>
                        <div class="">
                                <g:field type="number" class="form-control" name="nucleousCount_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "nucleousCount")}" onchange="percentageNucleousCountPositive(${personInstance.id});" disabled="${(session.user.role!="Administrator") && (diagnosticOrderInstance.status=="completed")}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="positiveNucleousCount">Positive nucleous count</label>
                            <div class="">
                                <g:field type="number" class="form-control" name="positiveNucleousCount_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "positiveNucleousCount")}" onchange="percentageNucleousCountPositive(${personInstance.id})" disabled="${(session.user.role!="Administrator") && (diagnosticOrderInstance.status=="completed")}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="percentageNucleousCountPositive">Percentage Nucleous Count Positive</label>
                            <div class="">
                                <g:field type="number" max="100" min="0" readonly="readonly" class="form-control" name="percentageNucleousCountPositive_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "percentageNucleousCountPositive")}" />
                            </div>
                        </div>
                    </div>

                    <div id="amplification_${personInstance.id}" style="display:none">

                        <div class="form-group">
                            <label class="control-label" for="centromerCount">Centromer Count</label>
                            <div class="">
                                <g:field type="number" class="form-control"  required="" name="centromerCount_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "centromerCount")}" onchange="ratioCentromerProbeCount(${personInstance.id})" disabled="${(session.user.role!="Administrator") && (diagnosticOrderInstance.status=="completed")}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="locusSpecificProbeCount">Probe Count</label>
                            <div class="">
                                <g:field type="number" class="form-control"  required="" name="locusSpecificProbeCount_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "locusSpecificProbeCount")}" onchange="ratioCentromerProbeCount(${personInstance.id})" disabled="${(session.user.role!="Administrator") && (diagnosticOrderInstance.status=="completed")}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="ratio">Ratio</label>
                            <div class="">
                                <g:field type="number" max="100" min="0" readonly="readonly" class="form-control" name="ratio_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "ratio")}" />
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="control-label" for="percentageNotCountableNucleous">Percentage not countable</label>
                        <div class="">
                            <g:field type="number" class="form-control" max="100" min="0"  required="" name="percentageNotCountableNucleous_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "percentageNotCountableNucleous")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderInstance.status=="completed")}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="result">Result</label>
                        <div class="">

                            <g:select class="form-control" title="result" name="result_${personInstance.id}" from="${personInstance.RESULT_STATUS}"  value="${fieldValue(bean: personInstance, field: "result")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderInstance.status=="completed")}"/>
                        </div>
                    </div>
                </div>

                <div class="col-md-5" style="background-color: whitesmoke">
                    <g:each in="${personInstance.uploadedFiles.sort { it.id }}" status="j" var="uploadedFile">
                        <div class="test">
                            <div class="images-containers">
                                <div class="fish-image" style="background-image:url(${createLink(controller:'diagnosticOrder', action:'getImage')}?filepath=${uploadedFile.filepath});">
                                    <%
                                        if((diagnosticOrderInstance.status!="completed") || (session.user.role=="Administrator")) {
                                            %>
                                    <a href="#" onclick="confirm('This image will be deleted permanently. Are you sure?') && deleteFile(this);" id="${uploadedFile.id}" class="delete"></a>
                                    <%
                                        }
                                        %>
                                </div>
                            </div>
                        </div>
                    </g:each>

                    <div class="image-upload">
                        <label for="item_${personInstance.id}">
                            <!--
                            <img src="http://goo.gl/pB9rpQ"/>
                            -->
    <%
    if((diagnosticOrderInstance.status!="completed") || (session.user.role=="Administrator")) {
  %>
                            <img title="Upload image" src="${createLinkTo(dir:'images',file:'uploadFile.png')}" />
                                    <%
                                        }
                                        %>
    
                        </label>
                    <!--
                        <input id="file-input" type="file"/>
                    -->
                        <input class="item_" id="item_${personInstance.id}" id2="item_${personInstance.id}" name="item_${personInstance.id}" type="file" onchange="uploadFile(this);" class="upload" />

                    </div>

                </div>
            </div>

            <br/>
            <div class="col-md-5">
                <input type="button" id="button" onclick="showhide(this, 'comment_div_${personInstance.id}');" value="${personInstance.comment?"Hide":"Show"} comment"/>
                <br/><br/>
                <div style="display:${personInstance.comment?"block":"none"};" id="comment_div_${personInstance.id}" class="comments">
                    <g:textArea style="width:100%; height:50%" name="comment_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "comment")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderInstance.status=="completed")}"/>
                </div>
            </div>


        </div>
<hr/>
    </g:each>


<!-- PASTE HERE -->


<%
}
%>

<!--
<g:ifUserHasRole roles="${User.ADMINISTRATOR}">

				<g:if test="${diagnosticOrderInstance?.medicalDoctor}">
				<li class="fieldcontain">
					<span id="user-label" class="property-label"><g:message code="diagnosticOrder.user.label" default="User" /></span>

						<span class="property-value" aria-labelledby="user-label"><g:link controller="user" action="show" id="${diagnosticOrderInstance?.medicalDoctor?.id}">${diagnosticOrderInstance?.medicalDoctor?.encodeAsHTML()}</g:link></span>
				</li>
				</g:if>

</g:ifUserHasRole>

-->
			</ol>
				<g:hiddenField name="version" value="${diagnosticOrderInstance?.version}" />                         
				<fieldset class="buttons">
                                    <!--
					<g:link class="edit" action="edit" resource="${diagnosticOrderInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                                        <g:link class="delete" action="delete" resource="${diagnosticOrderInstance}"><g:message code="default.button.delete.label" default="Delete" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
-->
<%
if((diagnosticOrderInstance.status!="completed") || (session.user.role=="Administrator")) {
%>
<%
if((nextStatus==null) || (nextStatus!="noStatusAvailable")) {
%>
	<g:select name="status" from="${[nextStatus, diagnosticOrderInstance?.status]}" required="" value="${diagnosticOrderInstance?.status}" valueMessagePrefix="diagnosticOrder.status"/>
<%
} else {
%>
        <g:select name="status" from="${[diagnosticOrderInstance?.status]}" required="" value="${diagnosticOrderInstance?.status}" valueMessagePrefix="diagnosticOrder.status"/>
<%
}
%>
                                <g:actionSubmit class="save" action="update" value="${message(code: 'default.button.creasdfsfte.label', default: 'Save')}" onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Are you sure?')}');" />
                                <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
<%
}
%>

<g:ifUserHasRole roles="${User.MEDICAL_DOCTOR}">
                    </g:ifUserHasRole>
				</fieldset>
<!--
    public static final String TECHNICAL_ASSISTANT = "TechnicalAssistant"
    public static final String MEDICAL_DOCTOR = "MedicalDoctor"
    public static final String ASSISTANT_MEDICAL_DOCTOR = "AssistantMedicalDoctor"
    public static final String MANAGER = "Manager"
    public static final String ACCOUNTANT = "Accountant"
    public static final String ADMINISTRATOR = "Administrator"
-->

			</g:form>

<!--
<g:form enctype="multipart/form-data" class="upload" name="fileinfo">
<label>File Name </label><input type="text" name="name" value="">
<input type="file" name="uploadField">
<input type="button" class="clickClass" value="submit">
</g:form>

-->

<script>

<!--
$('.clickClass').click(function(){
    var oData = new FormData(document.forms.namedItem("fileinfo"));
    var url="${createLink(controller:'diagnosticOrder', action:'uploading')}";
     $.ajax({
         url:url,
         type:'POST',
         data:oData,
         processData: false,  // tell jQuery not to process the data
         contentType: false ,
         success:function (req) {
               alert(req);
            }
         });
});
-->

</script>

<script>

//$('#imageFile').on("change", function(){ uploadFile(); });

function deleteFile(fileObject) {
        var fileInput = document.getElementById(fileObject.id);

        var oData = new FormData();
        oData.append("id", fileInput.id);

//        alert(oData);
        var url="${createLink(controller:'diagnosticOrder', action:'delimage')}";
//        alert("esta a funcar ate aki");

        $.ajax({
        type:'POST',
        data: oData,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        url: url,
        success:function(data, textStatus){
//            alert("sucesso! ");
            location.reload();
//            jQuery('#storedvideos').html(data);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            alert("failure?! "+ XMLHttpRequest.responseText);
            location.reload();
        }
        });
}


function uploadFile(fileObject) {

//        alert("entre em uploadFile! "+fileObject.name);
//        var oData = new FormData(document.forms.namedItem(fileObject.name));
//        var oData = new FormData($(fileObject.name).closest("form"));


//var fileInput = document.getElementById(fileObject.name);

//var fileInput = document.getElementByName(fileObject.name);
//var fileInput = $("input[name=fileObject.name]");

//alert(fileObject.name);

var fileInput = document.getElementsByName(fileObject.name)[0];

var file = fileInput.files[0];


        var oData = new FormData();
        oData.append("file", file);
        oData.append("itemId", fileObject.name);

//        alert(oData);
        var url="${createLink(controller:'diagnosticOrder', action:'testando')}";
//        alert("esta a funcar ate aki");

        $.ajax({
        type:'POST',
        data: oData,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        url: url,
        success:function(data, textStatus){
//            alert("sucesso! ");
            location.reload();
//            jQuery('#storedvideos').html(data);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            alert("failure: "+ XMLHttpRequest.responseText);
            location.reload();
        }
        });

}

</script>

<!--
<div id="storedvideos"></div>

<div id="drop_zone">Drop files here</div>
<output id="list"></output>

-->

<script>
//  function handleFileSelect(evt) {
//
//    var files = evt.target.files; // FileList object
//
//    // Loop through the FileList and render image files as thumbnails.
//    for (var i = 0, f; f = files[i]; i++) {
//
//      // Only process image files.
//      if (!f.type.match('image.*')) {
//        continue;
//      }
//
//      var reader = new FileReader();
//
//      // Closure to capture the file information.
//      reader.onload = (function(theFile) {
//        return function(e) {
//          // Render thumbnail.
//          var span = document.createElement('span');
//          span.innerHTML = ['<img class="thumb" src="', e.target.result,
//                            '" title="', escape(theFile.name), '"/>'].join('');
//          document.getElementById('list').insertBefore(span, null);
//        };
//      })(f);
//
//      // Read in the image file as a data URL.
//      reader.readAsDataURL(f);
//    }


//    evt.stopPropagation();
//    evt.preventDefault();
//
//    var files = evt.dataTransfer.files; // FileList object.
//
//    // files is a FileList of File objects. List some properties.
//    var output = [];
//    for (var i = 0, f; f = files[i]; i++) {
//      output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
//                  f.size, ' bytes, last modified: ',
//                  f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
//                  '</li>');
//    }
//    document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
//  }

//  function handleDragOver(evt) {
//    evt.stopPropagation();
//    evt.preventDefault();
//    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
//  }
//
//  // Setup the dnd listeners.
//  var dropZone = document.getElementById('drop_zone');
//  dropZone.addEventListener('dragover', handleDragOver, false);
//  dropZone.addEventListener('drop', handleFileSelect, false);
</script>
		</div>

<script>
    updateAllStatus();
</script>

	</body>
</html>
