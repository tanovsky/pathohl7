
<%@ page import="de.uni_heidelberg.iph.DiagnosticOrder" %>
<%@ page import="de.uni_heidelberg.iph.User" %>
<!DOCTYPE html>
<html>
	<head>
            
  <script>
    function showhide(caller, myObjectId) {
        var div = document.getElementById(myObjectId);
        if (div.style.display !== "none") {
            caller.value = "Show comment";
            div.style.display = "none";
        } else {
            caller.value = "Hide comment";
            div.style.display = "block";
        }
    }

    function percentageNotCountableNucleous(id){
        var positiveNucleousCount = document.getElementById("positiveNucleousCount"+ "_" + id);
        var nucleousCount = document.getElementById("nucleousCount"+ "_" + id);
        var percentageNotCountableNucleous = document.getElementById("percentageNotCountableNucleous"+ "_" + id);
        if (nucleousCount.value > 0) {
            percentagetCountableNucleousValue = (positiveNucleousCount.value/nucleousCount.value) * 100;
            percentageNotCountableNucleous.value = 100 - percentagetCountableNucleousValue;
        }
    }

  </script>
     
<style>
  .thumb {
    height: 75px;
    border: 1px solid #000;
    margin: 10px 5px 0 0;
  }

.fish-image { width:100px;height:100px;float:left;position:relative;background-size:100px 100px;cursor: pointer;}

.images-container { overflow:auto; }
a.delete { display:none;position:absolute;top:0;right:0;width:15px;height:15px;text-indent:-999px; background-image:url(http://sanmarco.bg/eshop/themes/prestashop/img/icon/delete.png)}

.fish-image:hover a.delete { display:block; }

.test {
    float:left;
    position:relative;
    border: transparent 5px;
    border-style: solid;
}

.image-upload > input
{
    display: none;
}

.image-upload img
{
    width: 30px;
    cursor: pointer;
}

tr {
//            pointer-events : none;
}


tbody tr:hover {
          background-color: inherit!important;   /* or whatever color you want */
}

</style>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'diagnosticOrder.label', default: 'DiagnosticOrder')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
            
		<a href="#show-diagnosticOrder" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-diagnosticOrder" class="content scaffold-show" role="main">
                
			<g:form name="uau" id="uau" url="[resource:diagnosticOrderInstance, action:'update']" method="PUT" >

<!--                            
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
-->                        
			<h1>Diagnostic Order</h1>
                            
                  
                        <div style="float: right; text-align:right;padding-right:50px;padding-top:40px;">
                            <img src="${createLink(controller:'diagnosticOrder', action:'barcode')}?data=${diagnosticOrderInstance?.barcode}&width=200&height=50"/>
                            <div style="text-align:bottom;padding-right:20px;">
                                ${diagnosticOrderInstance?.barcode}
                            </div>
                        </div>
 

                        
<div style="padding-left: 20px;width: 700px;">   <!-- fields diag order -->
                        
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list diagnosticOrder">
				<g:if test="${diagnosticOrderInstance?.eNumber}">
                                    <b>
				<li class="fieldcontain">
					<span id="eNumber-label" class="property-label"><g:message code="diagnosticOrder.eNumber.label" default="eNumber" /></span>
                                        <span class="property-value" aria-labelledby="eNumber-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="eNumber"/></span>
				</li>
                                </b>
				</g:if>
				<g:if test="${diagnosticOrderInstance?.block}">
                                    <b>
				<li class="fieldcontain">
					<span id="block-label" class="property-label"><g:message code="diagnosticOrder.block.label" default="Block" /></span>
                                        <span class="property-value" aria-labelledby="block-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="block"/></span>
				</li>
                                </b>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.priority}">
				<li class="fieldcontain">
					<span id="priority-label" class="property-label"><g:message code="diagnosticOrder.priority.label" default="Priority" /></span>

						<span class="property-value" aria-labelledby="priority-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="priority"/></span>
				</li>
				</g:if>

<div class="fieldcontain ${hasErrors(bean: diagnosticOrderInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="diagnosticOrder.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>


<%
if((nextStatus==null) || (nextStatus!="noStatusAvailable")) {
%>
	<g:select name="status" from="${[nextStatus, diagnosticOrderInstance?.status]}" required="" value="${diagnosticOrderInstance?.status}" valueMessagePrefix="diagnosticOrder.status"/>
<%
} else {
%>
        <g:select name="status" from="${[diagnosticOrderInstance?.status]}" required="" value="${diagnosticOrderInstance?.status}" valueMessagePrefix="diagnosticOrder.status"/>
<%
}
%>

</div>
<!--
				<g:if test="${diagnosticOrderInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="diagnosticOrder.status.label" default="Status" /></span>

						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="status"/></span>

				</li>
				</g:if>
-->

<!--
				<g:if test="${diagnosticOrderInstance?.note}">
				<li class="fieldcontain">
					<span id="note-label" class="property-label"><g:message code="diagnosticOrder.note.label" default="Note" /></span>

						<span class="property-value" aria-labelledby="note-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="note"/></span>

				</li>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.reason}">
				<li class="fieldcontain">
					<span id="reason-label" class="property-label"><g:message code="diagnosticOrder.reason.label" default="Reason" /></span>

						<span class="property-value" aria-labelledby="reason-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="reason"/></span>

				</li>
				</g:if>

-->                                
				<g:if test="${diagnosticOrderInstance?.supportingInformation}">
				<li class="fieldcontain">
					<span id="supportingInformation-label" class="property-label"><g:message code="diagnosticOrder.supportingInformation.label" default="Supporting Information" /></span>
<!--
						<span class="property-value" aria-labelledby="supportingInformation-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="supportingInformation"/></span>
-->                                                
                                                <span class="property-value" aria-labelledby="supportingInformation-label"><g:textArea readonly="readonly" name="supportingInformation" style="width:100%; height:50%" value="${fieldValue(bean: diagnosticOrderInstance, field: "supportingInformation")}"/></span>
                                                                                                                           
                                                

				</li>
				</g:if>
<!--
				<g:if test="${diagnosticOrderInstance?.item}">
				<li class="fieldcontain">
					<span id="item-label" class="property-label"><g:message code="diagnosticOrder.item.label" default="Item" /></span>

						<g:each in="${diagnosticOrderInstance.item}" var="i">
						<span class="property-value" aria-labelledby="item-label"><g:link controller="diagnosticOrderItem" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></span>
						</g:each>

				</li>
				</g:if>
-->



</div>   <!-- fields diag order -->

<%
if (diagnosticOrderInstance?.status in [ "requested", "in-progress"]) {
%>
				<g:if test="${diagnosticOrderInstance?.item}">
				<li class="fieldcontain">
					<span id="item-label" class="property-label"><g:message code="diagnosticOrder.item.label" default="Item" /></span>

						<g:each in="${diagnosticOrderInstance.item.sort { it.id }}" var="i">
						<span class="property-value" aria-labelledby="item-label">
                                                    <g:link controller="gene" action="show" id="${i.id}">${i.probe.gene.name?.encodeAsHTML()} - ${i.probe.name?.encodeAsHTML()} (${i.probe.procedure?.encodeAsHTML()})</g:link>
                                                </span>
						</g:each>
				</li>
				</g:if>

<%
} else {
%>

<br/><br/>
    
    
<div>    <!-- for the table of genes -->

                <table>
                    <thead>
                        <tr>
                            <g:sortableColumn property="gene" title="${message(code: 'person.firstName.label', default: 'Gene')}" />
                            <g:sortableColumn property="change" title="${message(code: 'person.lastName.label', default: 'change')}" />
                            <g:sortableColumn property="nucleousCount" title="${message(code: 'person.lastName.label', default: 'NucleousCount')}" />
                            <g:sortableColumn property="percentagePositiveNucleous" title="${message(code: 'person.dateOfBirth.label', default: '+NucleousCount')}" />
                            <g:sortableColumn property="percentageNotCountableNucleous" title="${message(code: 'person.dateOfBirth.label', default: '%NotCountable')}" />
    
                            <g:sortableColumn property="ratio" title="${message(code: 'person.dateOfBirth.label', default: 'Ratio')}" />
    <!--
                            <g:sortableColumn property="comment" title="${message(code: 'person.dateOfBirth.label', default: 'Comment')}" />
-->
<%
if((diagnosticOrderInstance.status!="completed") || (session.user.role=="Administrator")) {
%>
                            <g:sortableColumn property="fotoDocumented" title="${message(code: 'person.dateOfBirth.label', default: 'Upload')}" />
<%
}
%>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${diagnosticOrderInstance?.item?.sort { it.id }}" status="i" var="personInstance">
                        <tr onmouseover="" class="${(i % 2) == 0 ? 'even' : 'odd'}">
                            <td><g:link title="Gene name" controller="gene" action="show" id="${personInstance.probe.gene.id}">${fieldValue(bean: personInstance.probe.gene, field: "name")}</g:link></td>

                            <td><g:select title="Change status" name="change_${personInstance.id}" from="${personInstance.CHANGE_STATUS}"  value="${fieldValue(bean: personInstance, field: "change")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderInstance.status=="completed")}"/></td>
<!--
                            <td><g:textField name="change" value="${fieldValue(bean: personInstance, field: "change")}"/> </td>   -->
                            <td><g:field title="Nucleous count" type="number" min="0" max="500" size="4" id="nucleousCount_${personInstance.id}" name="nucleousCount_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "nucleousCount")}" onchange="percentageNotCountableNucleous(${personInstance.id})" /> </td>
    
                            <td><g:field title="Positive nucleous count" type="number" min="0" max="500" size="4" id="positiveNucleousCount_${personInstance.id}" name="positiveNucleousCount_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "positiveNucleousCount")}" onchange="percentageNotCountableNucleous(${personInstance.id})" /> </td>
    
                            <td><g:field title="Percentage not countable" readonly="readonly" type="number" min="0" max="25" id="percentageNotCountableNucleous_${personInstance.id}" name="percentageNotCountableNucleous_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "percentageNotCountableNucleous")}"/> </td>
    
    
<%
if(false) {
%>
    
                            <td><g:field title="ratio" readonly="readonly" type="number" min="0" max="25" id="ratio_{personInstance.id}" name="ratio_{personInstance.id}" value="${fieldValue(bean: personInstance, field: "ratio")}"/> </td>
<%
} else {
%>
<td>&nbsp;</td>
<%    
}    
%>

<!--    
                            <td><g:textField size="10" name="comment_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "comment")}"/> </td>
-->
                            
                            
<!--
                            <td><g:checkBox title="Urgent" name='urgesdsa' checked='${fieldValue(bean: personInstance, field: "fotoDocumented")}'/><br/></td>
-->
<%
if((diagnosticOrderInstance.status!="completed") || (session.user.role=="Administrator")) {
%>
<!--
                            <td><input type="file" class="upload" onchange="uploadFile(this);" id="item_${personInstance.id}" name="item_${personInstance.id}" /><br/></td>
-->
<td>

        <div class="image-upload">
    <label for="item_${personInstance.id}">
        <!--
        <img src="http://goo.gl/pB9rpQ"/>
        -->
        <img title="Upload image" src="https://cdn0.iconfinder.com/data/icons/super-mono-reflection/red/hard-drive-upload_red.png" />

    </label>
<!--
    <input id="file-input" type="file"/>
-->
        <input class="item_" id="item_${personInstance.id}" id2="item_${personInstance.id}" name="item_${personInstance.id}" type="file" onchange="uploadFile(this);" class="upload" />
        
</div>

        
</td>
        
        
        
<%
}
%>

                        </tr>

             <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" ><td colspan="7">
                     <g:each in="${personInstance.uploadedFile.sort { it.id }}" status="j" var="uploadedFile">
    <!--
                     ${uploadedFile.name}
    -->
    <div class="test">
    <div class="images-containers">
        <div class="fish-image" style="background-image:url(${createLink(controller:'diagnosticOrder', action:'getImage')}?filepath=${uploadedFile.filepath});">
<%
if((diagnosticOrderInstance.status!="completed") || (session.user.role=="Administrator")) {
%>
    <a href="#" onclick="deleteFile(this);" id="${uploadedFile.id}" class="delete"></a>
<%
}
%>
</div>
    </div>
    </div>

    <!--
    onclick="location.href='${createLink(controller:'diagnosticOrder', action:'getImage')}?filepath=${uploadedFile.filepath}'"

    href="${createLink(controller:'diagnosticOrder', action:'delimage', params:[id: uploadedFile.id])}"
    <img onclick="location.href=this.src;" height="100" width="100" src="${createLink(controller:'diagnosticOrder', action:'getImage')}?filepath=${uploadedFile.filepath}" alt="whatever. alternatively, filename"/>
    -->
                     </g:each>
                     </td>
             </tr>

    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
    <td colspan="7">

    
    <input type="button" id="button" onclick="showhide(this, 'comment_div_${personInstance.id}');" value="Show comment"/>
    
    <div style="display:none;" id="comment_div_${personInstance.id}" class="comments">

    <g:textArea style="width:100%; height:50%" name="comment_${personInstance.id}" value="${fieldValue(bean: personInstance, field: "comment")}"/>
    </div>
    </td>
    </tr>
                    </g:each>

                    </tbody>
                </table>
    
    
</div>    <!-- for the table of genes -->
    
<%
}
%>

<!--
<g:ifUserHasRole roles="${User.ADMINISTRATOR}">

				<g:if test="${diagnosticOrderInstance?.user}">
				<li class="fieldcontain">
					<span id="user-label" class="property-label"><g:message code="diagnosticOrder.user.label" default="User" /></span>

						<span class="property-value" aria-labelledby="user-label"><g:link controller="user" action="show" id="${diagnosticOrderInstance?.user?.id}">${diagnosticOrderInstance?.user?.encodeAsHTML()}</g:link></span>
				</li>
				</g:if>

</g:ifUserHasRole>

-->
			</ol>
				<g:hiddenField name="version" value="${diagnosticOrderInstance?.version}" />
				<fieldset class="buttons">
                                    <!--
					<g:link class="edit" action="edit" resource="${diagnosticOrderInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                                        <g:link class="delete" action="delete" resource="${diagnosticOrderInstance}"><g:message code="default.button.delete.label" default="Delete" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
-->
<%
if((diagnosticOrderInstance.status!="completed") || (session.user.role=="Administrator")) {
%>
                                <g:actionSubmit class="save" action="update" value="${message(code: 'default.button.creasdfsfte.label', default: 'Save')}" onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Are you sure?')}');" />
                                <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
<%
}
%>

<g:ifUserHasRole roles="${User.MEDICAL_DOCTOR}">
                    </g:ifUserHasRole>
				</fieldset>
<!--
    public static final String TECHNICAL_ASSISTANT = "TechnicalAssistant"
    public static final String MEDICAL_DOCTOR = "MedicalDoctor"
    public static final String ASSISTANT_MEDICAL_DOCTOR = "AssistantMedicalDoctor"
    public static final String MANAGER = "Manager"
    public static final String ACCOUNTANT = "Accountant"
    public static final String ADMINISTRATOR = "Administrator"
-->

			</g:form>

<!--
<g:form enctype="multipart/form-data" class="upload" name="fileinfo">
<label>File Name </label><input type="text" name="name" value="">
<input type="file" name="uploadField">
<input type="button" class="clickClass" value="submit">
</g:form>

-->

     <script>
<!--
function supportAjaxUploadWithProgress() {
  return supportFileAPI() && supportAjaxUploadProgressEvents() && supportFormData();

  function supportFileAPI() {
    var fi = document.createElement('INPUT');
    fi.type = 'file';
    return 'files' in fi;
  };

  function supportAjaxUploadProgressEvents() {
    var xhr = new XMLHttpRequest();
    return !! (xhr && ('upload' in xhr) && ('onprogress' in xhr.upload));
  };

  function supportFormData() {
    return !! window.FormData;
  }
}


// The Javascript
var fileInput = document.getElementById('the-file');
var file = fileInput.files[0];
var formData = new FormData();
formData.append('file', file);

//alert("supportAjax ? " + supportAjaxUploadWithProgress());
-->

          </script>

<script>

<!--
$('.clickClass').click(function(){
    var oData = new FormData(document.forms.namedItem("fileinfo"));
    var url="${createLink(controller:'diagnosticOrder', action:'uploading')}";
     $.ajax({
         url:url,
         type:'POST',
         data:oData,
         processData: false,  // tell jQuery not to process the data
         contentType: false ,
         success:function (req) {
               alert(req);
            }
         });
});
-->

</script>

<script>

//$('#imageFile').on("change", function(){ uploadFile(); });

function deleteFile(fileObject) {
        var fileInput = document.getElementById(fileObject.id);

        var oData = new FormData();
        oData.append("id", fileInput.id);

//        alert(oData);
        var url="${createLink(controller:'diagnosticOrder', action:'delimage')}";
//        alert("esta a funcar ate aki");

        $.ajax({
        type:'POST',
        data: oData,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        url: url,
        success:function(data, textStatus){
//            alert("sucesso! ");
            location.reload();
//            jQuery('#storedvideos').html(data);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            alert("failure?! "+ XMLHttpRequest.responseText);
            location.reload();
        }
        });
}


function uploadFile(fileObject) {

//        alert("entre em uploadFile! "+fileObject.name);
//        var oData = new FormData(document.forms.namedItem(fileObject.name));
//        var oData = new FormData($(fileObject.name).closest("form"));


//var fileInput = document.getElementById(fileObject.name);

//var fileInput = document.getElementByName(fileObject.name);
//var fileInput = $("input[name=fileObject.name]");

//alert(fileObject.name);

var fileInput = document.getElementsByName(fileObject.name)[0];

var file = fileInput.files[0];


        var oData = new FormData();
        oData.append("file", file);
        oData.append("itemId", fileObject.name);

//        alert(oData);
        var url="${createLink(controller:'diagnosticOrder', action:'testando')}";
//        alert("esta a funcar ate aki");

        $.ajax({
        type:'POST',
        data: oData,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        url: url,
        success:function(data, textStatus){
//            alert("sucesso! ");
            location.reload();
//            jQuery('#storedvideos').html(data);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            alert("failure: "+ XMLHttpRequest.responseText);
            location.reload();
        }
        });

}

</script>

<!--
<div id="storedvideos"></div>

<div id="drop_zone">Drop files here</div>
<output id="list"></output>

-->

<script>
//  function handleFileSelect(evt) {
//
//    var files = evt.target.files; // FileList object
//
//    // Loop through the FileList and render image files as thumbnails.
//    for (var i = 0, f; f = files[i]; i++) {
//
//      // Only process image files.
//      if (!f.type.match('image.*')) {
//        continue;
//      }
//
//      var reader = new FileReader();
//
//      // Closure to capture the file information.
//      reader.onload = (function(theFile) {
//        return function(e) {
//          // Render thumbnail.
//          var span = document.createElement('span');
//          span.innerHTML = ['<img class="thumb" src="', e.target.result,
//                            '" title="', escape(theFile.name), '"/>'].join('');
//          document.getElementById('list').insertBefore(span, null);
//        };
//      })(f);
//
//      // Read in the image file as a data URL.
//      reader.readAsDataURL(f);
//    }


//    evt.stopPropagation();
//    evt.preventDefault();
//
//    var files = evt.dataTransfer.files; // FileList object.
//
//    // files is a FileList of File objects. List some properties.
//    var output = [];
//    for (var i = 0, f; f = files[i]; i++) {
//      output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
//                  f.size, ' bytes, last modified: ',
//                  f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
//                  '</li>');
//    }
//    document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
  }

//  function handleDragOver(evt) {
//    evt.stopPropagation();
//    evt.preventDefault();
//    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
//  }
//
//  // Setup the dnd listeners.
//  var dropZone = document.getElementById('drop_zone');
//  dropZone.addEventListener('dragover', handleDragOver, false);
//  dropZone.addEventListener('drop', handleFileSelect, false);
</script>



		</div>
	</body>
</html>
