
<%@ page import="de.uni_heidelberg.iph.DiagnosticOrder" %>
<%@ page import="de.uni_heidelberg.iph.User" %>
<!DOCTYPE html>
<html>
	<head>

  <script>
    function showhide(caller, myObjectId) {
        var div = document.getElementById(myObjectId);
        if (div.style.display !== "none") {
            caller.value = "Show comment";
            div.style.display = "none";
        } else {
            caller.value = "Hide comment";
            div.style.display = "block";
        }
    }

    function percentageNucleousCountPositive(id){
        var positiveNucleousCount = document.getElementById("positiveNucleousCount"+ "_" + id);
        var nucleousCount = document.getElementById("nucleousCount"+ "_" + id);
        var percentageNucleousCountPositive = document.getElementById("percentageNucleousCountPositive"+ "_" + id);

        calculatePercentage(positiveNucleousCount, nucleousCount, percentageNucleousCountPositive);
/*
        if (nucleousCount.value > 0) {
            var percentageNucleousCountPositiveValue = (positiveNucleousCount.value/nucleousCount.value) * 100;
            percentageNucleousCountPositive.value = percentageNucleousCountPositiveValue;
        } else {
            positiveNucleousCount.value = 0;
            nucleousCount.value = 0;
            percentageNucleousCountPositive.value = 0;
        }
*/
    }

    function ratioCentromerProbeCount(id){
        var centromerCount = document.getElementById("centromerCount"+ "_" + id);
        var locusSpecificProbeCount = document.getElementById("locusSpecificProbeCount"+ "_" + id);
        var ratio = document.getElementById("ratio"+ "_" + id);

        calculatePercentage(locusSpecificProbeCount, centromerCount, ratio);

        /*
        if (centromerCount.value > 0) {
            var ratioValue = (locusSpecificProbeCount.value/centromerCount.value) * 100;
            ratio.value = ratioValue;
        } else {
            centromerCount.value = 0;
            locusSpecificProbeCount.value = 0;
            ratio.value = "0";
        }
        */
    }


    function calculatePercentage(numeratorObject, denominatorObject, resultObject) {
        if (denominatorObject.value > 0) {
            var resultValue = (numeratorObject.value/denominatorObject.value) * 100;
            resultObject.value = resultValue;
        } else {
            numeratorObject.value = 0;
            denominatorObject.value = 0;
            resultObject.value = 0;
        }
    }








    function statusChange(id) {
//        alert("statusChange called for id : " + id);
        var divAmplificationElement = document.getElementById("amplification_" + id);
//        alert("aki0");
        var divNoAmplificationElement = document.getElementById("noAmplification_" + id);
//alert("aki1");

        var statusElement = document.getElementById("change_" + id);

/*
        if (statusElement.value == "Amplification") {
            divAmplificationElement.style.display = "block";
            divNoAmplificationElement.style.display = "none";
        } else {
            divAmplificationElement.style.display = "none";
            divNoAmplificationElement.style.display = "block";
        }
*/
        var isAmplificationSelected = (statusElement.value == "Amplification");
        divAmplificationElement.style.display =  isAmplificationSelected ? "block":"none";
        divNoAmplificationElement.style.display = isAmplificationSelected ? "none":"block";

//        var centromerCount = document.getElementById("centromerCount"+ "_" + id);
//        var locusSpecificProbeCount = document.getElementById("locusSpecificProbeCount"+ "_" + id);
//        var ratio = document.getElementById("ratio"+ "_" + id);
//        var positiveNucleousCount = document.getElementById("positiveNucleousCount"+ "_" + id);
//        var nucleousCount = document.getElementById("nucleousCount"+ "_" + id);
//        var percentageNucleousCountPositive = document.getElementById("percentageNucleousCountPositive"+ "_" + id);
//
//        if ()
//        centromerCount = 0
//        locusSpecificProbeCount = 0
//        ratio = 0
//        positiveNucleousCount = 0
//        nucleousCount = 0
//        percentageNucleousCountPositive = 0
    }

    function updateAllStatus() {
        var listOfGenes = ${diagnosticOrderInstance?.items?.collect{it.id}};
        for (i in listOfGenes){
            statusChange(listOfGenes[i]);
        }
    }

  </script>

<style>
  .thumb {
    height: 75px;
    border: 1px solid #000;
    margin: 10px 5px 0 0;
  }

.fish-image { width:100px;height:100px;float:left;position:relative;background-size:100px 100px;cursor: pointer;}

.images-container { overflow:auto; }
//a.delete { display:none;position:absolute;top:0;right:0;width:15px;height:15px;text-indent:-999px; background-image:url(http://sanmarco.bg/eshop/themes/prestashop/img/icon/delete.png)}
a.delete { display:none;position:absolute;top:0;right:0;width:15px;height:15px;text-indent:-999px; background-image:url(${createLinkTo(dir:'images',file:'delete.png')})}



.fish-image:hover a.delete { display:block; }

.test {
    float:left;
    position:relative;
    border: transparent 5px;
    border-style: solid;
}

.image-upload > input
{
    display: none;
}

.image-upload img
{
    width: 30px;
    cursor: pointer;
}

tr {
//            pointer-events : none;
}


tbody tr:hover {
          background-color: inherit!important;   /* or whatever color you want */
}

</style>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'diagnosticOrder.label', default: 'DiagnosticOrder')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>

        <body>
		<a href="#show-diagnosticOrder" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.diagnostic.order.list" default="Diagnostic Order list" /></g:link></li>

<g:ifUserHasRole roles="${User.REQUESTER_MEDICAL_DOCTOR}">
				<li><g:link class="create" action="create"><g:message code="default.diagnostic.order.create" default="Create Diagnostic Order" /></g:link></li>
</g:ifUserHasRole>
			</ul>
		</div>
		<div id="show-diagnosticOrder" class="content scaffold-show" role="main">

			<g:form name="uau" id="uau" url="[resource:diagnosticOrderInstance, action:'updateDOStatus']" method="PUT" >
			<h1>${message(code: 'default.diagnostic.order', default: 'Diagnostic Order')}<span style="font-size:25px;text-align:right;color:green;"> <g:message code="default.fish.status.${diagnosticOrderInstance?.status}" /></span></h1>

                        <div style="float: right; text-align:right;padding-right:50px;padding-top:40px;">
                            <g:if test="${diagnosticOrderInstance?.eNumber}">
                                <img src="${createLink(controller:'diagnosticOrder', action:'barcode')}?data=${diagnosticOrderInstance?.eNumber}&width=300&height=75"/>
                                <center>
                                <div style="text-align:bottom;padding-right:20px;">
                                    ${diagnosticOrderInstance?.eNumber}
                                </div>
                                </center>
                            </g:if>
                            <g:else>
                                                            <div style="text-align:bottom;padding-right:20px;">
                                No Barcode available
                            </div>

                            </g:else>
                        </div>


<div style="padding-left: 20px;width: 800px;">   <!-- fields diag order -->

			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list diagnosticOrder">

				<g:if test="${diagnosticOrderInstance?.requesterMedicalDoctor}">
                                    <b>
				<li class="fieldcontain">
					<span id="medicalDoctor-label" class="property-label"><g:message code="default.requester" default="RequesterMedicalDoctor" /></span>
                                        <span class="property-value" aria-labelledby="medicalDoctor-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="requesterMedicalDoctor.name"/></span>
				</li>
                                </b>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.eNumber}">
                                    <b>
				<li class="fieldcontain">
					<span id="eNumber-label" class="property-label"><g:message code="default.eNumber" default="eNumber" /></span>
                                        <span class="property-value" aria-labelledby="eNumber-label"><g:formatENumber eNumber="${diagnosticOrderInstance.eNumber}" /></span>
                                </li>
<!--
				<g:fieldValue bean="${diagnosticOrderInstance}" field="eNumber"/>
-->
                                </b>
				</g:if>
				<g:if test="${diagnosticOrderInstance?.block}">
                                    <b>
				<li class="fieldcontain">
					<span id="block-label" class="property-label"><g:message code="default.block" default="Block" /></span>
                                        <span class="property-value" aria-labelledby="block-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="block"/></span>
				</li>
                                </b>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.patientName}">
                                    <b>
				<li class="fieldcontain">
					<span id="block-label" class="property-label"><g:message code="default.patient.name" default="patientName" /></span>
                                        <span class="property-value" aria-labelledby="block-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="patientName"/></span>
				</li>
                                </b>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.tumorType}">
                                    <b>
				<li class="fieldcontain">
					<span id="block-label" class="property-label"><g:message code="default.tumor.type" default="tumorType" /></span>
                                        <span class="property-value" aria-labelledby="block-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="tumorType"/></span>
				</li>
                                </b>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.priority}">
                                    <b>
				<li class="fieldcontain">
					<span id="priority-label" class="property-label"><g:message code="default.priority" default="Priority" /></span>

						<span class="property-value" aria-labelledby="priority-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="priority"/></span>
				</li>
                                </b>
				</g:if>

                                <g:if test="${diagnosticOrderInstance?.status}">
                                    <b>
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="default.status" default="Status" /></span>

						<span class="property-value" aria-labelledby="status-label"><g:message code="default.fish.status.${diagnosticOrderInstance?.status}" /></span>
				</li>
                                </b>
				</g:if>


                                <g:if test="${diagnosticOrderInstance?.dateCreated}">
                                    <b>
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="default.date.created" default="dateCreated" /></span>

						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${diagnosticOrderInstance.dateCreated}" type="datetime" style="LONG" timeStyle="SHORT"/></span>
				</li>
                                </b>
				</g:if>

<!--
				<g:if test="${diagnosticOrderInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="diagnosticOrder.status.label" default="Status" /></span>

						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="status"/></span>

				</li>
				</g:if>
-->

<!--
				<g:if test="${diagnosticOrderInstance?.note}">
				<li class="fieldcontain">
					<span id="note-label" class="property-label"><g:message code="diagnosticOrder.note.label" default="Note" /></span>

						<span class="property-value" aria-labelledby="note-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="note"/></span>

				</li>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.reason}">
				<li class="fieldcontain">
					<span id="reason-label" class="property-label"><g:message code="diagnosticOrder.reason.label" default="Reason" /></span>

						<span class="property-value" aria-labelledby="reason-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="reason"/></span>

				</li>
				</g:if>

-->
				<g:if test="${diagnosticOrderInstance?.supportingInformation}">
                                    <b>
				<li class="fieldcontain">
					<span id="supportingInformation-label" class="property-label"><g:message code="default.supporting.information" default="Supporting Information" /></span>
<!--
						<span class="property-value" aria-labelledby="supportingInformation-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="supportingInformation"/></span>
-->
                                                <span class="property-value" aria-labelledby="supportingInformation-label"><g:textArea readonly="readonly" name="supportingInformation" style="width:100%; height:50%" value="${fieldValue(bean: diagnosticOrderInstance, field: "supportingInformation")}"/></span>


				</li>
                                </b>
				</g:if>
                        </ol>

</div>   <!-- fields diag order -->

<br/><br/>

    <div style="padding-left: 80px; padding-right: 80px;">
                        <table>
                        <thead>
                            <th><g:message code="default.gene" default="Gene" /></th>
                            <th><g:message code="default.procedure" default="NOT_WORKING" /></th>
                            <th><g:message code="default.locus" default="NOT_WORKING" /></th>
                            <th><g:message code="default.tissue.type" default="NOT_WORKING" /></th>
                            <th><g:message code="default.status" default="NOT_WORKING" /></th>
                            <!--
                            <th></th>
                            -->
                        </thead>
                            <tbody>
                                <g:each in="${diagnosticOrderInstance?.items?.sort { it.id }}" status="i" var="doi">
                                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}, clickable-row" data-href="${createLink(controller: "diagnosticOrderItem", action:'item', id: doi.id)}">
    <!--
                                    <tr>
                                        <td>
                                            <g:checkBox title="${doi.probe.gene.name}" name='probes' checked='${false}' value="${"" + doi.probe.id}"/>
                                        </td>
    -->
                                        <td>${doi.probe.gene.name}</td>
                                        <td>${doi.probe.procedure}</td>
                                        <td>${doi.probe.gene.locus}</td>
                                        <td>${doi.probe?.tissueType?.name}</td>
                                        <td><g:message code="default.fish.status.${doi?.status}" /></td>
                                        <!-- <td>link - ${doi.id}</td>   -->
<!--
                                        <td><a href="${createLink(controller:'diagnosticOrderItem', action:'item', id: doi.id )}">link</a></td>
-->
                                    </tr>
                                </g:each>
                            </tbody>
                        </table>
    </div>
    </div>
    <div>
				<g:hiddenField name="version" value="${diagnosticOrderInstance?.version}" />
				<fieldset class="buttons">
                                    <!--
					<g:link class="edit" action="edit" resource="${diagnosticOrderInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                                        <g:link class="delete" action="delete" resource="${diagnosticOrderInstance}"><g:message code="default.button.delete.label" default="Delete" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
-->

<%
//if (!(diagnosticOrderInstance.status in [DiagnosticOrder.CANCELLED, DiagnosticOrder.COMPLETED]) || (session.user.role == User.ADMINISTRATOR)) {
if ((session.user.role in User.REQUESTER_MEDICAL_DOCTOR)  && !(diagnosticOrderInstance.status in [DiagnosticOrder.CANCELLED, DiagnosticOrder.COMPLETED]) || (session.user.role == User.ADMINISTRATOR)) {

List<String> selectOptionsAvailable = availableStatus

//    List selectOptionsAvailable
//    if((nextStatus==null) || (nextStatus!="noStatusAvailable")) {
//        selectOptionsAvailable = [diagnosticOrderInstance?.status]
//    } else {
//        selectOptionsAvailable = [nextStatus, diagnosticOrderInstance?.status]
//    }

%>
     <g:select name="status" from="${selectOptionsAvailable}" required="" value="${diagnosticOrderInstance?.status}" valueMessagePrefix="diagnosticOrder.status"/>
                                <g:actionSubmit class="save" action="updateDOStatus" value="${message(code: 'default.button.creasdfsfte.label', default: 'Save')}" onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Are you sure?')}');" />
<%
if (session.user.role == User.ADMINISTRATOR) {
%>

                                <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
<%
}
%>

<%
}
%>

<g:ifUserHasRole roles="${User.MEDICAL_DOCTOR}">
</g:ifUserHasRole>
				</fieldset>
<!--
    public static final String TECHNICAL_ASSISTANT = "TechnicalAssistant"
    public static final String MEDICAL_DOCTOR = "MedicalDoctor"
    public static final String ASSISTANT_MEDICAL_DOCTOR = "AssistantMedicalDoctor"
    public static final String MANAGER = "Manager"
    public static final String ACCOUNTANT = "Accountant"
    public static final String ADMINISTRATOR = "Administrator"
-->

			</g:form>

<!--
<g:form enctype="multipart/form-data" class="upload" name="fileinfo">
<label>File Name </label><input type="text" name="name" value="">
<input type="file" name="uploadField">
<input type="button" class="clickClass" value="submit">
</g:form>

-->

<script>

<!--
$('.clickClass').click(function(){
    var oData = new FormData(document.forms.namedItem("fileinfo"));
    var url="${createLink(controller:'diagnosticOrder', action:'uploading')}";
     $.ajax({
         url:url,
         type:'POST',
         data:oData,
         processData: false,  // tell jQuery not to process the data
         contentType: false ,
         success:function (req) {
               alert(req);
            }
         });
});
-->

</script>

<script>

//$('#imageFile').on("change", function(){ uploadFile(); });

function deleteFile(fileObject) {
        var fileInput = document.getElementById(fileObject.id);

        var oData = new FormData();
        oData.append("id", fileInput.id);

//        alert(oData);
        var url="${createLink(controller:'diagnosticOrder', action:'delimage')}";
//        alert("esta a funcar ate aki");

        $.ajax({
        type:'POST',
        data: oData,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        url: url,
        success:function(data, textStatus){
//            alert("sucesso! ");
            location.reload();
//            jQuery('#storedvideos').html(data);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            alert("failure?! "+ XMLHttpRequest.responseText);
            location.reload();
        }
        });
}


function uploadFile(fileObject) {

//        alert("entre em uploadFile! "+fileObject.name);
//        var oData = new FormData(document.forms.namedItem(fileObject.name));
//        var oData = new FormData($(fileObject.name).closest("form"));


//var fileInput = document.getElementById(fileObject.name);

//var fileInput = document.getElementByName(fileObject.name);
//var fileInput = $("input[name=fileObject.name]");

//alert(fileObject.name);

var fileInput = document.getElementsByName(fileObject.name)[0];

var file = fileInput.files[0];


        var oData = new FormData();
        oData.append("file", file);
        oData.append("itemId", fileObject.name);

//        alert(oData);
        var url="${createLink(controller:'diagnosticOrder', action:'testando')}";
//        alert("esta a funcar ate aki");

        $.ajax({
        type:'POST',
        data: oData,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        url: url,
        success:function(data, textStatus){
//            alert("sucesso! ");
            location.reload();
//            jQuery('#storedvideos').html(data);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            alert("failure: "+ XMLHttpRequest.responseText);
            location.reload();
        }
        });

}

</script>

<!--
<div id="storedvideos"></div>

<div id="drop_zone">Drop files here</div>
<output id="list"></output>

-->

<script>
//  function handleFileSelect(evt) {
//
//    var files = evt.target.files; // FileList object
//
//    // Loop through the FileList and render image files as thumbnails.
//    for (var i = 0, f; f = files[i]; i++) {
//
//      // Only process image files.
//      if (!f.type.match('image.*')) {
//        continue;
//      }
//
//      var reader = new FileReader();
//
//      // Closure to capture the file information.
//      reader.onload = (function(theFile) {
//        return function(e) {
//          // Render thumbnail.
//          var span = document.createElement('span');
//          span.innerHTML = ['<img class="thumb" src="', e.target.result,
//                            '" title="', escape(theFile.name), '"/>'].join('');
//          document.getElementById('list').insertBefore(span, null);
//        };
//      })(f);
//
//      // Read in the image file as a data URL.
//      reader.readAsDataURL(f);
//    }


//    evt.stopPropagation();
//    evt.preventDefault();
//
//    var files = evt.dataTransfer.files; // FileList object.
//
//    // files is a FileList of File objects. List some properties.
//    var output = [];
//    for (var i = 0, f; f = files[i]; i++) {
//      output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
//                  f.size, ' bytes, last modified: ',
//                  f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
//                  '</li>');
//    }
//    document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
//  }

//  function handleDragOver(evt) {
//    evt.stopPropagation();
//    evt.preventDefault();
//    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
//  }
//
//  // Setup the dnd listeners.
//  var dropZone = document.getElementById('drop_zone');
//  dropZone.addEventListener('dragover', handleDragOver, false);
//  dropZone.addEventListener('drop', handleFileSelect, false);
</script>
		</div>

<script>
//    updateAllStatus();
</script>

	</body>
</html>
