
<%@ page import="de.uni_heidelberg.iph.DiagnosticOrder" %>
<%@ page import="de.uni_heidelberg.iph.User" %>
<!DOCTYPE html>
<html>
	<head>

  <script>
    function showhide(caller, myObjectId) {
        var div = document.getElementById(myObjectId);
        if (div.style.display !== "none") {
            caller.value = "Show comment";
            div.style.display = "none";
        } else {
            caller.value = "Hide comment";
            div.style.display = "block";
        }
    }

    function percentageNucleousCountPositive(id){
        var positiveNucleousCount = document.getElementById("positiveNucleousCount"+ "_" + id);
        var nucleousCount = document.getElementById("nucleousCount"+ "_" + id);
        var percentageNucleousCountPositive = document.getElementById("percentageNucleousCountPositive"+ "_" + id);

        calculatePercentage(positiveNucleousCount, nucleousCount, percentageNucleousCountPositive);
/*
        if (nucleousCount.value > 0) {
            var percentageNucleousCountPositiveValue = (positiveNucleousCount.value/nucleousCount.value) * 100;
            percentageNucleousCountPositive.value = percentageNucleousCountPositiveValue;
        } else {
            positiveNucleousCount.value = 0;
            nucleousCount.value = 0;
            percentageNucleousCountPositive.value = 0;
        }
*/
    }

    function ratioCentromerProbeCount(id){
        var centromerCount = document.getElementById("centromerCount"+ "_" + id);
        var locusSpecificProbeCount = document.getElementById("locusSpecificProbeCount"+ "_" + id);
        var ratio = document.getElementById("ratio"+ "_" + id);

        calculatePercentage(locusSpecificProbeCount, centromerCount, ratio);

        /*
        if (centromerCount.value > 0) {
            var ratioValue = (locusSpecificProbeCount.value/centromerCount.value) * 100;
            ratio.value = ratioValue;
        } else {
            centromerCount.value = 0;
            locusSpecificProbeCount.value = 0;
            ratio.value = "0";
        }
        */
    }


    function calculatePercentage(numeratorObject, denominatorObject, resultObject) {
        if (denominatorObject.value > 0) {
            var resultValue = (numeratorObject.value/denominatorObject.value) * 100;
            resultObject.value = resultValue;
        } else {
            numeratorObject.value = 0;
            denominatorObject.value = 0;
            resultObject.value = 0;
        }
    }








    function statusChange(id) {
        alert("statusChange called for id : " + id);
        var divAmplificationElement = document.getElementById("amplification_" + id);
//        alert("aki0");
        var divNoAmplificationElement = document.getElementById("noAmplification_" + id);
//alert("aki1");

        var statusElement = document.getElementById("change_" + id);

/*
        if (statusElement.value == "Amplification") {
            divAmplificationElement.style.display = "block";
            divNoAmplificationElement.style.display = "none";
        } else {
            divAmplificationElement.style.display = "none";
            divNoAmplificationElement.style.display = "block";
        }
*/
        var isAmplificationSelected = (statusElement.value == "Amplification");
        divAmplificationElement.style.display =  isAmplificationSelected ? "block":"none";
        divNoAmplificationElement.style.display = isAmplificationSelected ? "none":"block";

//        var centromerCount = document.getElementById("centromerCount"+ "_" + id);
//        var locusSpecificProbeCount = document.getElementById("locusSpecificProbeCount"+ "_" + id);
//        var ratio = document.getElementById("ratio"+ "_" + id);
//        var positiveNucleousCount = document.getElementById("positiveNucleousCount"+ "_" + id);
//        var nucleousCount = document.getElementById("nucleousCount"+ "_" + id);
//        var percentageNucleousCountPositive = document.getElementById("percentageNucleousCountPositive"+ "_" + id);
//
//        if ()
//        centromerCount = 0
//        locusSpecificProbeCount = 0
//        ratio = 0
//        positiveNucleousCount = 0
//        nucleousCount = 0
//        percentageNucleousCountPositive = 0
    }

    function updateAllStatus() {
        var listOfGenes = ${diagnosticOrderInstance?.items?.collect{it.id}};
        for (i in listOfGenes){
            statusChange(listOfGenes[i]);
        }
    }

  </script>

<style>
  .thumb {
    height: 75px;
    border: 1px solid #000;
    margin: 10px 5px 0 0;
  }

.fish-image { width:100px;height:100px;float:left;position:relative;background-size:100px 100px;cursor: pointer;}

.images-container { overflow:auto; }
//a.delete { display:none;position:absolute;top:0;right:0;width:15px;height:15px;text-indent:-999px; background-image:url(http://sanmarco.bg/eshop/themes/prestashop/img/icon/delete.png)}
a.delete { display:none;position:absolute;top:0;right:0;width:15px;height:15px;text-indent:-999px; background-image:url(${createLinkTo(dir:'images',file:'delete.png')})}



.fish-image:hover a.delete { display:block; }

.test {
    float:left;
    position:relative;
    border: transparent 5px;
    border-style: solid;
}

.image-upload > input
{
    display: none;
}

.image-upload img
{
    width: 30px;
    cursor: pointer;
}

tr {
//            pointer-events : none;
}


tbody tr:hover {
          background-color: inherit!important;   /* or whatever color you want */
}

</style>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'diagnosticOrder.label', default: 'DiagnosticOrder')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body onload="updateAllStatus();">

		<a href="#show-diagnosticOrder" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-diagnosticOrder" class="content scaffold-show" role="main">

			<g:form name="uau" id="uau" url="[resource:diagnosticOrderInstance, action:'update']" method="PUT" >

<!--
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
-->
			<h1>Diagnostic Order Item</h1>

                        <div style="float: right; text-align:right;padding-right:50px;padding-top:40px;">
                            <g:if test="${diagnosticOrderInstance?.barcode}">
                                <img src="${createLink(controller:'diagnosticOrder', action:'barcode')}?data=${diagnosticOrderInstance?.barcode}&width=300&height=75"/>
                                <div style="text-align:bottom;padding-right:20px;">
                                    ${diagnosticOrderInstance?.barcode}
                                </div>
                            </g:if>
                            <g:else>
                                                            <div style="text-align:bottom;padding-right:20px;">
                                No Barcode available
                            </div>
                                
                            </g:else>
                        </div>


<div style="padding-left: 20px;width: 700px;">   <!-- fields diag order -->

			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list diagnosticOrder">

				<g:if test="${diagnosticOrderInstance?.requesterMedicalDoctor}">
                                    <b>
				<li class="fieldcontain">
					<span id="medicalDoctor-label" class="property-label"><g:message code="diagnosticOrder.medicalDoctor.label" default="RequesterMedicalDoctor" /></span>
                                        <span class="property-value" aria-labelledby="medicalDoctor-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="requesterMedicalDoctor.name"/></span>
				</li>
                                </b>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.eNumber}">
                                    <b>
				<li class="fieldcontain">
					<span id="eNumber-label" class="property-label"><g:message code="diagnosticOrder.eNumber.label" default="eNumber" /></span>
                                        <span class="property-value" aria-labelledby="eNumber-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="eNumber"/></span>
				</li>
                                </b>
				</g:if>
				<g:if test="${diagnosticOrderInstance?.block}">
                                    <b>
				<li class="fieldcontain">
					<span id="block-label" class="property-label"><g:message code="diagnosticOrder.block.label" default="Block" /></span>
                                        <span class="property-value" aria-labelledby="block-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="block"/></span>
				</li>
                                </b>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.priority}">
                                    <b>
				<li class="fieldcontain">
					<span id="priority-label" class="property-label"><g:message code="diagnosticOrder.priority.label" default="Priority" /></span>

						<span class="property-value" aria-labelledby="priority-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="priority"/></span>
				</li>
                                </b>
				</g:if>
                                
                                <g:if test="${diagnosticOrderInstance?.status}">
                                    <b>
				<li class="fieldcontain">
					<span id="priority-label" class="property-label"><g:message code="diagnosticOrder.status.label" default="Status" /></span>

						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="status"/></span>
				</li>
                                </b>
				</g:if>
<!--
				<g:if test="${diagnosticOrderInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="diagnosticOrder.status.label" default="Status" /></span>

						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="status"/></span>

				</li>
				</g:if>
-->

<!--
				<g:if test="${diagnosticOrderInstance?.note}">
				<li class="fieldcontain">
					<span id="note-label" class="property-label"><g:message code="diagnosticOrder.note.label" default="Note" /></span>

						<span class="property-value" aria-labelledby="note-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="note"/></span>

				</li>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.reason}">
				<li class="fieldcontain">
					<span id="reason-label" class="property-label"><g:message code="diagnosticOrder.reason.label" default="Reason" /></span>

						<span class="property-value" aria-labelledby="reason-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="reason"/></span>

				</li>
				</g:if>

-->
				<g:if test="${diagnosticOrderInstance?.supportingInformation}">
                                    <b>
				<li class="fieldcontain">
					<span id="supportingInformation-label" class="property-label"><g:message code="diagnosticOrder.supportingInformation.label" default="Supporting Information" /></span>
<!--
						<span class="property-value" aria-labelledby="supportingInformation-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="supportingInformation"/></span>
-->
                                                <span class="property-value" aria-labelledby="supportingInformation-label"><g:textArea readonly="readonly" name="supportingInformation" style="width:100%; height:50%" value="${fieldValue(bean: diagnosticOrderInstance, field: "supportingInformation")}"/></span>



				</li>
                                </b>
				</g:if>
<!--
				<g:if test="${diagnosticOrderInstance?.items}">
				<li class="fieldcontain">
					<span id="item-label" class="property-label"><g:message code="diagnosticOrder.item.label" default="Item" /></span>

						<g:each in="${diagnosticOrderInstance.items}" var="i">
						<span class="property-value" aria-labelledby="item-label"><g:link controller="diagnosticOrderItem" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></span>
						</g:each>

				</li>
				</g:if>
-->

</div>   <!-- fields diag order -->




    <div>
                        <table>
                        <thead>
                            <th>Gene</th>
                            <th>Procedure</th>
                            <th>Locus</th>
                            <th>TissueType</th>
                            <th>Status</th>
                        </thead>
                            <tbody>
                                    <tr>
                                        <td>${diagnosticOrderItemInstance.probe.gene.name}</td>
                                        <td>${diagnosticOrderItemInstance.probe.procedure}</td>
                                        <td>${diagnosticOrderItemInstance.probe.gene.locus}</td>
                                        <td>${diagnosticOrderItemInstance.probe?.tissueType?.name}</td>
                                        <td>TODO: UPDATE STATUS</td>
                                    </tr>
                            </tbody>
                        </table>    
    </div>
<br/><br/><br/>
    <div>
                        <table>
                        <thead>
                            <th>Probe</th>
                            <th>Code</th>
                            <th>Description</th>
                            <th>Note</th>
                            <th>manufacturer</th>
                        </thead>
                            <tbody>
                                    <tr>
                                        <td>${diagnosticOrderItemInstance.probe.name}</td>
                                        <td>${diagnosticOrderItemInstance.probe.code}</td>
                                        <td>${diagnosticOrderItemInstance.probe.description}</td>
                                        <td>${diagnosticOrderItemInstance.probe?.note}</td>
                                        <td>${diagnosticOrderItemInstance.probe?.manufacturer?.name}</td>
                                    </tr>
                            </tbody>
                        </table>    
    </div>

    
<!--    
    public static final String DRAFT = "draft"
    
    public static final String REQUESTED = "requested"

    public static final String ACCEPTED = "accepted"

    public static final String SUSPENDED = "suspended"

    /** In case of step non-conformity */
    public static final String PAUSED = "paused"

    public static final String IN_PROGRESS = "in-progress"

    public static final String REVIEW_LABORATORY_PREPARATION = "reviewLaboratoryPreparation"

    public static final String REVIEW_DIAGNOSTIC_EVALUATION = "reviewDiagnosticEvaluation"

    public static final String REPORT_SIGNED = "reportSigned"

    public static final String REPORT_SENT = "reportSent"

    public static final String COMPLETED = "completed"
-->
    
<%
if ((!(diagnosticOrderInstance?.status in [ DiagnosticOrder.REQUESTED, DiagnosticOrder.IN_PROGRESS ])) ||
    ((session.user.role in [User.ASSISTANT_MEDICAL_DOCTOR, User.MEDICAL_DOCTOR]))) {
%>


<br/><br/>

        <div class="container">

            <h3>${fieldValue(bean: diagnosticOrderItemInstance.probe.gene, field: "name")}</h3>
            <br/>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label" for="changeStatus">Change status</label>
                        <div class="">
                            <g:select onchange="statusChange(${diagnosticOrderItemInstance.id});" class="form-control" title="Change status" name="change_${diagnosticOrderItemInstance.id}" from="${diagnosticOrderItemInstance.CHANGE_STATUS}"  value="${fieldValue(bean: diagnosticOrderItemInstance, field: "change")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                        </div>
                    </div>

                    <div id="noAmplification_${diagnosticOrderItemInstance.id}" style="display:block">

                        <div class="form-group">
                            <label class="control-label" for="nucleousCount">Nucleous count</label>
                        <div class="">
                                <g:field type="number" class="form-control" name="nucleousCount_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "nucleousCount")}" onchange="percentageNucleousCountPositive(${diagnosticOrderItemInstance.id});" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="positiveNucleousCount">Positive nucleous count</label>
                            <div class="">
                                <g:field type="number" class="form-control" name="positiveNucleousCount_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "positiveNucleousCount")}" onchange="percentageNucleousCountPositive(${diagnosticOrderItemInstance.id})" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="percentageNucleousCountPositive">Percentage Nucleous Count Positive</label>
                            <div class="">
                                <g:field type="number" max="100" min="0" readonly="readonly" class="form-control" name="percentageNucleousCountPositive_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "percentageNucleousCountPositive")}" />
                            </div>
                        </div>
                    </div>

                    <div id="amplification_${diagnosticOrderItemInstance.id}" style="display:none">

                        <div class="form-group">
                            <label class="control-label" for="centromerCount">Centromer Count</label>
                            <div class="">
                                <g:field type="number" class="form-control"  required="" name="centromerCount_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "centromerCount")}" onchange="ratioCentromerProbeCount(${diagnosticOrderItemInstance.id})" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="locusSpecificProbeCount">Probe Count</label>
                            <div class="">
                                <g:field type="number" class="form-control"  required="" name="locusSpecificProbeCount_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "locusSpecificProbeCount")}" onchange="ratioCentromerProbeCount(${diagnosticOrderItemInstance.id})" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="ratio">Ratio</label>
                            <div class="">
                                <g:field type="number" max="100" min="0" readonly="readonly" class="form-control" name="ratio_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "ratio")}" />
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="control-label" for="percentageNotCountableNucleous">Percentage not countable</label>
                        <div class="">
                            <g:field type="number" class="form-control" max="100" min="0"  required="" name="percentageNotCountableNucleous_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "percentageNotCountableNucleous")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="result">Result</label>
                        <div class="">

                            <g:select class="form-control" title="result" name="result_${diagnosticOrderItemInstance.id}" from="${diagnosticOrderItemInstance.RESULT_STATUS}"  value="${fieldValue(bean: diagnosticOrderItemInstance, field: "result")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                        </div>
                    </div>
                </div>

                <div class="col-md-5" style="background-color: whitesmoke">
                    <g:each in="${diagnosticOrderItemInstance.uploadedFiles.sort { it.id }}" status="j" var="uploadedFile">
                        <div class="test">
                            <div class="images-containers">
                                <div class="fish-image" style="background-image:url(${createLink(controller:'diagnosticOrder', action:'getImage')}?filepath=${uploadedFile.filepath});">
                                    <%
                                        if((diagnosticOrderItemInstance.status!="completed") || (session.user.role=="Administrator")) {
                                            %>
                                    <a href="#" onclick="confirm('This image will be deleted permanently. Are you sure?') && deleteFile(this);" id="${uploadedFile.id}" class="delete"></a>
                                    <%
                                        }
                                        %>
                                </div>
                            </div>
                        </div>
                    </g:each>

                    <div class="image-upload">
                        <label for="item_${diagnosticOrderItemInstance.id}">
                            <!--
                            <img src="http://goo.gl/pB9rpQ"/>
                            -->
    <%
    if((diagnosticOrderItemInstance.status!="sdf")) {
  %>
                            <img title="Upload image" src="${createLinkTo(dir:'images',file:'uploadFile.png')}" />
                                    <%
                                        }
                                        %>

                        </label>
                    <!--
                        <input id="file-input" type="file"/>
                    -->
                        <input class="item_" id="item_${diagnosticOrderItemInstance.id}" id2="item_${diagnosticOrderItemInstance.id}" name="item_${diagnosticOrderItemInstance.id}" type="file" onchange="uploadFile(this);" class="upload" />

                    </div>

                </div>
            </div>

            <br/>
            <div class="col-md-5">
                <input type="button" id="button" onclick="showhide(this, 'comment_div_${diagnosticOrderItemInstance.id}');" value="${diagnosticOrderItemInstance.comment?"Hide":"Show"} comment"/>
                <br/><br/>
                <div style="display:${diagnosticOrderItemInstance.comment?"block":"none"};" id="comment_div_${diagnosticOrderItemInstance.id}" class="comments">
                    <g:textArea style="width:100%; height:50%" name="comment_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: personInstance, field: "comment")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                </div>
            </div>
        </div>

<%
}
%>

<!--
<g:ifUserHasRole roles="${User.ADMINISTRATOR}">

				<g:if test="${diagnosticOrderInstance?.medicalDoctor}">
				<li class="fieldcontain">
					<span id="user-label" class="property-label"><g:message code="diagnosticOrder.user.label" default="User" /></span>

						<span class="property-value" aria-labelledby="user-label"><g:link controller="user" action="show" id="${diagnosticOrderInstance?.medicalDoctor?.id}">${diagnosticOrderInstance?.medicalDoctor?.encodeAsHTML()}</g:link></span>
				</li>
				</g:if>

</g:ifUserHasRole>

-->
			</ol>
				<g:hiddenField name="version" value="${diagnosticOrderItemInstance?.version}" />                         
				<fieldset class="buttons">
                                    <!--
					<g:link class="edit" action="edit" resource="${diagnosticOrderInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                                        <g:link class="delete" action="delete" resource="${diagnosticOrderInstance}"><g:message code="default.button.delete.label" default="Delete" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
-->

<%
//if((diagnosticOrderItemInstance.status!="completed") || (session.user.role == User.ADMINISTRATOR)) {
%>
<%
if((availableStatus==null) || (availableStatus!="noStatusAvailable")) {
%>
estou aki <%=availableStatus %>
	<g:select name="status" from="${availableStatus}" required="" value="${diagnosticOrderItemInstance?.status}" valueMessagePrefix="diagnosticOrderItem.status"/>
<%
} else {
%>
estou ali <%=availableStatus %>
        <g:select name="status" from="${[diagnosticOrderItemInstance?.status]}" required="" value="${diagnosticOrderItemInstance?.status}" valueMessagePrefix="diagnosticOrderItem.status"/>
<%
}
%>
                                <g:actionSubmit class="save" action="updateDOI" value="${message(code: 'default.button.creasdfsfte.label', default: 'Save')}" onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Are you sure?')}');" />
    
<%
if (session.user.role == User.ADMINISTRATOR) {
%>
    
                                <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />

<%
}
%>
        
<%
//}
%>

<g:ifUserHasRole roles="${User.MEDICAL_DOCTOR}">
                    </g:ifUserHasRole>
				</fieldset>
			</g:form>
<script>

<!--
$('.clickClass').click(function(){
    var oData = new FormData(document.forms.namedItem("fileinfo"));
    var url="${createLink(controller:'diagnosticOrder', action:'uploading')}";
     $.ajax({
         url:url,
         type:'POST',
         data:oData,
         processData: false,  // tell jQuery not to process the data
         contentType: false ,
         success:function (req) {
               alert(req);
            }
         });
});
-->

</script>

<script>

//$('#imageFile').on("change", function(){ uploadFile(); });

function deleteFile(fileObject) {
        var fileInput = document.getElementById(fileObject.id);

        var oData = new FormData();
        oData.append("id", fileInput.id);

//        alert(oData);
        var url="${createLink(controller:'diagnosticOrder', action:'delimage')}";
//        alert("esta a funcar ate aki");

        $.ajax({
        type:'POST',
        data: oData,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        url: url,
        success:function(data, textStatus){
//            alert("sucesso! ");
            location.reload();
//            jQuery('#storedvideos').html(data);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            alert("failure?! "+ XMLHttpRequest.responseText);
            location.reload();
        }
        });
}


function uploadFile(fileObject) {

//        alert("entre em uploadFile! "+fileObject.name);
//        var oData = new FormData(document.forms.namedItem(fileObject.name));
//        var oData = new FormData($(fileObject.name).closest("form"));


//var fileInput = document.getElementById(fileObject.name);

//var fileInput = document.getElementByName(fileObject.name);
//var fileInput = $("input[name=fileObject.name]");

//alert(fileObject.name);

var fileInput = document.getElementsByName(fileObject.name)[0];

var file = fileInput.files[0];


        var oData = new FormData();
        oData.append("file", file);
        oData.append("itemId", fileObject.name);

//        alert(oData);
        var url="${createLink(controller:'diagnosticOrder', action:'testando')}";
//        alert("esta a funcar ate aki");

        $.ajax({
        type:'POST',
        data: oData,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        url: url,
        success:function(data, textStatus){
//            alert("sucesso! ");
            location.reload();
//            jQuery('#storedvideos').html(data);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            alert("failure: "+ XMLHttpRequest.responseText);
            location.reload();
        }
        });

}

</script>

<!--
<div id="storedvideos"></div>

<div id="drop_zone">Drop files here</div>
<output id="list"></output>

-->

<script>
//  function handleFileSelect(evt) {
//
//    var files = evt.target.files; // FileList object
//
//    // Loop through the FileList and render image files as thumbnails.
//    for (var i = 0, f; f = files[i]; i++) {
//
//      // Only process image files.
//      if (!f.type.match('image.*')) {
//        continue;
//      }
//
//      var reader = new FileReader();
//
//      // Closure to capture the file information.
//      reader.onload = (function(theFile) {
//        return function(e) {
//          // Render thumbnail.
//          var span = document.createElement('span');
//          span.innerHTML = ['<img class="thumb" src="', e.target.result,
//                            '" title="', escape(theFile.name), '"/>'].join('');
//          document.getElementById('list').insertBefore(span, null);
//        };
//      })(f);
//
//      // Read in the image file as a data URL.
//      reader.readAsDataURL(f);
//    }


//    evt.stopPropagation();
//    evt.preventDefault();
//
//    var files = evt.dataTransfer.files; // FileList object.
//
//    // files is a FileList of File objects. List some properties.
//    var output = [];
//    for (var i = 0, f; f = files[i]; i++) {
//      output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
//                  f.size, ' bytes, last modified: ',
//                  f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
//                  '</li>');
//    }
//    document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
//  }

//  function handleDragOver(evt) {
//    evt.stopPropagation();
//    evt.preventDefault();
//    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
//  }
//
//  // Setup the dnd listeners.
//  var dropZone = document.getElementById('drop_zone');
//  dropZone.addEventListener('dragover', handleDragOver, false);
//  dropZone.addEventListener('drop', handleFileSelect, false);
</script>
		</div>

<script>
    updateAllStatus();
</script>

	</body>
</html>
