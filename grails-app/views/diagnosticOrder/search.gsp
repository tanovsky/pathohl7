<%@ page import="de.uni_heidelberg.iph.DiagnosticOrder" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'diagnosticOrder.label', default: 'DiagnosticOrder')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-diagnosticOrder" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			</ul>
		</div>
		<div id="list-diagnosticOrder" class="content scaffold-list" role="main">
                    <div class="container">
			<h1>Query results for "${query}"</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>

                                                <g:sortableColumn property="eNumber" title="${message(code: 'default.eNumber', default: 'eNumber')}" />
                                                <g:sortableColumn property="block" title="${message(code: 'default.block', default: 'block')}" />
						<g:sortableColumn property="status" title="${message(code: 'default.status', default: 'Status')}" />
                                                <g:sortableColumn property="priority" title="${message(code: 'default.priority', default: 'Priority')}" />
<!--
						<g:sortableColumn property="note" title="${message(code: 'diagnosticOrder.note.label', default: 'Note')}" />
						<g:sortableColumn property="supportingInformation" title="${message(code: 'diagnosticOrder.supportingInformation.label', default: 'Supporting Information')}" />
-->
						<g:sortableColumn property="supportingInformation" title="${message(code: 'default.supporting.information', default: 'Supp. Information')}" />
						<th><g:message code="default.requester" default="Requester" /></th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${diagnosticOrderInstanceList}" status="i" var="diagnosticOrderInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">


                                                <td><g:link action="items" id="${diagnosticOrderInstance.id}">${fieldValue(bean: diagnosticOrderInstance, field: "eNumber")}</g:link></td>

						<td>${fieldValue(bean: diagnosticOrderInstance, field: "block")}</td>

						<td><g:message code="default.fish.status.${diagnosticOrderInstance?.status}" /></td>

						<td>${fieldValue(bean: diagnosticOrderInstance, field: "priority")}</td>
<!--
						<td>${fieldValue(bean: diagnosticOrderInstance, field: "note")}</td>
						<td>${fieldValue(bean: diagnosticOrderInstance, field: "supportingInformation")}</td>
-->
						<td>${fieldValue(bean: diagnosticOrderInstance, field: "supportingInformation")}</td>

						<td>${fieldValue(bean: diagnosticOrderInstance, field: "requesterMedicalDoctor.login")}</td>
					</tr>
				</g:each>
				</tbody>
			</table>
<%
    if (diagnosticOrderInstanceList.size()==0) {
        out << g.message(code: "default.no.results.found")
    }
%>


<!--
			<div class="pagination">
				<g:paginate total="${diagnosticOrderInstanceCount ?: 0}" />
			</div>
-->

                        <div style="min-height: 400px;min-height: 400px;height: 400px;"></div>
                        
                        
</div>   <!-- container -->
		</div>
                
                
                
	</body>
</html>
