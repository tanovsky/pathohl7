<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main">
        <g:set var="entityName" value="${message(code: 'diagnosticOrder.label', default: 'DiagnosticOrder')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!--
        <script src="http://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>
        -->
        <g:javascript src="jquery.mask.min.js" />

        <script>
            $(function() {
            $( "#tabs" ).tabs();
            });
        </script>
    </head>
    <body>

        <a href="#create-diagnosticOrder" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
<!--
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
-->
                <li><g:link class="list" action="index"><g:message code="default.diagnostic.order.list" args="[entityName]" /></g:link></li>
                </ul>
            </div>
            <div id="create-diagnosticOrder" class="content scaffold-create" role="main">
            <!--
                <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            -->
                <h1><g:message code="default.diagnostic.order.create" default="Create Diagnostic Order" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${diagnosticOrderInstance}">
                <ul class="errors" role="alert">
                    <g:eachError bean="${diagnosticOrderInstance}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                        </g:eachError>
                </ul>
            </g:hasErrors>

            <g:form url="[resource:diagnosticOrderInstance, action:'saveGeneListRequest']" >
                <fieldset id="barcodeform" class="form">
                    <g:render template="form"/>
                </fieldset>

                <div id="tabs" style="border-color:transparent;border-width:10px;">
                    <ul>
                        <li><a href="#tabs-1">CISH/FISH</a></li>
<!--
                        <li><a href="#tabs-2">Mutationsanalysen</a></li>
                        <li><a href="#tabs-3">Translokationsaanalysen</a></li>
                        <li><a href="#tabs-4">...</a></li>
-->
                    </ul>
                    <div id="tabs-1">

                        <table>
                        <thead>
                            <th></th>
                            <th><g:message code="default.gene" default="Gene" /></th>
                            <th><g:message code="default.procedure" default="Procedure" /></th>
                            <th><g:message code="default.locus" default="Locus" /></th>
                            <th><g:message code="default.tissue.type" default="Tissue type" /></th>
                            <!--
                            <th>Note</th>
                            -->
                        </thead>
                            <tbody>
                                <g:each in="${probeList}" status="jj" var="probe">
                                    <tr>
                                        <td>
                                            <g:checkBox title="${probe.gene.name}" name='probes' checked='${false}' value="${""+probe.id}"/>
                                        </td>
                                        <td>${probe.gene.name}</td>
                                        <td>${probe.procedure}</td>
                                        <td>${probe.gene.locus}</td>
                                        <td>${probe?.tissueType?.name}</td>
                                        <!--
                                        <td>${probe.note}</td>
                                        -->
                                    </tr>
                                </g:each>
                                <tr>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
<!--
                    <div id="tabs-2">
                        <p>TODO</p>
                    </div>
                    <div id="tabs-3">
                        <p>TODO</p>
                    </div>
                    <div id="tabs-4">
                        <p>TODO</p>
                    </div>
-->
                </div>




                <fieldset class="buttons">
                    <g:submitButton onclick="return isValid(document.getElementById('eNumber').value) && isAnyCheckBoxSelected()" name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                    </fieldset


                </g:form>
        </div>
<script>

// TODO Improve and replace by Regular expression
function isValid(eNumber){
    var isValido = true;
/*
    var currentTime = new Date();
    var actualYear = currentTime.getFullYear()- 2000;
    var allowedSymbols = "erdh"
    var eNumberParts = eNumber.split("-");
    isValido = ((eNumberParts.length==3)
         && (eNumberParts[0].length==1)
         && (allowedSymbols.indexOf(eNumberParts[0])>=0)
         && (eNumberParts[1].length==5)
         && (eNumberParts[2].length==2)
         && (eNumberParts[2] <= actualYear)
);
if (!isValido) {
         alert("Not a valid eNumber!");
}
*/
    return isValido;
}

function isAnyCheckBoxSelected(){
//  alert("entrei");
    var checkedAtLeastOne = false;
    $('input[type="checkbox"]').each(function() {
//        alert("" + $(this));
        if ($(this).is(":checked")) {
            checkedAtLeastOne = true;
        }
    });
    if (!checkedAtLeastOne) {
        alert("Please select at least one gene");
    }
    return checkedAtLeastOne;
}

/*
function validateForm() {

}
*/

$(document).ready(function () {
  //called when key is pressed in textbox
  $("#eNumber").keypress(function (e) {
//  alert(e.which);
     //if the letter is not digit then display error and don't type anything
     if (
     e.which == 60 ||    // '<'
     e.which == 13       //  carriage return
//		 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)
) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});

//var options =  {onKeyPress: function(cep, e, field, options){
//  this.value = this.value.toUpperCase();
//}};


$('#eNumber').mask('Z/00/000000', {
    translation: {
        'Z': {
            pattern: /[ERSTGN]/, optional: true
        }
    }, placeholder: "_/__/______"});
    
//    ,onKeyPress: function (value, event) {
//    alert(event.currentTarget.value);
//        event.currentTarget.value = value.toUpperCase();
//        alert(value.toUpperCase());
//    },
//placeholder: " /  /      "}
//);


//    $('#eNumber').keyup(function() {
//
//        this.value = this.value.toUpperCase();
//    });



</script>
    </body>
</html>
