<html>
	<head>
		<meta name="layout" content="main" />
		<title>Login</title>

<style>


    .form-signin {
  max-width: 400px;
  display:block;
  background-color: #f7f7f7;
  -moz-box-shadow: 0 0 3px 3px #888;
    -webkit-box-shadow: 0 0 3px 3px #888;
	box-shadow: 0 0 3px 3px #888;
  border-radius:2px;
}
.main{
	padding: 38px;
}
.social-box{
  margin: 0 auto;
  padding: 38px;
  border-bottom:1px #ccc solid;
}
.social-box a{
  font-weight:bold;
  font-size:18px;
  padding:8px;
}
.social-box a i{
  font-weight:bold;
  font-size:20px;
}
.heading-desc{
	font-size:20px;
	font-weight:bold;
	padding:38px 38px 0px 38px;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  font-size: 16px;
  height: 20px;
  padding: 20px;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="text"] {
  margin-bottom: 0px;
  border-radius: 5px;
}
.form-signin input[type="password"] {
  margin-bottom: 0px;
  border-radius: 5px;
}
.input-group {
    margin-bottom: 15px;
}
.login-footer{
	background:#f0f0f0;
	margin: 0 auto;
	border-top: 1px solid #dadada;
	padding:20px;
}
.login-footer .left-section a{
	font-weight:bold;
	line-height:19px;
}

.mg-btm{
	margin-bottom:20px;
}



.btn {
  background: #3498db;
  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
  background-image: -o-linear-gradient(top, #3498db, #2980b9);
  background-image: linear-gradient(to bottom, #3498db, #2980b9);
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 20px;

  text-decoration: none;
}

.btn:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
}





//
//.panel-heading {
//    padding: 5px 15px;
//}
//
//.panel-footer {
//	padding: 1px 15px;
//	color: #A0A0A0;
//}
//
//.profile-img {
//	width: 96px;
//	height: 96px;
//	margin: 0 auto 10px;
//	display: block;
//	-moz-border-radius: 50%;
//	-webkit-border-radius: 50%;
//	border-radius: 50%;
//}

</style>
        </head>
	<body>
		<div class="body" style="height:100%; min-height:700px; height:auto;">
			<g:if test="${flash.message}">
				<div class="message">${flash.message}</div>
			</g:if>

<!--
<div style="padding: 150px;">
			<g:form controller="user" action="authenticate" method="post" >

				<div style="background-color:WhiteSmoke;" class="dialog">
					<input type="hidden" name="cName" value="${cName}" >
					<input type="hidden" name="aName" value="${aName}" >
					<table>
						<tbody>
							<tr class="prop">
								<td class="name">
									<label for="login">Login:</label>
								</td>
								<td>
									<input type="text" id="login" name="login"/>
								</td>
							</tr>

							<tr class="prop">
								<td class="name">
									<label for="password">Password:</label>
								</td>
								<td>
									<input type="password" id="password" name="password"/>
								</td>
							</tr>
							<tr class="prop">
								<td class="name">
									<label for="btn"></label>
								</td>
								<td>
									<input class="save" type="submit" name= "btnSubmit" value="OK" />
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</g:form>
</div>
-->


<g:form controller="user" action="authenticate" method="post" >
  <div class="container" style="margin-top:40px">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong><g:message code="fish.signin.label"/></strong>
					</div>
					<div class="panel-body">
						<form role="form" action="#" method="POST">
							<fieldset>
<!--
								<div class="row">
									<div class="center-block">
										<img class="profile-img"
											src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="">
									</div>
								</div>
-->
								<div class="row">
									<div class="col-sm-12 col-md-10  col-md-offset-1 ">
										<div class="form-group">
											<div class="input-group">
<!--
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-user"></i>
												</span>
-->
												<input class="form-control" placeholder="Username" id="login" name="login" type="text" autofocus>
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
<!--
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-lock"></i>
												</span>
-->
												<input class="form-control" placeholder="Password" id="password" name="password" type="password" value="">
											</div>
										</div>
										<div class="form-group">
											<input type="submit" class="btn btn-lg btn-primary btn-block" value=<g:message code="default.signin"/>>
										</div>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
					<div class="panel-footer ">
						<g:message code="default.dont.have.account"/> <a href="mailto:${grailsApplication.config.application.admin.email}" onClick=""> <g:message code="default.contact.me"/> </a>
					</div>
                </div>
			</div>
		</div>
	</div>
</g:form>


<!--
<div style="margin: auto; width: 50%;padding: 50px;">
	<div class="row">
                <g:form class="form-signin mg-btm" controller="user" action="authenticate" method="post" >
    	<h3 class="heading-desc">Login</h3>

		<div class="main">
            <label>Username</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            	<input id="login" name="login" type="text" class="form-control" placeholder="Username" autofocus>
            </div>
            <label>Password</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                <input id="password" name="password" type="password" class="form-control" placeholder="Password">
            </div>

        	<div class="row">
                <div class="col-xs-6 col-md-6">
                     
                </div>
                <div class="col-xs-6 col-md-6 pull-right">
                    <button type="submit" class="btn btn-large btn-success pull-right">Login</button>
                </div>
            </div>
		</div>
		<span class="clearfix"></span>

		<div class="login-footer">
    		<div class="row">
                <div class="col-xs-6 col-md-6">
                    <div class="left-section">
                        <a href="">Create an account</a>
                        </div>
                    <div>
                            <a href="">(forgot password)</a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-6 pull-right">
                </div>
            </div>
		</div>
</g:form>
	</div>
</div>
-->






		</div>
	</body>
</html>