
<%@ page import="de.uni_heidelberg.iph.User" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title></title>
    </head>
    <body>
        <div class="nav">
            <ul>
                <li><a class="home" href="${createLinkTo(dir:'')}">Home</a></li>
                <g:ifUserHasRole roles="${User.REQUESTER_MEDICAL_DOCTOR},${User.TECHNICAL_ASSISTANT},${User.ASSISTANT_MEDICAL_DOCTOR},${User.MEDICAL_DOCTOR}">
                <li><g:link controller="diagnosticOrder" class="list" action="index">${message(code: 'default.diagnostic.order.list', default: 'Diagnostic Orders')}</g:link></li>
                </g:ifUserHasRole>
                <g:ifUserHasRole roles="Administrator,Manager">
                <li><g:link controller="console" class="list" action="index">Console</g:link></li>
                <li><g:link controller="user" class="list" action="index">Users</g:link></li>
                <li><g:link controller="Gene" class="list" action="index">Genes</g:link></li>
                <li><g:link controller="Probe" class="list" action="index">Probes</g:link></li>
                <li><g:link controller="diagnosticOrder" class="list" action="index">DiagnosticOrder</g:link></li>
                </g:ifUserHasRole>
            </ul>
<!--
role(inList:["TechnicalAssistant", "MedicalDoctor", "AssistentMedicalDoctor", "Manager", "Accountant", "Administrator"])
-->
        </div>
        <div class="body">
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <div style="padding-left: 100px;padding-right: 100px;" id="list-diagnosticOrder" class="content scaffold-list" role="main">
                <h1>${message(code: 'default.diagnostic.order.items.pending', default: 'Pending Diagnostic Orders Items')}</h1>
                <table>
                    <thead>
                        <tr>
                            <g:sortableColumn property="eNumber" title="${message(code: 'default.eNumber', default: 'eNumber')}" />
                            <g:sortableColumn property="block" title="${message(code: 'default.block', default: 'Block')}" />
                            <g:sortableColumn property="gene" title="${message(code: 'default.gene', default: 'Gene')}" />
                            <g:sortableColumn property="tissueType" title="${message(code: 'default.tissue.type', default: 'Tissue type')}" />
                            <g:sortableColumn property="procedure" title="${message(code: 'default.procedure', default: 'Procedure')}" />
                            <g:sortableColumn property="status" title="${message(code: 'default.status', default: 'Status')}" />
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${diagnosticOrderItemsInstanceList}" status="i" var="diagnosticOrderItemInstance">
                            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}, clickable-row" data-href="${createLink(controller: "diagnosticOrderItem", action:'item', params: [id: diagnosticOrderItemInstance.id])}">
<!--
                                <td><g:link action="items" controller="diagnosticOrder" id="${diagnosticOrderItemInstance.diagnosticOrder.id}">${fieldValue(bean: diagnosticOrderItemInstance.diagnosticOrder, field: "eNumber")}</g:link></td>
-->
                                <td>${fieldValue(bean: diagnosticOrderItemInstance.diagnosticOrder, field: "eNumber")}</td>
                                <td>${fieldValue(bean: diagnosticOrderItemInstance, field: "diagnosticOrder.block")}</td>
                                <td>${fieldValue(bean: diagnosticOrderItemInstance, field: "probe.gene.name")}</td>
                                <td>${fieldValue(bean: diagnosticOrderItemInstance, field: "probe.tissueType.name")}</td>
                                <td>${fieldValue(bean: diagnosticOrderItemInstance, field: "probe.procedure")}</td>
                                <td><g:message code="default.fish.status.${diagnosticOrderItemInstance?.status}" /></td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>
<!--
                <div class="pagination">
                    <g:paginate controller="user" action="entry" total="${diagnosticOrderItemsInstanceTotal ?: 0}" />
                </div>
-->

<%
    if (diagnosticOrderItemsInstanceList.size()==0) {
        out << g.message(code: "default.no.pending.tasks")
    }
%>
            </div>
            <div style="padding-left: 100px;padding-right: 100px;" id="list-diagnosticOrder" class="content scaffold-list" role="main">
                <h1>${message(code: 'default.diagnostic.order.items.latest', default: 'Latest Diagnostic Orders Items')}</h1>
                <table>
                    <thead>
                        <tr>
                            <g:sortableColumn property="eNumber" title="${message(code: 'default.eNumber', default: 'eNumber')}" />
                            <g:sortableColumn property="block" title="${message(code: 'default.block', default: 'Block')}" />
                            <g:sortableColumn property="gene" title="${message(code: 'default.gene', default: 'Gene')}" />
                            <g:sortableColumn property="tissueType" title="${message(code: 'default.tissue.type', default: 'Tissue type')}" />
                            <g:sortableColumn property="procedure" title="${message(code: 'default.procedure', default: 'Procedure')}" />
                            <g:sortableColumn property="status" title="${message(code: 'default.status', default: 'Status')}" />
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${myLatestDiagnosticOrderItemsInstanceList}" status="i" var="diagnosticOrderItemInstance">
                            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}, clickable-row" data-href="${createLink(controller: "diagnosticOrderItem", action:'item', params: [id: diagnosticOrderItemInstance.id])}">
<!--
                                <td><g:link action="items" controller="diagnosticOrder" id="${diagnosticOrderItemInstance.diagnosticOrder.id}">${fieldValue(bean: diagnosticOrderItemInstance.diagnosticOrder, field: "eNumber")}</g:link></td>
-->
                                <td>${fieldValue(bean: diagnosticOrderItemInstance.diagnosticOrder, field: "eNumber")}</td>
                                <td>${fieldValue(bean: diagnosticOrderItemInstance, field: "diagnosticOrder.block")}</td>
                                <td>${fieldValue(bean: diagnosticOrderItemInstance, field: "probe.gene.name")}</td>
                                <td>${fieldValue(bean: diagnosticOrderItemInstance, field: "probe.tissueType.name")}</td>
                                <td>${fieldValue(bean: diagnosticOrderItemInstance, field: "probe.procedure")}</td>
                                <td><g:message code="default.fish.status.${diagnosticOrderItemInstance?.status}" /></td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>
<!--
                <div class="pagination">
                    <g:paginate controller="user" action="entry" total="${latestPerformedDiagnosticOrderItemsTotal ?: 0}" />
                </div>
-->
<%
    if (myLatestDiagnosticOrderItemsInstanceList.size()==0) {
        out << g.message(code: "default.no.tasks.performed.lately", args: [grailsApplication.config.application.fish.history.daysBefore])
    }
%>
            </div>
        </div>
        <div style="min-height: 400px;min-height: 400px;height: 400px;"></div>
    </body>
</html>
