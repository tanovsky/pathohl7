
<%@ page import="de.uni_heidelberg.iph.User" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>User List</title>
    </head>
    <body>
        <div class="nav">
            <ul>
                <li><a class="home" href="${createLinkTo(dir:'')}">Home</a></li>
                <li><g:link class="create" action="create">New User</g:link></li>
            </ul>
        </div>    
        <div class="body">
            <h1>User List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="login" title="Login" />
<!--                        
                   	        <g:sortableColumn property="password" title="Password" />
-->                        
                   	        <g:sortableColumn property="role" title="Role" />
                        
                   	        <g:sortableColumn property="name" title="Name" />
                        
                   	        <g:sortableColumn property="tel" title="Tel" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${userInstanceList}" status="i" var="userInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${userInstance.id}">${fieldValue(bean:userInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:userInstance, field:'login')}</td>
                            
<!--                            
                        
                            <td>${fieldValue(bean:userInstance, field:'password')}</td>
-->                        
                            <td>${fieldValue(bean:userInstance, field:'role')}</td>
                        
                            <td>${fieldValue(bean:userInstance, field:'name')}</td>
                        
                            <td>${fieldValue(bean:userInstance, field:'tel')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>

            
			<div class="pagination">
				<g:paginate total="${userInstanceTotal ?: 0}" />
			</div>
        </div>
    </body>
</html>
