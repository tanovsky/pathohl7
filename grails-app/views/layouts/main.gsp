<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>
                    <g:message code="default.mother.institution"/>
                    <g:message code="default.institute"/>
                    <g:message code="default.molecular.diagnostic.department"/>
                </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">

        <asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>

		<link rel="stylesheet" href="${createLinkTo(dir:'css',file:'main.css')}" />


<!--
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
-->
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
                <link rel="stylesheet" href="${resource(dir: 'js', file: 'bootstrap.min.js')}" type="text/css">

<!--
		<link rel="shortcut icon" href="${createLinkTo(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
-->

<!--
                <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'jquery.ui.all.css')}">
<link rel="stylesheet" href="${createLinkTo(dir:'css',file:'demos.css')}">
-->


<style>
/*

COMO E POSSIVEL QUE ISTO ESTRAGUE O HIBERNATE!?=!!=


div#header {
	background-image: url(BannerTileBackground.GIF);
	background-repeat: repeat-x;

}
*/

.mylogo {
    width:100px;
//    height:50px;
//    max-width:2000px;
//    background-color:#0000FF;
}

div.inline { float:left; }
.clearBoth { clear:both; }

</style>






		<g:layoutHead />

<!--
		<g:javascript library="application" />
-->


                
<script type="text/javascript">
$(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
});
</script>
<style type="text/css">
.clickable-row{
cursor:pointer;
}
</style>


	</head>
	<body>

		<div id="header" style="background-color:#1e4f83;height:100px;">
                  <br/>
			<!--
			<p><a class="home" href="${createLinkTo(dir:'/pi')}">controladores</a></p>
   <div class="logo"><img style="padding-left:30px;padding-top:10px" src="${createLinkTo(dir:'images',file:'CBMASmall.jpg')}" alt="Grails" /></div>
-->
                  <div>
                      <img src="${resource(file: "Logo.png")}" style="height:50px;float:right;padding-right:30px;display:inline;" alt="Grails"/>
<!--
                      <div class="mylogo" ><img src="${createLinkTo(dir:'images',file:'custom/Logo.png')}" alt="IPH" style="width:100%;height:100%;" /></div>
-->

                      <img src="${resource(file: "UniversitaetKlinikumLogoOnlyWhite.png")}" style="height:65px;float:left;padding-left:30px;display:inline;" alt="Grails"/>
                      <div id="nomeAplicacao"  style="float:left;padding-left:30px;display: inline;">
                          <b><font size="4" color="WHITE">${message(code: 'default.mother.institution', default: 'University Clinic Heidelberg')}</font><font color="WHITE"></font></b><br/>
                          <b><font size="4" color="WHITE">${message(code: 'default.institute', default: 'Institute of Pathology Heidelberg')}</font><font color="WHITE"></font></b><br/>
                          <b><font size="2" color="WHITE">${message(code: 'default.molecular.diagnostic.department', default: 'Molecular Pathological Diagnostics')}</font><font color="WHITE"></font></b><br/>
                      </div>

                  </div>

<g:if test="${session.user}">

<div id="searchForm"  style="text-align:right;border-width: 150px;">
<fieldset class="form">
    <g:form name="myForm" url="[action:'search', controller:'diagnosticOrder']">
        <div class="fieldcontain">
            <g:textField style="width:50%;" name="query" value="${params.query}"/>&nbsp&nbsp&nbsp
        </div>
    </g:form>
</fieldset>
</div>

</g:if>


<!--
<table border="0"><tr border="0"><td>
<div id="logods"><img style="padding-left:0px;padding-top:0px" src="${createLinkTo(dir:'images',file:'logoDNAOpen1.jpg')}" alt="Grails" /></div>
</td>
<td>

<div id="nomeAplicacao"  style="padding-left:0px;">
<font size="5" color="WHITE">Bioinformatics<br/> Tools<br/> Agregator</font><font color="WHITE">     beta</font>
</div>
</td>
  </tr>
  </table>
-->

  <!--
  <p><a class="home" href="${createLinkTo(dir:'/user/entry')}">home</a></p>
			-->
<!--
<div style="text-align: right;">testing text align</div>
			-->

			<div id="loginHeader" style="text-align:left;">

			</div>
		</div>
<!--
		<div id="spinner" class="spinner" style="display:none;">
			<img src="${createLinkTo(dir:'images',file:'spinner.gif')}" alt="Spinner" />
		</div>
-->
		<!--
		<div class="logo"><img src="${createLinkTo(dir:'images',file:'grails_logo.jpg')}" alt="Grails" /></div>
-->
			<div id="barra" class="barra" style="background-color:#ffffff;text-align:right;">

			<div style="text-align:right;padding-right:20px;">
				<p>
                                    <b><g:if test="${session.user}"><a style="text-align:left;" href="">${message(code: 'default.welcome', default: 'Welcome')} ${session.user.login} | </a><g:link controller="user" action="logout">${message(code: 'default.logout', default: 'Logout')}</g:link></b>
					</g:if>
                                        <g:else>
                                          <br/>
                                        </g:else>

				</p>
			</div>
			</div>

		<g:layoutBody/>
                &nbsp;

  <!--
                <div id="espaco" style="min-height: 100%;">
              </div>
  -->
		<div class="footer" style="background-color:#1e4f83">
<!--

                  <div><img style="padding-left:30px;padding-top:10px;float:left;height:50%" src="${createLinkTo(dir:'images',file:'CBMA_Logo_transparente.PNG')}" alt="CBMA" /></div>

                  <div><img style="padding-left:30px;padding-top:10px;float:left;height:50%" src="${createLinkTo(dir:'images',file:'dilogo.png')}" alt="University of Minho" /></div>

-->



<!-- NOT WORKING.. To force the footer to stay down.. http://ryanfait.com/resources/footer-stick-to-bottom-of-page/ -->
<div class="push"></div>



<div style="padding-left:200px;">
  <br/>
  <font size="1" color="WHITE">${message(code: 'default.please.send.feedback', default: 'Please send feedback to')}: <%=grailsApplication.config.application.admin.email%> © <g:formatDate format="yyyy" date="${new Date()}"/>, Institut Pathologie Heidelberg</font>
  <p>

</p>




<!--
<g:if test="${session.user}">

<div id="searchForm"  style="text-align:right;">
<fieldset class="form">
    <g:form name="changeUser" url="[action:'change', controller:'user']">
        <div class="fieldcontain">
            <g:textField style="width:40%;" name="query" value="${params.query}"/>
        </div>
    </g:form>
</fieldset>
</div>

</g:if>
-->



</div>


		</div>



	</body>
</html>

