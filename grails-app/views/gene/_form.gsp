<%@ page import="de.uni_heidelberg.iph.Gene" %>



<div class="fieldcontain ${hasErrors(bean: geneInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="gene.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${geneInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: geneInstance, field: 'alias', 'error')} ">
	<label for="alias">
		<g:message code="gene.alias.label" default="Alias" />
		
	</label>
	<g:textField name="alias" value="${geneInstance?.alias}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: geneInstance, field: 'locus', 'error')} required">
	<label for="locus">
		<g:message code="gene.locus.label" default="Locus" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="locus" required="" value="${geneInstance?.locus}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: geneInstance, field: 'probe', 'error')} ">
	<label for="probe">
		<g:message code="gene.probe.label" default="Probe" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${geneInstance?.probe?}" var="p">
    <li><g:link controller="probe" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="probe" action="create" params="['gene.id': geneInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'probe.label', default: 'Probe')])}</g:link>
</li>
</ul>


</div>

