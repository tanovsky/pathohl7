
<%@ page import="de.uni_heidelberg.iph.Gene" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'gene.label', default: 'Gene')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-gene" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-gene" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list gene">
			
				<g:if test="${geneInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="gene.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${geneInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${geneInstance?.alias}">
				<li class="fieldcontain">
					<span id="alias-label" class="property-label"><g:message code="gene.alias.label" default="Alias" /></span>
					
						<span class="property-value" aria-labelledby="alias-label"><g:fieldValue bean="${geneInstance}" field="alias"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${geneInstance?.locus}">
				<li class="fieldcontain">
					<span id="locus-label" class="property-label"><g:message code="gene.locus.label" default="Locus" /></span>
					
						<span class="property-value" aria-labelledby="locus-label"><g:fieldValue bean="${geneInstance}" field="locus"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${geneInstance?.probe}">
				<li class="fieldcontain">
					<span id="probe-label" class="property-label"><g:message code="gene.probe.label" default="Probe" /></span>
					
						<g:each in="${geneInstance.probe}" var="p">
						<span class="property-value" aria-labelledby="probe-label"><g:link controller="probe" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:geneInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${geneInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
