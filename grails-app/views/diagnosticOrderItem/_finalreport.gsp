<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
    <head>
        <style>

.some-class {
    height: 50px;
//    width: 10px;
//    background-color: powderblue;
}

body {
//    background-color: yellow;
    padding-left: 30px;
    padding-right: 10px;
    padding-bottom: 100px;
    padding-top: 10px;
}

.footer {
    position: fixed;
    bottom: 0px;
    left: 100px;
}

.images {
//   background-color: yellow;
    border-style: solid;
    border-width: 0px;
   padding: 0px;
//   display: inline-block;
}

/* Single-direction drop shadow .style-four */

hr {
//    height: 12px;
    height: 0px;
    border: 0;
    box-shadow: inset 0 12px 12px -12px rgba(0, 0, 0, 0.5);
}

.img {
//    width: 100px;
//    max-width:25px; /* or whatever width you want. */
}

.thumbnail {
//    width: 75px;
//    height: 100px;
    height: 50px;
padding: 5px;
//    max-height: 100px;
//    max-width: 100px;
}

.logoMajor {
    width:100px;
}

.logoMinor {
    width:80px;
//    width: 80px;
//    height: 125px;

//    max-height: 100px;
//    max-width: 100px;
}

//.imagerendering {
//    max-width:100%;
//}


div {
    margin-bottom: 8px;
}

#patientData {
  display: flex;
}

#patientData div {
  flex-grow: 0;
  flex-shrink: 0;
  flex-basis: 40px;
}

////	.label { display: table-cell; }
//
//.label {
////    display: table-caption;
//    display: inline;
////    display: inline-block;
////    display: table-cell;
//    width: 150px;
//    text-align: left;
//   white-space:nowrap;
//    overflow:hidden;
//}

.justify {
    display: flex;
    justify-content: space-between;
    align-items: center;
}



//#contactContent { margin-top: 20px; margin-left: 20px;}
//label{
//    display:inline-block;
//    width:100px;
//}
//
//.input {
//    border: none;
//    background-color: #000;
//    margin-left: 20px;
//    margin-bottom: 5px;
//    padding-right: 50px;
//    font-size: large;
//    font-weight: bold;
//}




.labelValuePairContent { margin-top: 20px; margin-left: 20px;}
.value {
    border: none;
//    background-color: #000;
    margin-left: 20px;
    margin-bottom: 5px;
    padding-right: 0px;
    font-size: large;

}

.label {
    display:inline-block;
    width:200px;
    margin-bottom: 5px;
    font-weight: bold;
}

        </style>
    </head>
    <body>

<!--
        <p>This is the example of <span style="color:green">span tag</span> and the <span style="color:red">div tag</span> alongwith CSS</p>

        <p>

<%
10.times {
    out << 'sssWelcome '
}
%>

Congratulations, you have successfully started your first Grails application! At the moment this is the default page, feel free to modify it to either redirect to a controller or display whatever
            content you may choose. Below is a list of controllers that are currently deployed in this application,
            click on each to execute its default action.</p>
-->

        <div id="header" style="clear:both;width:100%;">

            <div style="float:left;margin:10px;">
                <rendering:inlineJpeg bytes="${logoMajorBytes}" class="logoMajor"/>
            </div>

            <div style="float:right;margin:10px;">
                <rendering:inlineJpeg bytes="${logoMinorBytes}" class="logoMinor"/>
            </div>

            <div style="float:left;margin:10px;width:250px;font-size:12px;font-weight: bold;">
                Molekularpathologische Diagnostik<br/>
                Pathologisches Institut<br/>
                Universitätklinikum Heidelberg
                <br/>
            </div>
        </div>
        <br/><br/><br/><br/><hr/>

<div id="requester">
    <div class="labelValuePairContent">
        <label class="label"><font>Anforderer :</font> </label><label class="value">${requester}</label><br/>
        <label class="label">Anforderungsdatum :</label><label class="value">${dateOfReceivedRequest}</label><br/>
    </div>
</div>

<hr/>


<!--

<div class="labelValuePairContent">

     <label class="label">Email:</label><label class="value">Email:</label>
 <br/>
     <label class="label">Subjectasdasd:</label><label class="value">Email:</label>

</div>


     <label class="label">Email:</label><label class="value">Email:</label>
 <br/>
     <label class="label">Subjectasdasd:</label><label class="value">Email:</label>


<hr/>
-->


<!--
<div id="patientwwwwwww">
    <h2>Patient</h2>
<div id="patientDatawwwwwww" style="clear:both;width:100%;">
    <div style="float:left;margin:10px;">
        <div id="eNumberwwwwwww">
            <span class="justify">eNumber : </span><span class="value">${eNumber}</span>
        </div>
        <div id="blockNumberwwwww">
            <span class="justify">blockNumber : </span><span class="value">${blockNumber}</span>
        </div>
    </div>

    <div id="barcodewwww" style="float:right;margin:10px;width:300px;">
        <rendering:inlineJpeg bytes="${barcodeBytes}" />
        <center>
            <div><b>${eNumber}</b></div>
        </center>
    </div>
</div>

<br/><br/><br/><br/><br/>

</div>
<hr/>

-->




<div id="patient">
    <h2>Patient</h2>
<div id="patientData" style="clear:both;width:100%;">
    <div class="labelValuePairContent" style="float:left;">
        <label class="label">eNumber : </label><label class="value">${eNumber}</label><br/>
        <label class="label">Block : </label><label class="value">${blockNumber}</label>
    </div>

    <div id="barcode" style="float:right;margin:10px;width:250px;">
        <rendering:inlineJpeg bytes="${barcodeBytes}" />
        <center>
            <div><b>${eNumber}</b></div>
        </center>
    </div>
</div>

<br/><br/><br/><br/>
</div>

<hr/>


<!--
<div id="patientsd">
    <h2>Patient</h2>
<div id="patientDatasd">
    <div class="labelValuePairContent" style="display: inline-block">
     <label class="label">eNumber : </label><label class="value">${eNumber}</label>
 <br/>
     <label class="label">blockNumber : </label><label class="value">${blockNumber}</label>
    </div>

    <div class="labelValuePairContent" style="display: inline-block;padding:50px;">
     <label class="label">eNumber : </label><label class="value">${eNumber}</label>
 <br/>
     <label class="label">blockNumber : </label><label class="value">${blockNumber}</label>
    </div>
</div>

<br/><br/><br/><br/><br/>
</div>

<hr/>
-->



<div id="results">
    <h2>Ergebnis</h2>
    <div class="labelValuePairContent">
        <label class="label">Veränderung : </label><label class="value">${change}</label><br/>
    </div>
<!--
<div id="change">
    <span class="label">change: </span><span class="value">${change}</span>
</div>
-->

<%
switch (change) {
    case "Amplification":
%>

    <div class="labelValuePairContent">
        <label class="label">centromerCount : </label><label class="value">${centromerCount}</label><br/>
        <label class="label">locusSpecificProbeCount : </label><label class="value">${locusSpecificProbeCount}</label><br/>
        <label class="label">ratio : </label><label class="value">${ratio}</label><br/>
    </div>

<%
    break
    default:
%>

    <div class="labelValuePairContent">
        <label class="label">Anzahl Kerne : </label><label class="value">${nucleousCount}</label><br/>
        <label class="label">Anzahl positive Kerne : </label><label class="value">${positiveNucleousCount}</label><br/>
        <label class="label">% Positive Kerne : </label><label class="value">${percentageNucleousCountPositive.round()}</label><br/>
        <label class="label">% Nicht auswertbarer Kerne : </label><label class="value">${percentageNotCountableNucleous.round()}</label><br/>
    </div>

<%
    break
    }
%>

    <div class="labelValuePairContent">
        <label class="label">Ergebnis : </label><label class="value">${result}</label><br/>
    </div>

</div>


<div class="images">

<%
images.each {
//    out << ' <h1>${it}</h1>'
%>

<span>
    <rendering:inlineJpeg bytes="${it}" class="thumbnail"/>
</span>

<%
}
%>

</div>

<%
if(comment) {
%>
<div id="comment">
    <div class="labelValuePairContent">
        <label class="label">Kommentar : </label><label class="value" style="width:100%;">${comment}</label><br/>
    </div>
</div>
<%
}
%>


<div id="accounting">

    <div class="labelValuePairContent" style="float:right;">
        <label class="label">Abrechnung : </label><label class="value">2 x 4872A</label><br/>
    </div>

</div>



<div id="footer" class="footer">
    <div class="labelValuePairContent">
        <label class="label" style="width:200px;"><font>Assistenzarzt |-Ärztin</font> : </label><label class="value"  style="width:100%;">____________________________________</label><br/>
        <label class="label" style="width:200px;"></label>&nbps;<label class="value">(${dateNow}, <font>${assistantMedicalDoctor}</font>)</label><br/>
    </div>
    <div class="labelValuePairContent">
        <label class="label" style="width:200px;"><font>Fach |-Oberarzt |-Ärztin</font>: </label><label class="value">____________________________________</label><br/>
        <label class="label" style="width:200px;"></label>&nbps;<label class="value">(${dateNow}, <font>${medicalDoctor}</font>)</label>
    </div>
</div>


<!--
        inicio<input type="text" name="myradio" value="dsfdfasd"/>fim
-->


<!--
<g:message code="common.login"/>

<h1><%=g.message(code: "creatsdfsdsdfae")%></h1>
-->




<!--
        dsffdsfsdf
        <form action="demo_form.asp">
            First name: <input type="text" name="fname" width="10" value="Mickey"/><br/>
            Last name: <input type="text" name="lname" value="fdassdf"/><br/>

            <form>
                <input type="radio" name="gender" value="male" checked="true"/><br/>
                <input type="radio" name="gender" value="female"/> Female<br/>
                <input type="radio" name="gender" value="other"/> Other
            </form>

            <input type="submit" value="Submit"/>
        </form>

<p>This is a paragraph. This text has no alignment specified.</p>

<div align="center" style="border:1px solid red">
This is some text in a div element!
</div>
<div align="center" style="border:1px solid red">
This is some text in a div element!
</div>
<div align="center" style="border:1px solid red">
This is some text in a div element!
</div>


<renderingDELETE:inlineJpeg bytes="${paidBytes}" class="some-class"/>


<p>This is a paragraph. This text has no alignment specified.</p>
<h1>h1</h1>
-->

<!--

<table border="1"><tr><td>
<div class="some-class" style="align:left;">
<renderingDELETE:inlineJpeg bytes="${paidBytes}" />
</div>

</td><td>
<div class="some-class" style="align:right;">
<renderingDELETE:inlineJpeg bytes="${paidBytes}" />
</div>

</td></tr>
</table>

<%=2+2%>
-->


<!--
        <g:form controller="person" action="save">
            <label>First Name: </label>
            <g:textField name="firstName"/><br/>
            <label>Last Name: </label>
            <g:textField name="lastName"/><br/>
            <label>Age: </label>
            <g:textField name="age"/><br/>
            <g:actionSubmit value="Save"/>
        </g:form>
-->
<!--
<div align="right" name="sdfsdf">something</div>

-->


    </body>
</html>