
<%@ page import="de.uni_heidelberg.iph.DiagnosticOrder" %>
<%@ page import="de.uni_heidelberg.iph.DiagnosticOrderItem" %>
<%@ page import="de.uni_heidelberg.iph.User" %>

<g:set var="ws" bean="workflowService"/>
<g:set var="allowedToPerformActions"
    value="${(diagnosticOrderItemInstance.status in ws.diagnosticOrderItemStatusHandledByRole(session.user.role))}"/>

<g:set var="allowedToEdit"
    value="${(diagnosticOrderItemInstance.status in ws.diagnosticOrderItemStatusEditedByRole(session.user.role))}"/>

<!DOCTYPE html>
<html>
    <head>
    <g:javascript src="jquery.mask.min.js" />
  <script>
    function showhide(caller, myObjectId) {
        var div = document.getElementById(myObjectId);
        if (div.style.display !== "none") {
            caller.value = "${g.message(code: 'default.show.comment')}";
            div.style.display = "none";
        } else {
            caller.value = "${g.message(code: 'default.hide.comment')}";
            div.style.display = "block";
        }
    }

    function percentageNucleousCountPositive(id){
        var positiveNucleousCount = document.getElementById("positiveNucleousCount"+ "_" + id);
        var nucleousCount = document.getElementById("nucleousCount"+ "_" + id);
        var percentageNucleousCountPositive = document.getElementById("percentageNucleousCountPositive"+ "_" + id);
        calculate("PERCENTAGE", positiveNucleousCount, nucleousCount, percentageNucleousCountPositive);
/*
        if (nucleousCount.value > 0) {
            var percentageNucleousCountPositiveValue = (positiveNucleousCount.value/nucleousCount.value) * 100;
            percentageNucleousCountPositive.value = percentageNucleousCountPositiveValue;
        } else {
            positiveNucleousCount.value = 0;
            nucleousCount.value = 0;
            percentageNucleousCountPositive.value = 0;
        }
*/
    }

    function ratioCentromerProbeCount(id) {
        var centromerCount = document.getElementById("centromerCount"+ "_" + id);
        var locusSpecificProbeCount = document.getElementById("locusSpecificProbeCount"+ "_" + id);
        var ratio = document.getElementById("ratio"+ "_" + id);

        calculate("RATIO", locusSpecificProbeCount, centromerCount, ratio)/100;

        /*
        if (centromerCount.value > 0) {
            var ratioValue = (locusSpecificProbeCount.value/centromerCount.value) * 100;
            ratio.value = ratioValue;
        } else {
            centromerCount.value = 0;
            locusSpecificProbeCount.value = 0;
            ratio.value = "0";
        }
        */
    }

    function calculate(what, numeratorObject, denominatorObject, resultObject) {
        if (denominatorObject.value > 0) {
            var resultValue = (numeratorObject.value/denominatorObject.value);
            switch (what) {
                case "PERCENTAGE":
                     resultValue = Math.round(resultValue * 100);
                break;
                case "RATIO":
//                    resultValue = (numeratorObject.value/denominatorObject.value);
                break;
            }
//            resultObject.value = (""+resultValue).replace(",", ".");
            resultObject.value = "" +resultValue
        } else {
            numeratorObject.value = 0;
            denominatorObject.value = 0;
            resultObject.value = 0;
        }
    }

    function statusChange(id) {
//        alert("statusChange called for id : " + id);
        var divAmplificationElement = document.getElementById("amplification_" + id);
//        alert("aki0");
        var divNoAmplificationElement = document.getElementById("noAmplification_" + id);
//alert("aki1");

        var statusElement = document.getElementById("change_" + id);

//        alert("statusElement : " + statusElement);

/*
        if (statusElement.value == "Amplification") {
            divAmplificationElement.style.display = "block";
            divNoAmplificationElement.style.display = "none";
        } else {
            divAmplificationElement.style.display = "none";
            divNoAmplificationElement.style.display = "block";
        }
*/
        var isAmplificationSelected = (statusElement.value == "Amplification");
        divAmplificationElement.style.display =  isAmplificationSelected ? "block":"none";
        divNoAmplificationElement.style.display = isAmplificationSelected ? "none":"block";

//        var centromerCount = document.getElementById("centromerCount"+ "_" + id);
//        var locusSpecificProbeCount = document.getElementById("locusSpecificProbeCount"+ "_" + id);
//        var ratio = document.getElementById("ratio"+ "_" + id);
//        var positiveNucleousCount = document.getElementById("positiveNucleousCount"+ "_" + id);
//        var nucleousCount = document.getElementById("nucleousCount"+ "_" + id);
//        var percentageNucleousCountPositive = document.getElementById("percentageNucleousCountPositive"+ "_" + id);
//
//        if ()
//        centromerCount = 0
//        locusSpecificProbeCount = 0
//        ratio = 0
//        positiveNucleousCount = 0
//        nucleousCount = 0
//        percentageNucleousCountPositive = 0
    }

    function updateAllStatus() {
        var listOfGenes = ${diagnosticOrderInstance?.items?.collect{it.id}};
        for (i in listOfGenes){
            statusChange(listOfGenes[i]);
        }
    }

  </script>

<style>
  .thumb {
    height: 75px;
    border: 1px solid #000;
    margin: 10px 5px 0 0;
  }

.fish-image { width:100px;height:100px;float:left;position:relative;background-size:100px 100px;cursor: pointer;}

.images-container { overflow:auto; }
//a.delete { display:none;position:absolute;top:0;right:0;width:15px;height:15px;text-indent:-999px; background-image:url(http://sanmarco.bg/eshop/themes/prestashop/img/icon/delete.png)}
a.delete { display:block;position:absolute;top:0;right:0;width:13px;height:13px;text-indent:-999px; background-image:url(${createLinkTo(dir:'images',file:'delete.png')})}

.fish-image:hover a.delete { display:block; }

.test {
    float:left;
    position:relative;
    border: transparent 5px;
    border-style: solid;
}

.image-upload > input
{
    display: none;
}

.image-upload img
{
    width: 30px;
    cursor: pointer;
}

tr {
//            pointer-events : none;
}


tbody tr:hover {
          background-color: inherit!important;   /* or whatever color you want */
}











.timeline {
    list-style: none;
    padding: 20px 0 20px;
    position: relative;
}

    .timeline:before {
        top: 0;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 3px;
        background-color: #eeeeee;
        left: 50%;
        margin-left: -1.5px;
    }

    .timeline > li {
        margin-bottom: 20px;
        position: relative;
    }

        .timeline > li:before,
        .timeline > li:after {
            content: " ";
            display: table;
        }

        .timeline > li:after {
            clear: both;
        }

        .timeline > li:before,
        .timeline > li:after {
            content: " ";
            display: table;
        }

        .timeline > li:after {
            clear: both;
        }

        .timeline > li > .timeline-panel {
            width: 46%;
            float: left;
            border: 1px solid #d4d4d4;
            border-radius: 2px;
            padding: 20px;
            position: relative;
            -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
            box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        }

            .timeline > li > .timeline-panel:before {
                position: absolute;
                top: 26px;
                right: -15px;
                display: inline-block;
                border-top: 15px solid transparent;
                border-left: 15px solid #ccc;
                border-right: 0 solid #ccc;
                border-bottom: 15px solid transparent;
                content: " ";
            }

            .timeline > li > .timeline-panel:after {
                position: absolute;
                top: 27px;
                right: -14px;
                display: inline-block;
                border-top: 14px solid transparent;
                border-left: 14px solid #fff;
                border-right: 0 solid #fff;
                border-bottom: 14px solid transparent;
                content: " ";
            }

        .timeline > li > .timeline-badge {
            color: #fff;
            width: 50px;
            height: 50px;
            line-height: 50px;
            font-size: 1.4em;
            text-align: center;
            position: absolute;
            top: 16px;
            left: 50%;
            margin-left: -25px;
            background-color: #999999;
            z-index: 100;
            border-top-right-radius: 50%;
            border-top-left-radius: 50%;
            border-bottom-right-radius: 50%;
            border-bottom-left-radius: 50%;
        }

        .timeline > li.timeline-inverted > .timeline-panel {
            float: right;
        }

            .timeline > li.timeline-inverted > .timeline-panel:before {
                border-left-width: 0;
                border-right-width: 15px;
                left: -15px;
                right: auto;
            }

            .timeline > li.timeline-inverted > .timeline-panel:after {
                border-left-width: 0;
                border-right-width: 14px;
                left: -14px;
                right: auto;
            }

.timeline-badge.primary {
    background-color: #2e6da4 !important;
}

.timeline-badge.success {
    background-color: #3f903f !important;
}

.timeline-badge.warning {
    background-color: #f0ad4e !important;
}

.timeline-badge.danger {
    background-color: #d9534f !important;
}

.timeline-badge.info {
    background-color: #5bc0de !important;
}

.timeline-title {
    margin-top: 0;
    color: inherit;
}

.timeline-body > p,
.timeline-body > ul {
    margin-bottom: 0;
}

    .timeline-body > p + p {
        margin-top: 5px;
    }

@media (max-width: 767px) {
    ul.timeline:before {
        left: 40px;
    }

    ul.timeline > li > .timeline-panel {
        width: calc(100% - 90px);
        width: -moz-calc(100% - 90px);
        width: -webkit-calc(100% - 90px);
    }

    ul.timeline > li > .timeline-badge {
        left: 15px;
        margin-left: 0;
        top: 16px;
    }

    ul.timeline > li > .timeline-panel {
        float: right;
    }

        ul.timeline > li > .timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        ul.timeline > li > .timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }
}


</style>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'diagnosticOrder.label', default: 'DiagnosticOrder')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>

<!--
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/south-street/jquery-ui.css" id="theme">
-->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!--
<link rel="stylesheet" href="css/bootstrap-image-gallery.min.css">
-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.image-gallery/3.1.0/css/bootstrap-image-gallery.min.css">

	</head>
        <body>

<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Hidden object just to hack the image gallery without changing what exists -->
<div class="links" style="display: none;">
    <g:each in="${diagnosticOrderItemInstance.uploadedFiles.sort { it.id }}" status="j" var="uploadedFile">
        <a id="${'image_' + uploadedFile.id}" href="${createLink(controller:'diagnosticOrder', action:'getImage')}?filepath=${uploadedFile.filepath}" title="" data-gallery>
            <!-- those are suposed to be the thumbnails...-->
        <!--
        <img src="http://placehold.it/350x150" alt="someImageMissing">
        -->
        </a>
    </g:each>
</div>
		<a href="#show-diagnosticOrder" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index" controller="diagnosticOrder"><g:message code="default.diagnostic.order.list" default="Diagnostic Order list" /></g:link></li>
				<li><g:link class="list" action="items" controller="diagnosticOrder" id="${diagnosticOrderInstance?.id}"><g:message code="default.diagnostic.order" default="Diagnostic Order" /></g:link></li>
                                <li><g:link class="list" action="timeline" controller="diagnosticOrderItem" id="${diagnosticOrderItemInstance?.id}"><g:message code="default.show.timeline.label" default="Timeline" /></g:link></li>
			</ul>
		</div>
		<div id="show-diagnosticOrder" class="content scaffold-show" role="main">
<g:workflowButtons diagnosticOrderItem="${diagnosticOrderItemInstance}"/>
			<g:form name="uau" id="uau" url="[resource:diagnosticOrderItemInstance, action:'updateDOI']" method="PUT" >
<div id="dataFields">

<!--
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
-->

<h1>${message(code: 'default.diagnostic.order.item', default: 'Diagnostic Order Item')}
<%
    if (isNonConformity) {
%>
    <span style="font-size:25px;color:red;">${message(code: 'default.workflow.status.taskNonConformant', default: 'Non-conformity')} : </span><span style="font-size:15px;color:red;"><%=nonConformityMsg%></span>
<%
} else {
%>
    <span style="font-size:25px;text-align:right;color:green;"><g:message code="default.fish.status.${diagnosticOrderItemInstance?.status}" /></span>
<%
// g.message(code: ws.statusLabel(diagnosticOrderItemInstance?.status))


}
%>
</h1>
                        <div style="float: right; text-align:right;padding-right:50px;padding-top:40px;">
                            <g:if test="${diagnosticOrderInstance?.eNumber}">
                                <img src="${createLink(controller:'diagnosticOrder', action:'barcode')}?data=${diagnosticOrderInstance?.eNumber}&width=300&height=75"/>
                                <center>
                                <div style="text-align:bottom;padding-right:20px;">
                                    ${diagnosticOrderInstance?.eNumber}
                                </div>
                                </center>
                            </g:if>
                            <g:else>
                                                            <div style="text-align:bottom;padding-right:20px;">
                                No Barcode available
                            </div>

                            </g:else>
                        </div>


<div style="padding-left: 20px;width: 800px;">   <!-- fields diag order -->

			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list diagnosticOrder">

				<g:if test="${diagnosticOrderInstance?.requesterMedicalDoctor}">
                                    <b>
				<li class="fieldcontain">
					<span id="medicalDoctor-label" class="property-label"><g:message code="default.requester" default="Requester" /></span>
                                        <span class="property-value" aria-labelledby="medicalDoctor-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="requesterMedicalDoctor.name"/></span>
				</li>
                                </b>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.eNumber}">
                                    <b>
				<li class="fieldcontain">
					<span id="eNumber-label" class="property-label"><g:message code="default.eNumber" default="eNumber" /></span>
                                        <span class="property-value" aria-labelledby="eNumber-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="eNumber"/></span>
				</li>
                                </b>
				</g:if>
				<g:if test="${diagnosticOrderInstance?.block}">
                                    <b>
				<li class="fieldcontain">
					<span id="block-label" class="property-label"><g:message code="default.block" default="Block" /></span>
                                        <span class="property-value" aria-labelledby="block-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="block"/></span>
				</li>
                                </b>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.priority}">
                                    <b>
				<li class="fieldcontain">
					<span id="priority-label" class="property-label"><g:message code="default.priority" default="Priority" /></span>

						<span class="property-value" aria-labelledby="priority-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="priority"/></span>
				</li>
                                </b>
				</g:if>

                                <g:if test="${diagnosticOrderInstance?.status}">
                                    <b>
				<li class="fieldcontain">
					<span id="priority-label" class="property-label"><g:message code="default.status.label" default="Status" /></span>

						<span class="property-value" aria-labelledby="status-label"><g:message code="default.fish.status.${diagnosticOrderInstance?.status}" /></span>
				</li>
                                </b>
				</g:if>

                                <g:if test="${diagnosticOrderInstance?.dateCreated}">
                                    <b>
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="default.date.created" default="dateCreated" /></span>

						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${diagnosticOrderInstance.dateCreated}" type="datetime" style="LONG" timeStyle="SHORT"/></span>
				</li>
                                </b>
				</g:if>

<!--
				<g:if test="${diagnosticOrderInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="diagnosticOrder.status.label" default="Status" /></span>

						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="status"/></span>

				</li>
				</g:if>
-->

<!--
				<g:if test="${diagnosticOrderInstance?.note}">
				<li class="fieldcontain">
					<span id="note-label" class="property-label"><g:message code="diagnosticOrder.note.label" default="Note" /></span>

						<span class="property-value" aria-labelledby="note-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="note"/></span>

				</li>
				</g:if>

				<g:if test="${diagnosticOrderInstance?.reason}">
				<li class="fieldcontain">
					<span id="reason-label" class="property-label"><g:message code="diagnosticOrder.reason.label" default="Reason" /></span>

						<span class="property-value" aria-labelledby="reason-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="reason"/></span>

				</li>
				</g:if>

-->
				<g:if test="${diagnosticOrderInstance?.supportingInformation}">
                                    <b>
				<li class="fieldcontain">
					<span id="supportingInformation-label" class="property-label"><g:message code="default.supporting.information" default="Supporting Information" /></span>
<!--
						<span class="property-value" aria-labelledby="supportingInformation-label"><g:fieldValue bean="${diagnosticOrderInstance}" field="supportingInformation"/></span>
-->
                                                <span class="property-value" aria-labelledby="supportingInformation-label"><g:textArea readonly="readonly" name="supportingInformation" style="width:100%; height:50%" value="${fieldValue(bean: diagnosticOrderInstance, field: "supportingInformation")}"/></span>



				</li>
                                </b>
				</g:if>
<!--
				<g:if test="${diagnosticOrderInstance?.items}">
				<li class="fieldcontain">
					<span id="item-label" class="property-label"><g:message code="diagnosticOrder.item.label" default="Item" /></span>

						<g:each in="${diagnosticOrderInstance.items}" var="i">
						<span class="property-value" aria-labelledby="item-label"><g:link controller="diagnosticOrderItem" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></span>
						</g:each>

				</li>
				</g:if>
-->

</div>   <!-- fields diag order -->




    <div style="padding-left: 80px; padding-right: 80px;">
                        <table>
                        <thead>
                            <th><g:message code="default.gene" default="Gene" /></th>
                            <th><g:message code="default.procedure" default="Procedure" /></th>
                            <th><g:message code="default.locus" default="Locus" /></th>
                            <th><g:message code="default.tissue.type" default="Tissue type" /></th>
                            <th><g:message code="default.status" default="Status" /></th>
                        </thead>
                            <tbody>
                                    <tr>
                                        <td>${diagnosticOrderItemInstance.probe.gene.name}</td>
                                        <td>${diagnosticOrderItemInstance.probe.procedure}</td>
                                        <td>${diagnosticOrderItemInstance.probe.gene.locus}</td>
                                        <td>${diagnosticOrderItemInstance.probe?.tissueType?.name}</td>
<!--
                                        <td>TODO: UPDATE STATUS</td>
-->
                                        <td><g:message code="default.fish.status.${diagnosticOrderItemInstance?.status}" /></td>

                                    </tr>
                            </tbody>
                        </table>
    </div>
<br/><br/><br/>
    <div style="padding-left: 80px; padding-right: 80px;">
                        <table>
                        <thead>
                            <th><g:message code="default.probe" default="Probe" /></th>
                            <th><g:message code="default.code" default="Code" /></th>
                            <th><g:message code="default.description" default="Description" /></th>
                            <th><g:message code="default.note" default="Note" /></th>
                            <th><g:message code="default.manufacturer" default="Manufacturer" /></th>
                        </thead>
                            <tbody>
                                    <tr>
                                        <td>${diagnosticOrderItemInstance.probe.name}</td>
                                        <td>${diagnosticOrderItemInstance.probe.code}</td>
                                        <td>${diagnosticOrderItemInstance.probe.description}</td>
                                        <td>${diagnosticOrderItemInstance.probe?.note}</td>
                                        <td>${diagnosticOrderItemInstance.probe?.manufacturer?.name}</td>
                                    </tr>
                            </tbody>
                        </table>
    </div>


<!--
    public static final String DRAFT = "draft"

    public static final String REQUESTED = "requested"

    public static final String ACCEPTED = "accepted"

    public static final String SUSPENDED = "suspended"

    /** In case of step non-conformity */
    public static final String PAUSED = "paused"

    public static final String IN_PROGRESS = "in-progress"

    public static final String REVIEW_LABORATORY_PREPARATION = "reviewLaboratoryPreparation"

    public static final String REVIEW_DIAGNOSTIC_EVALUATION = "reviewDiagnosticEvaluation"

    public static final String REPORT_SIGNED = "reportSigned"

    public static final String REPORT_SENT = "reportSent"

    public static final String COMPLETED = "completed"
-->

                <%

//                    List<Integer> validityYears = [1,2,3,4,5].collect { new Date()​[Calendar.YEAR]​ + it }​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
//                    def validityYears = [1,2,3,4,5].collect {it}​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
//                    def validityYears = [2016, 2017, 2018, 2019, 2020]
                    def now = Calendar.instance
                    def lastYear = now[Calendar.YEAR]-1
                    def validityYears = now[Calendar.YEAR]-1..now[Calendar.YEAR]+10

/*
def now = Calendar.instance

def nextYear = now[YEAR] + 4
def oneYearFromNow = now.copyWith(year: nextYear)
println now.time
println oneYearFromNow.time​
*/

                %>

<%
    if (session.user.role in [User.TECHNICAL_ASSISTANT]
//    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.REQUESTED, DiagnosticOrder.LABORATORY_PREPARATION, DiagnosticOrder.REVIEW_LABORATORY_PREPARATION]
    ) {
%>
            <div class="container">
                <div class="">
                    <hr/>
    <!--
                    <h2>Extra Info</h2>
    -->
    
                    <h3><g:message code="default.probe" default="Probe" /></h3>

                    <br/>
                    <div class="form-group">
                        <label class="control-label" for="result"><g:message code="default.lote" default="Supporting Information" /></label>
                        <div class="">
                            <g:textField
                                class="form-control" style="width:100px;"
                                max="100"
                                min="0"
                                required=""
                                name="probeLoteNumber"
                                value="${fieldValue(bean: diagnosticOrderItemInstance, field: "probeLoteNumber")}"
                                disabled="${!(diagnosticOrderItemInstance?.status in [DiagnosticOrder.LABORATORY_PREPARATION, DiagnosticOrder.REQUESTED])} "/>
                            <g:renderErrors bean="${diagnosticOrderItemInstance}" as="list" field="probeLoteNumber"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="probeExpirationDate"><g:message code="default.expiration.date" default="Supporting Information" /></label>
                        <div class="">
                            <g:textField
                                class="form-control" style="width:100px;"
                                id="probeExpirationDate"
                                name="probeExpirationDate"
                                pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}"
                                required="true"
                                value="${formatDate(date: diagnosticOrderItemInstance.probeExpirationDate, format: 'dd/MM/yyyy')}"
                                readonly="${!(diagnosticOrderItemInstance?.status in [DiagnosticOrder.LABORATORY_PREPARATION, DiagnosticOrder.REQUESTED])}"/>
                            <g:renderErrors bean="${diagnosticOrderItemInstance}" as="list" field="probeExpirationDate"/>
<!--
            <g:textField
         pattern="d{2}/\\d{2}/\\d{4}"
         name="cellPhone1"/>

                            <g:textField id="probeExpirationDate" name="probeExpirationDate" required="" value="${diagnosticOrderItemInstance?.probeExpirationDate}" pattern="^d{2}/\\d{2}/\\d{4}\$" />
            -->
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="result"><g:message code="default.comment" default="Comment" /></label>
                        <div class="">
                            <!-- readonly="readonly" -->
                            <g:textArea name="laboratoryPreparationComment" style="width:100%; height:50%"
                                value="${fieldValue(bean: diagnosticOrderItemInstance, field: "laboratoryPreparationComment")}"
                                readonly="${!(diagnosticOrderItemInstance?.status in [DiagnosticOrder.LABORATORY_PREPARATION, DiagnosticOrder.REQUESTED])} "/>
                        </div>
                    </div>
                </div>
            </div>
<%
}
%>

<%
//if ((!(diagnosticOrderItemInstance?.status in [
//                DiagnosticOrder.REQUESTED,
//                DiagnosticOrder.IN_PROGRESS,
//                DiagnosticOrder.CANCELLED
//            ])) ||
//    ((session.user.role in [
//                User.ASSISTANT_MEDICAL_DOCTOR,
//                User.MEDICAL_DOCTOR]))) {

    if (
//        (session.user.role in [User.REQUESTER_MEDICAL_DOCTOR]
//    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.COMPLETED]) ||

//    (session.user.role in [User.TECHNICAL_ASSISTANT]
//    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.COMPLETED]) ||

    (session.user.role in [User.ASSISTANT_MEDICAL_DOCTOR]
    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.REVIEW_LABORATORY_PREPARATION, DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION, DiagnosticOrder.COMPLETED]) ||
    (session.user.role in [User.MEDICAL_DOCTOR]
    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION, DiagnosticOrder.COMPLETED])) {
%>

                            <br/><br/>

        <div class="container">

            <h3>${fieldValue(bean: diagnosticOrderItemInstance.probe.gene, field: "name")}</h3>
            <br/>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label" for="changeStatus">${message(code: 'default.probe.type', default: 'Probe type')}</label>
                        <div class="">
                            <g:select onchange="statusChange(${diagnosticOrderItemInstance.id});" class="form-control" title="Change status" name="change_${diagnosticOrderItemInstance.id}" from="${diagnosticOrderItemInstance.CHANGE_STATUS}"  value="${fieldValue(bean: diagnosticOrderItemInstance, field: "change")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                        </div>
                    </div>

                    <div id="noAmplification_${diagnosticOrderItemInstance.id}" style="display:block">

                        <div class="form-group">
                            <label class="control-label" for="nucleousCount">${message(code: 'default.nucleous.count', default: 'Nucleous Count')}</label>
                        <div class="">
                                <g:field type="number" class="form-control" name="nucleousCount_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "nucleousCount")}" onchange="percentageNucleousCountPositive(${diagnosticOrderItemInstance.id});" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="positiveNucleousCount">${message(code: 'default.positive.nucleous.count', default: 'Positive nucleous count')}</label>
                            <div class="">
                                <g:field type="number" class="form-control" name="positiveNucleousCount_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "positiveNucleousCount")}" onchange="percentageNucleousCountPositive(${diagnosticOrderItemInstance.id})" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="percentageNucleousCountPositive">${message(code: 'default.percentage.positive.nucleous.count', default: '% positive nucleous count')}</label>
                            <div class="">
                                <g:field type="number" max="100" min="0" readonly="readonly" class="form-control" name="percentageNucleousCountPositive_${diagnosticOrderItemInstance.id}" value="${(fieldValue(bean: diagnosticOrderItemInstance, field: "percentageNucleousCountPositive").replace(',' ,'.') as Double).round()}" />
                            </div>
                        </div>
                    </div>

                    <div id="amplification_${diagnosticOrderItemInstance.id}" style="display:none">

                        <div class="form-group">
                            <label class="control-label" for="centromerCount">${message(code: 'default.centromere.count', default: 'Centromere count')}</label>
                            <div class="">
                                <g:field type="number" class="form-control"  required="" name="centromerCount_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "centromerCount")}" onchange="ratioCentromerProbeCount(${diagnosticOrderItemInstance.id})" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="locusSpecificProbeCount">${message(code: 'default.probe.count', default: 'Probe count')}</label>
                            <div class="">
                                <g:field type="number" class="form-control"  required="" name="locusSpecificProbeCount_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "locusSpecificProbeCount")}" onchange="ratioCentromerProbeCount(${diagnosticOrderItemInstance.id})" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="ratio">${message(code: 'default.ratio', default: 'Probe count')}</label>
                            <div class="">
                                    <g:field readonly="readonly" class="form-control" name="ratio_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "ratio")}" />

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="percentageNotCountableNucleous">${message(code: 'default.percentage.not.countable', default: '% not countable')}</label>
                        <div class="">
                            <g:field type="number" class="form-control" max="100" min="0"  required="" name="percentageNotCountableNucleous_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "percentageNotCountableNucleous")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="result">${message(code: 'default.result', default: 'Result')}</label>
                        <div class="">

                            <g:select class="form-control" title="result" name="result_${diagnosticOrderItemInstance.id}" from="${diagnosticOrderItemInstance.RESULT_STATUS}"  value="${fieldValue(bean: diagnosticOrderItemInstance, field: "result")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                        </div>
                    </div>
                </div>

                <div class="col-md-5" style="background-color: whitesmoke">
                    <g:each in="${diagnosticOrderItemInstance.uploadedFiles.sort { it.id }}" status="j" var="uploadedFile">
                        <div class="test">
                            <div class="images-containers">
                                    <%
                                        if (allowedToEdit || (session.user.role=="Administrator")) {
                                            %>
<div>
        &nbsp
                                    <a href="#" onclick="!formAlteredCheck() && confirm('This image will be deleted permanently. Are you sure?') && deleteFile(this);" id="${uploadedFile.id}" class="delete"></a>
        </div>
                                    <%
                                        }
                                    %>
                                    <div class="fish-image" style="background-image:url(${createLink(controller:'diagnosticOrder', action:'getImage')}?filepath=${uploadedFile.filepath});" onclick="openImageGallery('${"image_" + uploadedFile.id}')">

                                </div>
                            </div>
                        </div>
                    </g:each>

                    <%
                        if((allowedToEdit) || (session.user.role == User.ADMINISTRATOR)) {
                    %>
                    <img title="Upload image" src="${createLinkTo(dir:'images',file:'uploadFile.png')}" onclick="!formAlteredCheck() && $('#item_${diagnosticOrderItemInstance.id}').click();"/>
                    <div class="image-upload">
                        <label for="item_${diagnosticOrderItemInstance.id}">
                            <!--
                            <img src="http://goo.gl/pB9rpQ"/>
                            -->
                        </label>
                    <!--
                        <input id="file-input" type="file"/>
                    -->
                        <input class="item_" id="item_${diagnosticOrderItemInstance.id}" id2="item_${diagnosticOrderItemInstance.id}" name="item_${diagnosticOrderItemInstance.id}" type="file" onchange="uploadFile(this);" class="upload" style="display:none" <%=(diagnosticOrderItemInstance.status=='completed')?'disabled':''%> />

                    </div>
                    <%
                        }
                    %>

                </div>
            </div>

            <br/>
            <div class="col-md-5">
                <%
                    if (allowedToEdit || (session.user.role == User.ADMINISTRATOR)) {
                %>
                <input type="button" id="button" onclick="showhide(this, 'comment_div_${diagnosticOrderItemInstance.id}');" value="${diagnosticOrderItemInstance.comment?g.message(code: 'default.hide.comment'):g.message(code: 'default.show.comment')}"/>
                <br/><br/>
                <%
                    }
                %>
                <div style="display:${diagnosticOrderItemInstance.comment?"block":"none"};" id="comment_div_${diagnosticOrderItemInstance.id}" class="comments">
                    <g:textArea style="width:100%; height:50%" name="comment_${diagnosticOrderItemInstance.id}" value="${fieldValue(bean: diagnosticOrderItemInstance, field: "comment")}" disabled="${(session.user.role!="Administrator") && (diagnosticOrderItemInstance.status=="completed")}"/>
                </div>
            </div>
        </div>

<%
}
%>

			</ol>
				<g:hiddenField name="version" value="${diagnosticOrderItemInstance?.version}" />
            </div> <!-- id="dataFields" --> 

<br/><br/>
				<fieldset class="buttons" id="formButtons" >
                                    <!--
                style = "display:none"
					<g:link class="edit" action="edit" resource="${diagnosticOrderInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                                        <g:link class="delete" action="delete" resource="${diagnosticOrderInstance}"><g:message code="default.button.delete.label" default="Delete" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="javascript:return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
-->


<%
//    if((diagnosticOrderItemInstance.status != DiagnosticOrder.COMPLETED) || (session.user.role == User.ADMINISTRATOR)) {
    if (!(diagnosticOrderInstance.status in [DiagnosticOrder.CANCELLED, DiagnosticOrder.COMPLETED]) || (session.user.role == User.ADMINISTRATOR)) {

if ((availableStatus==null) || (availableStatus!="noStatusAvailable")) {

%>

<!--

status          : <%=diagnosticOrderItemInstance?.status %>

<br/>
<br/>
availableStatus :  <%=availableStatus %>

-->
	<g:select name="status" from="${availableStatus}" required="" value="${diagnosticOrderItemInstance?.status}" valueMessagePrefix="diagnosticOrderItem.status"/>
<%
} else {
%>
        <g:select name="status" from="${[diagnosticOrderItemInstance?.status]}" required="" value="${diagnosticOrderItemInstance?.status}" valueMessagePrefix="diagnosticOrderItem.status"/>
<%
}

if (session.user.role == User.ASSISTANT_MEDICAL_DOCTOR) {
%>

<%
}
%>
                                <g:actionSubmit class="save" id="updateDOI" name="oijoji" action="updateDOI" value="${message(code: 'default.button.save.label', default: 'Save')}" onclick="javascript:return confirm('${message(code: 'default.button.confirm.message', default: 'Are you sure?')}');" />

<%
//}
if (session.user.role == User.ADMINISTRATOR) {
%>

                                <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="javascript:return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure ADMIN?')}');" />




<%
}
%>

<%
}
%>

				</fieldset>
			</g:form>

<g:workflowButtons diagnosticOrderItem="${diagnosticOrderItemInstance}"/>


<!--
<g:actionSubmit class="delete" action="printLaboratoryPreparation" value="Prinssdt" />
-->

<!--
<g:form>
    <fieldset>
-->

<!--
    </fieldset>
</g:form>
-->


<!--
<div id="linkssss">
    <a id="banana" href="http://placehold.it/3500x1500" title="Banana" data-gallery>
        <img src="http://placehold.it/350x150" alt="Banana">
    </a>
    <a id="apple" href="http://placehold.it/3500x1500" title="Apple" data-gallery>
        <img src="http://placehold.it/350x150" alt="Apple">
    </a>
    <a id="orange" href="http://placehold.it/3500x1500" title="Orange" data-gallery>
        <img src="http://placehold.it/350x150" alt="Orange">
    </a>
    <a id="batata" href="http://placehold.it/3500x1500" title="batata" data-gallery>

                                        <div class="fish-image" style="background-image:url(/pathoHL7/diagnosticOrder/getImage?filepath=//Users//tanovsky//pathoHL7imagesRepository//fish-cish////R-10000-15_HER2_4a2b14d7-a3c0-4ac1-b0c1-c0b57e3344e2.jpg);">

                                    <a href="#" onclick="confirm('This image will be deleted permanently. Are you sure?') && deleteFile(this);" id="98" class="delete"></a>

                                </div>

    </a>
</div>
-->


<script>
//    function isStatusDifferent(oldStatus, newStatus) {
//        var x = document.forms["myForm"]["fname"].value;
//        if (x == null || x == "") {
//            alert("Status is the same");
//            return false;
//        }
//    }

    function openImageGallery(i) {
        var objectClicked = '#'.concat(i);
        $(objectClicked).click();
    }

<!--
$('.clickClass').click(function(){
    var oData = new FormData(document.forms.namedItem("fileinfo"));
    var url="${createLink(controller:'diagnosticOrder', action:'uploading')}";
     $.ajax({
         url:url,
         type:'POST',
         data:oData,
         processData: false,  // tell jQuery not to process the data
         contentType: false ,
         success:function (req) {
               alert(req);
            }
         });
});
-->

</script>

<script>

//    function setGUIStatus(status) {
////        alert(document.getElementById("status").value);
////        alert(document.getElementById("status").value);
//        $('#status').val(status);
//        $('#updateDOI').click();
//
//    }

function backToPreviousStatus(id, reportURL) {
    var reasonForNonConformity = prompt("Reason for non conformity: ", "");
    if (reasonForNonConformity) {
//    alert(reasonForNonConformity);
//    var url="${createLink(controller:'diagnosticOrderItem', action:'nonconformity', id: id)}?description=" + escape(reasonForNonConformity);
    var url="${createLink(controller:'diagnosticOrderItem', action:'nonconformity')}?id="+id+"&description=" + escape(reasonForNonConformity);
    window.open(url, '_self', false)
    }
}

function isValidDate(dateString) {
    var regEx = /^\d{2}\/\d{2}\/\d{4}$/ ;
    return regEx.test(dateString);
}

function passingAllRequiredFields() {
    var passAllFields = Boolean(true);

<%
if (session.user.role in [User.TECHNICAL_ASSISTANT]) {
%>

    //Boolean($('#probeExpirationDate').length)
    var validExpirationDate = ($('#probeExpirationDate').val().length != 0) && Boolean(isValidDate($('#probeExpirationDate').val()));
    var validProbeLoteNumber = Boolean($('#probeLoteNumber').length) && ($('#probeLoteNumber').val().length != 0);
    if ( !validProbeLoteNumber || !validExpirationDate) {
        var msg = "${message(code: 'default.please.fill.all.required.fields', default: 'Please fill all required fields')}";
        alert(msg);
        passAllFields = Boolean(false);
    }

<%
}
%>

    return passAllFields;
}

    function updateStatus(status) {
        console.log("updateStatus");
        var update = false
        if (passingAllRequiredFields()) {
            update = confirm('${message(code: 'default.button.confirm.message', default: 'Are you sure?')}');
            if (update) {
                $('#status').val(status);
                document.getElementById('uau').submit();
            }
        }
//        alert("update : " + update);
        return update
    }

    function openReportToPrint(reportURL) {
        window.open(reportURL, '_blank');
    }

    function updateStatusAndOpenReportToPrint(status, reportURL) {
        if (updateStatus(status)) {
//            alert(reportURL);
            openReportToPrint(reportURL);
        }
    }

/*
    function updateStatusAndOpenReportToPrint(status, reportURL) {
        console.log("updateStatusAndOpenReportToPrint");
        var update = true
        if (passingAllRequiredFields()) {
//            $('#updateDOI').click();
            var confirmation = confirm('${message(code: 'default.button.confirm.message', default: 'Are you sure?')}')
            if (confirmation) {
                $('#status').val(status);
//                $( "#uau" ).submit();
//alert($("#uau")[0].i);
//                $("#uau")[0].submit();
alert(document.getElementById('uau'));
document.getElementById('uau').submit();
//                document.uau.submit()
                window.open(reportURL, '_blank');
            }
        }
    }

*/

//$('#imageFile').on("change", function(){ uploadFile(); });

function formAlteredCheck() {
//    alert("fui clicado");
    if (formAltered) {
        var msg = "${message(code: 'default.data.was.changed', default: 'DiagnosticOrder')}.\n${message(code: 'default.please.save.before.proceeding', default: 'DiagnosticOrder')}.";
        alert(msg);
    };
    return formAltered;
}

function deleteFile(fileObject) {
        var fileInput = document.getElementById(fileObject.id);

        var oData = new FormData();
        oData.append("id", fileInput.id);

//        alert(oData);
        var url="${createLink(controller:'diagnosticOrderItem', action:'delimage')}";
//        alert("esta a funcar ate aki");

        $.ajax({
        type:'POST',
        data: oData,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        url: url,
        success:function(data, textStatus){
//            alert("sucesso! ");
            location.reload();
//            jQuery('#storedvideos').html(data);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            alert("failure?! "+ XMLHttpRequest.responseText);
            location.reload();
        }
        });
}
                                            
    function enoughImagesUploaded() {
        var enough;
                                            
<%
        if(session.user.role != User.ASSISTANT_MEDICAL_DOCTOR) {
%>
            enough = true;
<%
        } else {
%>
            enough = ${enoughImagesUploaded};
<%
        }
%>
        if (!enough) {
            alert("${message(code: 'default.not.enough.images.uploaded', args : ['3'])}");
        }
        return enough;
    };
                                            

function uploadFile(fileObject) {

var validFile = true;

//        alert("entre em uploadFile! "+fileObject.name);
//        var oData = new FormData(document.forms.namedItem(fileObject.name));
//        var oData = new FormData($(fileObject.name).closest("form"));


//var fileInput = document.getElementById(fileObject.name);

//var fileInput = document.getElementByName(fileObject.name);
//var fileInput = $("input[name=fileObject.name]");

//alert(fileObject.name);

var fileInput = document.getElementsByName(fileObject.name)[0];

var file = fileInput.files[0];


//var w = $(fileObject.name).width();
//alert(fileObject);
//alert("Height" + ${grailsApplication.config.application.fish.images.height});
//alert("width" + ${grailsApplication.config.application.fish.images.width});

               //Initiate the FileReader object.
                var reader = new FileReader();
                //Read the contents of Image File.
//                reader.readAsDataURL(fileUpload.files[0]);

                reader.readAsDataURL(file);


//                http://www.aspsnippets.com/Articles/Validate-Check-dimensions-Height-and-Width-of-Image-before-Upload-using-HTML5-JavaScript-and-jQuery.aspx
                reader.onload = function (e) {
                    //Initiate the JavaScript Image object.
                    var image = new Image();
                    //Set the Base64 string return from FileReader as source.
                    image.src = e.target.result;
                    image.onload = function () {
                        //Determine the Height and Width.
                        var height = this.height;
                        var width = this.width;
                        var allowedHeight = "${grailsApplication.config.application.fish.images.height}";
                        var allowedWidth = "${grailsApplication.config.application.fish.images.width}";
//                        alert(Boolean(${grailsApplication.config.application.fish.images.width}));
                        if (((Boolean(allowedHeight)) && (Boolean(allowedWidth))) && ((allowedHeight != height) ||
                            (allowedWidth != width))) {
                            // alert(allowedWidth + " != " + width + " px and "+ allowedHeight + " != " + height + "px.");
                            alert("Allowed Dimension for Images are Width : " + allowedWidth + " px \n and  Height : " + allowedHeight + " px." )
                            validFile = false;
//                            return false;
                        } else {
//                            alert("Uploaded image has valid Height and Width.");

if (validFile) {
        var oData = new FormData();
        oData.append("file", file);
        oData.append("itemId", fileObject.name);

        var url="${createLink(controller:'diagnosticOrderItem', action:'testando')}";
        $.ajax({
        type:'POST',
        data: oData,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        url: url,
        success:function(data, textStatus){
//            alert("sucesso! ");
            location.reload();
//            jQuery('#storedvideos').html(data);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            alert("failure: "+ XMLHttpRequest.responseText);
            location.reload();
        }
        });
    }

//                        return true;
                        }
                    }
                }

}

</script>

<!--
<div id="storedvideos"></div>

<div id="drop_zone">Drop files here</div>
<output id="list"></output>

-->

<script>
//  function handleFileSelect(evt) {
//
//    var files = evt.target.files; // FileList object
//
//    // Loop through the FileList and render image files as thumbnails.
//    for (var i = 0, f; f = files[i]; i++) {
//
//      // Only process image files.
//      if (!f.type.match('image.*')) {
//        continue;
//      }
//
//      var reader = new FileReader();
//
//      // Closure to capture the file information.
//      reader.onload = (function(theFile) {
//        return function(e) {
//          // Render thumbnail.
//          var span = document.createElement('span');
//          span.innerHTML = ['<img class="thumb" src="', e.target.result,
//                            '" title="', escape(theFile.name), '"/>'].join('');
//          document.getElementById('list').insertBefore(span, null);
//        };
//      })(f);
//
//      // Read in the image file as a data URL.
//      reader.readAsDataURL(f);
//    }


//    evt.stopPropagation();
//    evt.preventDefault();
//
//    var files = evt.dataTransfer.files; // FileList object.
//
//    // files is a FileList of File objects. List some properties.
//    var output = [];
//    for (var i = 0, f; f = files[i]; i++) {
//      output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
//                  f.size, ' bytes, last modified: ',
//                  f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
//                  '</li>');
//    }
//    document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
//  }

//  function handleDragOver(evt) {
//    evt.stopPropagation();
//    evt.preventDefault();
//    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
//  }
//
//  // Setup the dnd listeners.
//  var dropZone = document.getElementById('drop_zone');
//  dropZone.addEventListener('dragover', handleDragOver, false);
//  dropZone.addEventListener('drop', handleFileSelect, false);
</script>
		</div>
                                                

                                                

<script>

$('#probeExpirationDate').mask('00/00/0000', {
    translation: {
    }, placeholder: "__/__/____"});


<%    if (session.user.role in [User.ASSISTANT_MEDICAL_DOCTOR, User.MEDICAL_DOCTOR]
    & !(diagnosticOrderItemInstance?.status in [
            DiagnosticOrder.REQUESTED,
            DiagnosticOrder.LABORATORY_PREPARATION,
//            DiagnosticOrder.REVIEW_LABORATORY_PREPARATION
        ])) {
%>

    statusChange(${diagnosticOrderItemInstance.id});
    percentageNucleousCountPositive(${diagnosticOrderItemInstance.id});
    ratioCentromerProbeCount(${diagnosticOrderItemInstance.id});
//    updateAllStatus();

<%
}
%>

var formAltered = false;
$('#uau').change(function() {
    formAltered = true;
});


//$('#formButtons').hide();
$('#status').hide();
                                            
<%
if (session.user.role != User.ASSISTANT_MEDICAL_DOCTOR || !allowedToEdit)  {
%>
$('#updateDOI').hide();
<%
}
%>
</script>
                                                
                                                
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>

<script>
                
    $("#dataFields :input").attr("disabled", ${!allowedToEdit});
//    $("#uau :input").attr("disabled", ${!allowedToEdit});
//                        $("#uau :input").attr("disabled", true);
</script>
<!--
<script src="js/bootstrap-image-gallery.min.js"></script>
-->
<!--
<script src="cdn.jsdelivr.net/bootstrap.image-gallery/3.1.0/js/bootstrap-image-gallery.min.js"></script>
-->


<!--
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="js/jquery.image-gallery.min.js"></script>
-->

	</body>
</html>
