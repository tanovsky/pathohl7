<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
    <head>
        <style>

.some-class {
    height: 50px;
//    width: 10px;
//    background-color: powderblue;
}

body {
//    background-color: yellow;
    padding-left: 30px;
    padding-right: 10px;
    padding-bottom: 100px;
    padding-top: 10px;
}

.footer {
    position: fixed;
    bottom: 0px;
    left: 100px;
}

.images {
//   background-color: yellow;
    border-style: solid;
    border-width: 0px;
   padding: 0px;
//   display: inline-block;
}

/* Single-direction drop shadow .style-four */

hr {
    border-style: solid;
    border-color: black;
#    border-width: 1px 0 0 0;
    border-width: 0px 0 0 0;
}

.img {
//    width: 100px;
//    max-width:25px; /* or whatever width you want. */
}

.thumbnail {
//    width: 75px;
    height: 100px;
padding: 5px;
//    max-height: 100px;
//    max-width: 100px;
}

.logoMajor {
    width:100px;
}

.logoMinor {
    width:80px;
//    width: 80px;
//    height: 125px;

//    max-height: 100px;
//    max-width: 100px;
}

//.imagerendering {
//    max-width:100%;
//}


div {
    margin-bottom: 8px;
}

#patientData {
  display: flex;
}

#patientData div {
  flex-grow: 0;
  flex-shrink: 0;
  flex-basis: 40px;
}

////	.label { display: table-cell; }
//
//.label {
////    display: table-caption;
//    display: inline;
////    display: inline-block;
////    display: table-cell;
//    width: 150px;
//    text-align: left;
//   white-space:nowrap;
//    overflow:hidden;
//}

.justify {
    display: flex;
    justify-content: space-between;
    align-items: center;
}



//#contactContent { margin-top: 20px; margin-left: 20px;}
//label{
//    display:inline-block;
//    width:100px;
//}
//
//.input {
//    border: none;
//    background-color: #000;
//    margin-left: 20px;
//    margin-bottom: 5px;
//    padding-right: 50px;
//    font-size: large;
//    font-weight: bold;
//}




.labelValuePairContent { margin-top: 20px; margin-left: 20px;}
.value {
    border: none;
//    background-color: #000;
    margin-left: 20px;
    margin-bottom: 5px;
    padding-right: 0px;
    font-size: large;

}

.label {
    display:inline-block;
    width:200px;
    margin-bottom: 5px;
    font-weight: bold;
}
        </style>
    </head>
    <body>

<!--
        <p>This is the example of <span style="color:green">span tag</span> and the <span style="color:red">div tag</span> alongwith CSS</p>

        <p>

<%
10.times {
    out << 'sssWelcome '
}
%>

Congratulations, you have successfully started your first Grails application! At the moment this is the default page, feel free to modify it to either redirect to a controller or display whatever
            content you may choose. Below is a list of controllers that are currently deployed in this application,
            click on each to execute its default action.</p>
-->

        <div id="header" style="clear:both;width:100%;">

            <div style="float:left;margin:10px;">
                <rendering:inlineJpeg bytes="${logoMajorBytes}" class="logoMajor"/>
            </div>

            <div style="float:right;margin:10px;">
                <rendering:inlineJpeg bytes="${logoMinorBytes}" class="logoMinor"/>
            </div>

            <div style="float:left;margin:10px;width:250px;font-size:15px;font-weight: bold;">
                <br/>
                Molekularpathologische Diagnostik<br/>
                Pathologisches Institut<br/>
                Universitätklinikum Heidelberg
                <br/>
            </div>
        </div>
        <br/><br/><br/><br/><br/><br/><hr/>

<div id="requester">
    <div class="labelValuePairContent">
        <label class="label"><font>Anforderer :</font> </label><label class="value">${requester}</label><br/>
        <label class="label">Anforderungsdatum :</label><label class="value">${dateOfReceivedRequest}</label><br/>
<!--
        <label class="label">Datum <font>RequestCompleted : </font></label><label class="value">${dateOfRequestCompleted}</label>
-->
    </div>
</div>




<div id="patient">
    <h2>Patient</h2>
<div id="patientData" style="clear:both;width:100%;">
    <div class="labelValuePairContent" style="float:left;">
        <label class="label">eNumber : </label><label class="value">${eNumber}</label><br/>
        <label class="label">Block : </label><label class="value">${blockNumber}</label>
    </div>

    <div id="barcode" style="float:right;margin:10px;width:250px;">
        <rendering:inlineJpeg bytes="${barcodeBytes}" />
        <center>
            <div><b>${rawENumber}</b></div>
        </center>
    </div>
</div>

<br/><br/><br/><br/>
</div>

<hr/>

<div id="results">
    <h2>Laborvorbereitung</h2>
    <div class="labelValuePairContent">
        <label class="label">Probe : </label><label class="value">${probe}</label><br/>
    </div>
    <div class="labelValuePairContent">
        <label class="label">Lote Nummer : </label><label class="value">${loteNumber}</label><br/>
    </div>
    <div class="labelValuePairContent">
        <label class="label">Haltbarkeitsdatum : </label><label class="value">${probeExpirationDate}</label><br/>
    </div>

<!---
    <div class="labelValuePairContent">
        <label class="label" style="color:red;">lote Expire Date : </label><label class="value">${loteExpireDate}</label><br/>
    </div>
-->
    <div class="labelValuePairContent">
        <label class="label">Hersteller : </label><label class="value">${manufacturer}</label><br/>
    </div>
    <div class="labelValuePairContent">
        <label class="label">Wiederholung : </label><label class="value">${(repeated as Boolean)?"Ja":"Nein"}</label><br/>
    </div>
<% if (laboratoryPreparationComment) { %>
    <div class="labelValuePairContent">
        <label class="label">Kommentar : </label><label class="value">${laboratoryPreparationComment}</label><br/>
    </div>
<% } %>
</div>

<hr/>

<div id="footer" class="footer">
    <div class="labelValuePairContent">
        <label class="label" style="width:200px;"><font>Technischer Assistent </font> : </label><label class="value"  style="width:100%;">____________________________________</label><br/>
        <label class="label" style="width:200px;"></label>&nbps;<label class="value">(${dateNow}, <font>${technicianName}</font>)</label><br/>
    </div>
</div>


    </body>
</html>