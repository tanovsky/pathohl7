import de.uni_heidelberg.iph.DiagnosticOrderItem
import de.uni_heidelberg.iph.DiagnosticOrder
import de.uni_heidelberg.iph.User


class PathoTagLib {
    def barcodeGeneratorService

//    static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]


//    https://designshack.net/articles/javascript/25-useful-resources-for-creating-tooltips-with-javascript-or-css/

//https://codepen.io/thousand/pen/FtxHw
//    https://github.com/igorescobar/jQuery-Mask-Plugin
//https://igorescobar.github.io/jQuery-Mask-Plugin/
//http://jsfiddle.net/igorescobar/6pco4om7/
//    http://doptiq.com/smart-forms/demos/samples/flat/masking.html
//https://www.jqwidgets.com/jseditor/?key=jqwidgets-mask-input-template
//http://robinherbots.github.io/Inputmask/

//String stripAccents(String s)  {
//    s = Normalizer.normalize(s, Normalizer.Form.NFD)
//    s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
//    return s
//}

//println stripAccents("É123123123"​)​

    def formatENumber = { attrs ->
        out << barcodeGeneratorService.format(attrs.eNumber)
    }



    def workflowButtons = { attrs ->
        DiagnosticOrderItem diagnosticOrderItemInstance = attrs.diagnosticOrderItem
        StringBuffer sb = new StringBuffer()
        sb << """
<div id="buttonsShortcuts" style="float: right;margin-right:50px;">
"""
    if (session.user.role in [User.TECHNICAL_ASSISTANT]
    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.REQUESTED]) {

sb << """
<!--
${message(code: 'default.workflow.status.taskNonConformant', default: 'Non-conformant')}
${message(code: 'default.workflow.status.taskCompleted', default: 'Done')}
${message(code: 'default.workflow.status.workflowCompleted', default: 'Completed')}
${message(code: 'default.workflow.status.printReport', default: 'Print')}
-->
    <button type="button" class="btn btn-success glyphicon glyphicon-ok" onclick="updateStatus('laboratoryPreparation', '${createLink(controller:'diagnosticOrderItem', action:'laboratoryPreparationPDF')}/${diagnosticOrderItemInstance.id}');" data-toggle="tooltip" data-placement="bottom" title="Laboratory Preparation">&nbsp${message(code: 'default.workflow.status.taskCompleted', default: 'Done')}</button>
<!--
    <button type="button" class="btn btn-success glyphicon glyphicon-ok" onclick="updateStatusAndOpenReportToPrint('laboratoryPreparation', '${createLink(controller:'diagnosticOrderItem', action:'laboratoryPreparationPDF')}/${diagnosticOrderItemInstance.id}');" data-toggle="tooltip" data-placement="bottom" title="Laboratory Preparation">&nbsp${message(code: 'default.workflow.status.taskCompleted', default: 'Done')}</button>\n\
-->

    <!--
    <button class="btn-info btn-lg" onclick="updateStatusAndOpenReportToPrint('laboratoryPreparation', '${createLink(controller:'diagnosticOrderItem', action:'laboratoryPreparationPDF')}/${diagnosticOrderItemInstance.id}');">LaboratoryPreparationAndOpenReportToPrint</button>
    -->
"""

        } else if (session.user.role in [User.TECHNICAL_ASSISTANT]
    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.LABORATORY_PREPARATION]) {
sb << """
    <button type="button" class="btn btn-success glyphicon glyphicon-ok" onclick="updateStatus('reviewLaboratoryPreparation', '${createLink(controller:'diagnosticOrderItem', action:'laboratoryPreparationPDF')}/${diagnosticOrderItemInstance.id}');" data-toggle="tooltip" data-placement="bottom" title="Review Laboratory Preparation">&nbsp${message(code: 'default.workflow.status.taskCompleted', default: 'Done')}</button>
    <button type="button" class="btn btn-info glyphicon glyphicon-print" onclick="openReportToPrint('${createLink(controller:'diagnosticOrderItem', action:'laboratoryPreparationPDF')}/${diagnosticOrderItemInstance.id}');"> ${message(code: 'default.workflow.status.printReport', default: 'Done')}</button>
<!--
    <button class="btn-info btn-lg" onclick="window.open('${createLink(controller:'diagnosticOrderItem', action:'laboratoryPreparationPDF')}/${diagnosticOrderItemInstance.id}', '_blank');">OpenReportToPrint</button>
-->
"""
            
        } else if (session.user.role in [User.MEDICAL_DOCTOR]
    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION]) {

sb << """

    <button type="button" class="btn btn-danger glyphicon glyphicon-remove" onclick="backToPreviousStatus('${diagnosticOrderItemInstance.id}', '${createLink(controller:'diagnosticOrderItem', action:'previousStatus')}/${diagnosticOrderItemInstance.id}');" title="Non Conformity"> ${message(code: 'default.workflow.status.taskNonConformant', default: 'Non-conformant')}</button>
    <button type="button" class="btn btn-success glyphicon glyphicon-ok" onclick="updateStatus('completed');" data-toggle="tooltip" data-placement="bottom" title="Completed"> ${message(code: 'default.workflow.status.workflowCompleted', default: 'Completed')}</button>
<!--
    <button type="button" class="btn btn-info glyphicon glyphicon-arrow-right glyphicon-print" onclick="updateStatusAndOpenReportToPrint('completed', '${createLink(controller:'diagnosticOrderItem', action:'completedPDF')}/${diagnosticOrderItemInstance.id}');" title="Update Status and Print"> Print</button>

    <button type="button" class="btn btn-info glyphicon glyphicon-print" onclick="updateStatusAndOpenReportToPrint('completed', '${createLink(controller:'diagnosticOrderItem', action:'completedPDF')}/${diagnosticOrderItemInstance.id}');" title="Print"> Print</button>
-->

<!--
    <button class="btn-info btn-lg" onclick="updateStatusAndOpenReportToPrint('completed', '${createLink(controller:'diagnosticOrderItem', action:'completedPDF')}/${diagnosticOrderItemInstance.id}');">CompletedAndOpenReportToPrint</button>
    -->
"""
            
       } else if (session.user.role in [User.MEDICAL_DOCTOR]
    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.COMPLETED]) {

sb << """
<!--
    <button class="btn-primary btn-lg" onclick="window.open('${createLink(controller:'diagnosticOrderItem', action:'completedPDF')}/${diagnosticOrderItemInstance.id}', '_blank');">Print</button>
-->
    <button type="button" class="btn btn-info btn-lg btn-block btn-huge glyphicon glyphicon-print" onclick="openReportToPrint('${createLink(controller:'diagnosticOrderItem', action:'completedPDF')}/${diagnosticOrderItemInstance.id}');" title="Print"> ${message(code: 'default.workflow.status.printReport', default: 'Print')}</button>

"""
}

if (
//(session.user.role in [User.TECHNICAL_ASSISTANT] &
//((diagnosticOrderItemInstance?.status in [DiagnosticOrder.REQUESTED])))|
//(session.user.role in [User.ASSISTANT_MEDICAL_DOCTOR, User.MEDICAL_DOCTOR]) &
(session.user.role in [User.ASSISTANT_MEDICAL_DOCTOR]) &
diagnosticOrderItemInstance?.status in [DiagnosticOrder.REVIEW_LABORATORY_PREPARATION]) {

sb << """
    <button type="button" class="btn btn-danger glyphicon glyphicon-remove" onclick="backToPreviousStatus('${diagnosticOrderItemInstance.id}', '${createLink(controller:'diagnosticOrderItem', action:'previousStatus')}/${diagnosticOrderItemInstance.id}');" title="Non Conformity"> ${message(code: 'default.workflow.status.taskNonConformant', default: 'Non-conformant')}</button>
    <button type="button" class="btn btn-success glyphicon glyphicon-ok" onclick="enoughImagesUploaded() && updateStatus('reviewDiagnosticEvaluation');" data-toggle="tooltip" data-placement="bottom" title="Review Diagnostic Evaluation"> ${message(code: 'default.workflow.status.taskCompleted', default: 'Done')}</button>
    <!--
<button class="btn-danger btn-lg" onclick="backToPreviousStatus('${diagnosticOrderItemInstance.id}', '${createLink(controller:'diagnosticOrderItem', action:'previousStatus')}/${diagnosticOrderItemInstance.id}');">Non Conformity</button>
    -->

"""
}

sb << """
</div>
"""
        out << sb.toString()
    }

}






/*


        out << """
<div id="buttonsShortcuts" style="float: right;margin-right:50px;">
<!--
    <button type="button" class="btn btn-danger glyphicon glyphicon-arrow-left"> Back</button>
    <button type="button" class="btn btn-success glyphicon glyphicon-arrow-right"> Next</button>
    <button type="button" class="btn btn-info glyphicon glyphicon-print"> Print</button>
-->

<%
    if (session.user.role in [User.TECHNICAL_ASSISTANT]
    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.REQUESTED]) {
%>

    <button type="button" class="btn btn-success glyphicon glyphicon-arrow-right" onclick="updateStatusAndOpenReportToPrint('laboratoryPreparation', '${createLink(controller:'diagnosticOrderItem', action:'laboratoryPreparationPDF')}/${diagnosticOrderItemInstance.id}');" data-toggle="tooltip" data-placement="bottom" title="Laboratory Preparation"> Next</button>
    <!--
    <button class="btn-info btn-lg" onclick="updateStatusAndOpenReportToPrint('laboratoryPreparation', '${createLink(controller:'diagnosticOrderItem', action:'laboratoryPreparationPDF')}/${diagnosticOrderItemInstance.id}');">LaboratoryPreparationAndOpenReportToPrint</button>
    -->

<%
        } else if (session.user.role in [User.TECHNICAL_ASSISTANT]
    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.LABORATORY_PREPARATION]) {
%>
    <button type="button" class="btn btn-success glyphicon glyphicon-arrow-right" onclick="updateStatus('reviewLaboratoryPreparation', '${createLink(controller:'diagnosticOrderItem', action:'laboratoryPreparationPDF')}/${diagnosticOrderItemInstance.id}');" data-toggle="tooltip" data-placement="bottom" title="Review Laboratory Preparation"> Next</button>
    <button type="button" class="btn btn-info glyphicon glyphicon-print" onclick="openReportToPrint('${createLink(controller:'diagnosticOrderItem', action:'laboratoryPreparationPDF')}/${diagnosticOrderItemInstance.id}');"> Print</button>
<!--
    <button class="btn-info btn-lg" onclick="window.open('${createLink(controller:'diagnosticOrderItem', action:'laboratoryPreparationPDF')}/${diagnosticOrderItemInstance.id}', '_blank');">OpenReportToPrint</button>
-->
<%
        } else if (session.user.role in [User.MEDICAL_DOCTOR]
    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION]) {
%>

    <button type="button" class="btn btn-danger glyphicon glyphicon-arrow-left" onclick="backToPreviousStatus('${diagnosticOrderItemInstance.id}', '${createLink(controller:'diagnosticOrderItem', action:'previousStatus')}/${diagnosticOrderItemInstance.id}');" title="Non Conformity"> Back</button>
    <button type="button" class="btn btn-success glyphicon glyphicon-arrow-right" onclick="updateStatus('completed');" data-toggle="tooltip" data-placement="bottom" title="Completed"> Next</button>
<!--
    <button type="button" class="btn btn-info glyphicon glyphicon-arrow-right glyphicon-print" onclick="updateStatusAndOpenReportToPrint('completed', '${createLink(controller:'diagnosticOrderItem', action:'completedPDF')}/${diagnosticOrderItemInstance.id}');" title="Update Status and Print"> Print</button>
-->
    <button type="button" class="btn btn-info glyphicon glyphicon-print" onclick="updateStatusAndOpenReportToPrint('completed', '${createLink(controller:'diagnosticOrderItem', action:'completedPDF')}/${diagnosticOrderItemInstance.id}');" title="Print"> Print</button>
<!--
    <button class="btn-info btn-lg" onclick="updateStatusAndOpenReportToPrint('completed', '${createLink(controller:'diagnosticOrderItem', action:'completedPDF')}/${diagnosticOrderItemInstance.id}');">CompletedAndOpenReportToPrint</button>
    -->
<%
       } else if (session.user.role in [User.MEDICAL_DOCTOR]
    & diagnosticOrderItemInstance?.status in [DiagnosticOrder.COMPLETED]) {
%>
<!--
    <button class="btn-primary btn-lg" onclick="window.open('${createLink(controller:'diagnosticOrderItem', action:'completedPDF')}/${diagnosticOrderItemInstance.id}', '_blank');">Print</button>
-->
    <button type="button" class="btn btn-info btn-lg btn-block btn-huge glyphicon glyphicon-print" onclick="openReportToPrint('${createLink(controller:'diagnosticOrderItem', action:'completedPDF')}/${diagnosticOrderItemInstance.id}');" title="Print"> Print</button>
<%
}

if (
//(session.user.role in [User.TECHNICAL_ASSISTANT] &
//((diagnosticOrderItemInstance?.status in [DiagnosticOrder.REQUESTED])))|
//(session.user.role in [User.ASSISTANT_MEDICAL_DOCTOR, User.MEDICAL_DOCTOR]) &
(session.user.role in [User.ASSISTANT_MEDICAL_DOCTOR]) &
diagnosticOrderItemInstance?.status in [DiagnosticOrder.REVIEW_LABORATORY_PREPARATION]) {

%>

    <button type="button" class="btn btn-danger glyphicon glyphicon-arrow-left" onclick="backToPreviousStatus('${diagnosticOrderItemInstance.id}', '${createLink(controller:'diagnosticOrderItem', action:'previousStatus')}/${diagnosticOrderItemInstance.id}');" title="Non Conformity"> Back</button>
    <button type="button" class="btn btn-success glyphicon glyphicon-arrow-right" onclick="updateStatus('reviewDiagnosticEvaluation');" data-toggle="tooltip" data-placement="bottom" title="Review Diagnostic Evaluation"> Next</button>
    <!--
<button class="btn-danger btn-lg" onclick="backToPreviousStatus('${diagnosticOrderItemInstance.id}', '${createLink(controller:'diagnosticOrderItem', action:'previousStatus')}/${diagnosticOrderItemInstance.id}');">Non Conformity</button>
    -->

<%
}
%>

</div>
"""

*/