package de.uni_heidelberg.iph

class MailNotificationService {

    def sendMail (String msgDestinatary, String msgSubject, String msgBody) {
        sendMail {
            async true
            to msgDestinatary
            subject msgSubject
            body msgBody
        }
    }
}