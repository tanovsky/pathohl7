package de.uni_heidelberg.iph

import grails.transaction.Transactional

@Transactional
class WorkflowService {
//    public static final String REQUESTER_MEDICAL_DOCTOR = "RequesterMedicalDoctor"
//    public static final String TECHNICAL_ASSISTANT = "TechnicalAssistant"
//    public static final String MEDICAL_DOCTOR = "MedicalDoctor"
//    public static final String ASSISTANT_MEDICAL_DOCTOR = "AssistantMedicalDoctor"
//    public static final String MANAGER = "Manager"
//    public static final String ACCOUNTANT = "Accountant"
//    public static final String ADMINISTRATOR = "Administrator"

    def diagnosticOrderAvailableStatus(String status, String role) {

    }

    def allowedToPrint(String presentStatus, String allowedStatus) {
        def statusOrderedFlow = [
            DiagnosticOrder.REQUESTED,
            DiagnosticOrder.LABORATORY_PREPARATION,
            DiagnosticOrder.REVIEW_LABORATORY_PREPARATION,
            DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION,
            DiagnosticOrder.COMPLETED]
        return ((statusOrderedFlow.indexOf(presentStatus) - statusOrderedFlow.indexOf(allowedStatus))>=0)
    }

    def diagnosticOrderItemStatusHandledByRole(String role) {
        def statusHandledByRole = [
            (User.REQUESTER_MEDICAL_DOCTOR): [DiagnosticOrder.REQUESTED],
            (User.TECHNICAL_ASSISTANT) : [DiagnosticOrder.REQUESTED, DiagnosticOrder.LABORATORY_PREPARATION],
            (User.ASSISTANT_MEDICAL_DOCTOR) : [DiagnosticOrder.REVIEW_LABORATORY_PREPARATION],
            (User.MEDICAL_DOCTOR) : [DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION]
        ]
        return statusHandledByRole.get(role)
    }

    def diagnosticOrderItemStatusEditedByRole(String role) {
        return [
            (User.REQUESTER_MEDICAL_DOCTOR): [DiagnosticOrder.REQUESTED],
//            (User.TECHNICAL_ASSISTANT) : [DiagnosticOrder.REQUESTED, DiagnosticOrder.LABORATORY_PREPARATION],
            (User.TECHNICAL_ASSISTANT) : [DiagnosticOrder.REQUESTED],
            (User.ASSISTANT_MEDICAL_DOCTOR) : [DiagnosticOrder.REVIEW_LABORATORY_PREPARATION],
//            (User.MEDICAL_DOCTOR) : [DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION]
        ].get(role)
    }

    def diagnosticOrderItemPreviousStatus(String status) {
//        List workflow = [
//            DiagnosticOrder.REQUESTED,
//            DiagnosticOrder.LABORATORY_PREPARATION,
//            DiagnosticOrder.REVIEW_LABORATORY_PREPARATION,
//            DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION,
//            ]

        String previousStatus
        switch (status) {
            case DiagnosticOrder.REQUESTED:
                previousStatus = DiagnosticOrder.REQUESTED
            break
            case DiagnosticOrder.LABORATORY_PREPARATION:
                previousStatus = DiagnosticOrder.REQUESTED
            break
            case DiagnosticOrder.REVIEW_LABORATORY_PREPARATION:
//                previousStatus = DiagnosticOrder.LABORATORY_PREPARATION
                previousStatus = DiagnosticOrder.REQUESTED
            break
            case DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION:
                previousStatus = DiagnosticOrder.REVIEW_LABORATORY_PREPARATION
            break
        }
        return previousStatus
    }

    def diagnosticOrderItemsAvailableStatus(String status, String role) {
        List<String> availableStatus
        switch (role) {
                case User.REQUESTER_MEDICAL_DOCTOR:
                    switch (status) {
                        case DiagnosticOrder.REQUESTED:
                            availableStatus = [
                                DiagnosticOrder.REQUESTED,
                                DiagnosticOrder.CANCELLED]
                        break
                        case DiagnosticOrder.CANCELLED:
//                            availableStatus = [
//                                DiagnosticOrder.CANCELLED
//                            ]
                            availableStatus = [
//                                DiagnosticOrder.REQUESTED,
                                DiagnosticOrder.CANCELLED]
                        break
                        default:
                            availableStatus = [status]
                        break
                    }
                break
                case User.TECHNICAL_ASSISTANT:
                    availableStatus = [status]
                break
                case User.ASSISTANT_MEDICAL_DOCTOR:
                    switch (status) {
                        case DiagnosticOrder.REQUESTED:
                            availableStatus = [status]
//                            availableStatus = [
//                                DiagnosticOrder.REVIEW_LABORATORY_PREPARATION,
//                                DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION
//            //                                DiagnosticOrder.CANCELLED
//                            ]
                        break
                        default:
                            availableStatus = [status]
                        break
                    }
                break
                case User.MEDICAL_DOCTOR:
                    switch (status) {
                        case DiagnosticOrder.REQUESTED:
                            availableStatus = [
                                DiagnosticOrder.REQUESTED,
                                DiagnosticOrder.COMPLETED]
                        break
//                        case DiagnosticOrder.COMPLETED:
//                            availableStatus = [
//                                DiagnosticOrder.COMPLETED]
//                        break
                        default:
                            availableStatus = [status]
                        break
                    }
                    break
                case User.MANAGER:
                case User.ACCOUNTANT:
                case User.ADMINISTRATOR:
                    availableStatus = [status]
                break
                default:
                    availableStatus = ["draft"]
                break
        }

//        return ["requested", "canceled"]
        return availableStatus
    }

    def diagnosticOrderItemAvailableStatus(String status, String role) {
        println "status : " + status
        println "role : " + role
        List<String> availableStatus
        switch (role) {
            case User.REQUESTER_MEDICAL_DOCTOR:
                switch (status) {
                    case DiagnosticOrder.REQUESTED:
                        availableStatus = [
                            DiagnosticOrder.REQUESTED,
//                            DiagnosticOrder.CANCELLED
                        ]
                    break
                    case DiagnosticOrder.CANCELLED:
                        availableStatus = [
//                            DiagnosticOrder.REQUESTED,
                            DiagnosticOrder.CANCELLED
                        ]
                    break
                    default:
                        availableStatus = [status]
                    break
                }
            break
            case User.TECHNICAL_ASSISTANT:
                    availableStatus = [
                        DiagnosticOrder.REQUESTED,
                    ]
                    switch (status) {
                        case DiagnosticOrder.REQUESTED:
//                            println "dasss estou aki"
//availableStatus = ["laboratoryPreparation"]

                            availableStatus = [
                                DiagnosticOrder.REQUESTED,
                                DiagnosticOrder.LABORATORY_PREPARATION]
                        break
                        case DiagnosticOrder.LABORATORY_PREPARATION:
                            availableStatus = [
//                                DiagnosticOrder.REQUESTED,
                                DiagnosticOrder.LABORATORY_PREPARATION,
                                DiagnosticOrder.REVIEW_LABORATORY_PREPARATION]
                        break
                        case DiagnosticOrder.REVIEW_LABORATORY_PREPARATION:
                            availableStatus = [
//                                DiagnosticOrder.REQUESTED,
//                                DiagnosticOrder.LABORATORY_PREPARATION,
                                DiagnosticOrder.REVIEW_LABORATORY_PREPARATION]
                        break
//                        case DiagnosticOrder.CANCELLED:
//                            availableStatus = [
//                                DiagnosticOrder.CANCELLED]
//                        break
                        default:
                            availableStatus = [status]
                        break
                    }
            break
            case User.ASSISTANT_MEDICAL_DOCTOR:
                switch (status) {
//                    case DiagnosticOrder.REQUESTED:
//                        availableStatus = [
//                            DiagnosticOrder.REQUESTED
//                    ]
//                    break
                    case DiagnosticOrder.REVIEW_LABORATORY_PREPARATION:
                        availableStatus = [
                            DiagnosticOrder.REVIEW_LABORATORY_PREPARATION,
                            DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION,
//                                DiagnosticOrder.CANCELLED
                        ]
                    break
                    case DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION:
                        availableStatus = [
                            DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION,
//                                DiagnosticOrder.CANCELLED
                        ]
                    break
                    default:
                        availableStatus = [status]
                    break
                }
            break
            case User.MEDICAL_DOCTOR:
                switch (status) {
                    case DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION:
                        availableStatus = [
                            DiagnosticOrder.REVIEW_DIAGNOSTIC_EVALUATION,
                            DiagnosticOrder.COMPLETED
                        ]
                    break
//                    case DiagnosticOrder.COMPLETED:
//                        availableStatus = [
//                                    DiagnosticOrder.COMPLETED
//                        ]
//                    break
                    default:
                        availableStatus = [status]
                    break
                }
            break
            case User.ADMINISTRATOR:
            case User.MANAGER:
                availableStatus = [status]
            break
            default:
                availableStatus = ["draft"]
            break
        }
//            break
//                case User.MEDICAL_DOCTOR:
//                case User.ASSISTANT_MEDICAL_DOCTOR:
//            case User.MANAGER:
//            case User.ACCOUNTANT:
//            case User.ADMINISTRATOR:
//            break
//            default:
//                println "none"
//            break

//        return ["requested", "canceled"]
            println "-----------------------------------------------------  availableStatus " + availableStatus
            return availableStatus
        }
}
