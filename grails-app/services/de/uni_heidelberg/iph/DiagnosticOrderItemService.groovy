package de.uni_heidelberg.iph

import grails.transaction.Transactional

@Transactional
class DiagnosticOrderItemService {

    def lastUserWithRoleToPerformActionOnDiagnosticOrderItem(String role, DiagnosticOrderItem aDiagnosticOrderItem) {
        def a = DiagnosticOrderItemStatusEvent.where {
            user.role == role &&
            diagnosticOrderItem == aDiagnosticOrderItem
        }.last()
        return a.user
    }

    def lastStatusEvent(DiagnosticOrderItem theDiagnosticOrderItem) {
        def a = DiagnosticOrderItemStatusEvent.where {
            diagnosticOrderItem == theDiagnosticOrderItem
            order("dateCreated", "desc")
        }.first()
        return a
    }
}
