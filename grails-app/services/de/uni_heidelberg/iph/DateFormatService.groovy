package de.uni_heidelberg.iph

import org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware

class DateFormatService {
    public String longPattern() {
        return 'E, dd MMM yyyy, hh:mm aaa'
    }

    public String shortPattern() {
        return 'dd/MM/yyyy'
    }
}

