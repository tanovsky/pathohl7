package de.uni_heidelberg.iph

import grails.transaction.Transactional
import java.awt.image.BufferedImage
import javax.imageio.ImageIO


import com.google.zxing.MultiFormatWriter
import com.google.zxing.client.j2se.MatrixToImageWriter
import com.google.zxing.common.BitMatrix
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType

@Transactional
class BarcodeGeneratorService {

MultiFormatWriter barCodeWriter = new MultiFormatWriter()

def barcodeBytes(String code, int width, int height) {

//String formatIPH = "EAN_13"
String formatIPH = "CODE_128" //  p/20Ju8b9Ey/SRkUbxIxZA
//String data = "4006381333931"
//
//    int width = 200
//    int height = 50

        String fileSeparator = System.getProperty("file.separator")
//        String destinationDirectory = "/Users/tanovsky/wip/pathoHL7/images-repo/"
//        String destinationDirectory = "~/images-repo/"
//        String destinationDirectory = System.getProperty("user.home") + "/pathoHL7imagesRepository/fish-cish/"

        String destinationDirectory = System.getProperty("user.home") + fileSeparator +
                                      "tmp-barcodes" + fileSeparator

        File temporaryDestinationDirectory = new File(destinationDirectory)
        temporaryDestinationDirectory.mkdirs()

    BarcodeFormat format = BarcodeFormat.valueOf(formatIPH)
    File file = File.createTempFile("temp-barcode", ".png", temporaryDestinationDirectory)


    int paramsWidth = width
    int paramsHeight = height
    String paramsData = code


    String path = renderImageToFile(file, paramsData, paramsWidth as int, paramsHeight as int, format as BarcodeFormat)

    BufferedImage originalImage = ImageIO.read(new File(path));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    def fileext = path.substring(path.indexOf(".")+1, path.length())

    ImageIO.write( originalImage, fileext, baos );
    baos.flush();

    byte[] img = baos.toByteArray();
    return img
}


    void renderImage(response, String data, int width, int height, BarcodeFormat format = BarcodeFormat.QR_CODE) {
        Hashtable hints = [(EncodeHintType.CHARACTER_SET): 'UTF8']
        BitMatrix bitMatrix = barCodeWriter.encode(data, format, width, height, hints)
        MatrixToImageWriter.writeToStream(bitMatrix, "png", response.outputStream)
    }

    private File renderImageToFile(File file, String data, int width, int height, BarcodeFormat format = BarcodeFormat.QR_CODE) {
        Hashtable hints = [(EncodeHintType.CHARACTER_SET): 'UTF8']
        BitMatrix bitMatrix = barCodeWriter.encode(data, format, width, height, hints)
        MatrixToImageWriter.writeToFile(bitMatrix, "png", file)
        return file
    }

    def format(String data) {
        String id = data.substring(0,1)
        String year = data.substring(1,3)
        String number = data.substring(3,data.size())
        return "${id}/${year}/${number}"
//        return "${data.substring(0,1)}/${data.substring(1,3)}/${data.substring(3,data.size())}"​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
    }
}
