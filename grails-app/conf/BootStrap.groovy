
import de.uni_heidelberg.iph.*
import javax.servlet.ServletContext

class BootStrap {

List sample(int maxValue) {
/*    if (number > maxValue) {
        throw new Exception("wrong params :  number > maxValue")
    } else
*/

    def number = Math.abs((new Random()).nextInt() % maxValue) + 1
    println "my number " + number

    def myRange = 0..maxValue
    List sample = myRange.collect{it}
    Collections.shuffle(sample)
    return sample.take(number)
}

    def generator = { String alphabet, int n ->
        new Random().with {
            (1..n).collect { alphabet[ nextInt( alphabet.length() ) ] }.join()
        }
    }

    def combination = (('A'..'Z')+('0'..'9')).join()

    def init = { servletContext ->
        initialize(servletContext)
    }

    void initialize(ServletContext servletContext) {

        //                def userNumber = 10;

        int REQUESTER_MEDICAL_DOCTOR_COUNT = 2
        int TECHNICAL_ASSISTANT_COUNT = 3
        int MEDICAL_DOCTOR_COUNT = 3
        int ASSISTANT_MEDICAL_DOCTOR_COUNT = 1
        int MANAGER_COUNT = 1
        int ACCOUNTANT_COUNT = 1
        int ADMINISTRATOR_COUNT = 1

        //                def roles = ["Registered", "Administrator"];

        def admin = new User(
            login:"admin",
            password:"pass",
            role: User.ADMINISTRATOR,
            name:"Cris",
            mobile:"123123123",
            email:"cristiano_manuel@hotmail.com",
            tel:"123123123",
            address:"rua...")
        admin.save()

        def user;

        for (int i in 0..REQUESTER_MEDICAL_DOCTOR_COUNT - 1) {
            user = new User(
                login:"rmd"+i,
                password:"pass"+i,
                //				role:roles[random.nextInt(2)],
                role: User.REQUESTER_MEDICAL_DOCTOR,
                name:"REQUESTER_MEDICAL_DOCTOR "+i,
                mobile:"123123123",
                email:"tanovsky@gmail.com",
                tel:"123123123",
                address:"rua...")
            user.save()
        }

        for (int i in 0..TECHNICAL_ASSISTANT_COUNT-1) {
            user = new User(
                login:"ta"+i,
                password:"pass"+i,
                //				role:roles[random.nextInt(2)],
                role: User.TECHNICAL_ASSISTANT,
                name:"TechnicalAssistant "+i,
                mobile:"123123123",
                email:"tanovsky@gmail.com",
                tel:"123123123",
                address:"rua...")
            user.save()
        }

        for (int i in 0..MEDICAL_DOCTOR_COUNT-1) {
            user = new User(
                login:"md"+i,
                password:"pass"+i,
                //				role:roles[random.nextInt(2)],
                role: User.MEDICAL_DOCTOR,
                name:"MedicalDoctor "+i,
                mobile:"123123123",
                email:"tanovsky@gmail.com",
                tel:"123123123",
                address:"rua...")
            user.save()
        }

        for (int i in 0..ASSISTANT_MEDICAL_DOCTOR_COUNT-1) {
            user = new User(
                login:"amd"+i,
                password:"pass"+i,
                //				role:roles[random.nextInt(2)],
                role: User.ASSISTANT_MEDICAL_DOCTOR,
                name:"AssistantMedicalDoctor "+i,
                mobile:"123123123",
                email:"tanovsky@gmail.com",
                tel:"123123123",
                address:"rua...")
            user.save();
        }

        for (int i in 0..MANAGER_COUNT-1) {
            user = new User(
                login:"manager"+i,
                password:"pass"+i,
                //				role:roles[random.nextInt(2)],
                role: User.MANAGER,
                name:"Manager "+i,
                mobile:"123123123",
                email:"tanovsky@gmail.com",
                tel:"123123123",
                address:"rua...")
            user.save();
        }

        for (int i in 0..ACCOUNTANT_COUNT-1) {
            user = new User(
                login:"accountant"+i,
                password:"pass"+i,
                //				role:roles[random.nextInt(2)],
                role: User.ACCOUNTANT,
                name:"Accountant "+i,
                mobile:"123123123",
                email:"tanovsky@gmail.com",
                tel:"123123123",
                address:"rua...")
            user.save();
        }

//        Probe probe = new Probe(
//            name: "probe2",
//            code: "456",
//            description: "This is probe2...",
//            available: false,
//            enabled: true,
//            gene: gene1,
//            tissueType: tissueTypeHematologicTumor,
//            manufacturer: manufacturer2)

        if (!Gene.count() && !Probe.count()) {
            //        Gene gene1 = new Gene(
            //            name: "GENE1",
            //            locus: "17q21/cen17",
            //            alias: "gene1")
            //        gene1.save()
            //
            //        Gene gene2 = new Gene(
            //            name: "GENE2",
            //            locus: "8q24",
            //            alias: "gene2")
            //        gene2.save(failOnError: true)


        // Solide Tumoren
        new Gene(name: "HER2", locus: "17q21/cen17", alias: "ERBB2").save()
        new Gene(name: "c-myc", locus: "8q24", alias: "CEN8").save()
        new Gene(name: "EGFR", locus: "7p12/cen7", alias: "").save()
        new Gene(name: "MET", locus: "7q31/cen7", alias: "").save()
        new Gene(name: "ALK", locus: "2p23", alias: "").save()
        new Gene(name: "TFE3", locus: "Xp11", alias: "").save()
        new Gene(name: "FGFR1", locus: "8p12", alias: "").save()
        new Gene(name: "FGFR2", locus: "10q26", alias: "").save()
        new Gene(name: "ROS1", locus: "6q22", alias: "").save()
        new Gene(name: "MAML-2", locus: "11q21", alias: "").save()
        new Gene(name: "RET", locus: "10q11.21", alias: "").save()

        // Haematologie
        new Gene(name: "BRC-ABL", locus: "22q11/9q34", alias: "").save()
        new Gene(name: "IGH-MYC", locus: "14q32/8q24", alias: "").save()
        new Gene(name: "MYB", locus: "6q23.2-q23.3", alias: "").save()

        //Weichteiltumoren
        new Gene(name: "MDM2", locus: "12q14", alias: "").save()
        new Gene(name: "EWS", locus: "22q12", alias: "").save()
        new Gene(name: "CHOP", locus: "12q13", alias: "").save()
        new Gene(name: "SYT", locus: "18q11", alias: "").save()
        new Gene(name: "FOXO1", locus: "13q14", alias: "").save()
        new Gene(name: "NR4A3", locus: "9q22.23", alias: "").save()
        new Gene(name: "USP6", locus: "17q13.2", alias: "").save()
        new Gene(name: "FUS", locus: "16q11.2", alias: "").save()

        // not available at the form but existing at the xls sheet
//        new Gene(name: "PML/RARA", locus: "15q22", alias: "").save()





//            200.times { int it ->
//                Gene gene = new Gene(
//                    name: "GENE" + it,
//                    locus: ""+ generator( combination, 10 ) ,
//                    alias: "gene " + it)
//                gene.save(flush: true, failtOnError: true)
//            }

            TissueType tissueTypeSolidTumor = new TissueType(
                code: UUID.randomUUID().toString(),
                name: "SolideTumoren",
                description: "not soft.."
            )
            tissueTypeSolidTumor.save(failOnError: true)

            TissueType tissueTypeHematologicTumor = new TissueType(
                code: "456",
                name: "HematologicTumoren",
                description: "liquid.."
            )
            tissueTypeHematologicTumor.save(failOnError: true)

            TissueType tissueTypeSoftTumor = new TissueType(
                code: "789",
                name: "SoftTumoren",
                description: "not hard.."
            )
            tissueTypeSoftTumor.save(flush: true, failOnError: true)

//            5.times {
//                Manufacturer manufacturer = new Manufacturer(
//                    name: "Manufacturer of Probes " + generator( combination, 10 ))
//                manufacturer.save(flush: true, failOnError: true)
//            }

            // https://www.abbottmolecular.com
            Manufacturer manufacturer = new Manufacturer(
                name: "Abbot/Vysis")
            manufacturer.save()

            // http://www.zytomed-systems.com/
            manufacturer = new Manufacturer(
                name: "Zytomed/Zytovision")
            manufacturer.save()

            // http://www.leicabiosystems.com/ihc-ish-fish/kreatech-fish-probes/?no_cache=1
            manufacturer = new Manufacturer(
                name: "Kreatech")
            manufacturer.save()

            Probe probe = new Probe(
                name: "HER-2",
                code: "02J01",
                description: "DNA Probe Kit",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_FISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeSolidTumor,
                gene: Gene.findByName("HER2"),
                manufacturer: Manufacturer.findByName("Abbot/Vysis"))
            probe.save()

            probe = new Probe(
                name: "MDM2",
                code: "01N15",
                description: "DNA Probe Kit",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_FISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeSoftTumor,
                gene: Gene.findByName("MDM2"),
                manufacturer: Manufacturer.findByName("Abbot/Vysis"))
            probe.save()

            /* THE GENE NAME CANNOT CONTAIN "/"  */
//            probe = new Probe(
//                name: "PML/RARA",
//                code: "05N45",
//                description: "DNA Probe Kit",
//                procedure: Probe.HYBRIDIZATION_PROCEDURE_FISH,
//                available: true,
//                enabled: true,
//                tissueType: tissueTypeSolidTumor,
//                gene: Gene.findByName("PML/RARA"),
//                manufacturer: Manufacturer.findByName("Abbot/Vysis"))
//            probe.save()

            probe = new Probe(
                name: "EWSR1",
                code: "03N59",
                description: "DNA Probe Kit",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_SPLIT_FISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeSoftTumor,
                gene: Gene.findByName("EWS"),
                manufacturer: Manufacturer.findByName("Abbot/Vysis"))
            probe.save()

            probe = new Probe(
                name: "ALK",
                code: "06N38",
                description: "DNA Probe Kit",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_SPLIT_FISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeSolidTumor,
                gene: Gene.findByName("ALK"),
                manufacturer: Manufacturer.findByName("Abbot/Vysis"))
            probe.save()

            probe = new Probe(
                name: "EGFR",
                code: "01N35",
                description: "DNA Probe Kit",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_FISH,
                available: true,
                enabled: true,
                gene: Gene.findByName("EGFR"),
                tissueType: tissueTypeSolidTumor,
                manufacturer: Manufacturer.findByName("Abbot/Vysis"))
            probe.save()

            probe = new Probe(
                name: "CHOP",
                code: "03N57",
                description: "DNA Probe Kit",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_SPLIT_FISH,
                available: false,
                enabled: false,
                gene: Gene.findByName("CHOP"),
                tissueType: tissueTypeSoftTumor,
                manufacturer: Manufacturer.findByName(""))
            probe.save()

            probe = new Probe(
                name: "DDIT3",
                code: "03N57",
                description: "DNA Probe Kit",
                note: "Previously Vysis LSI CHOP (12q13) Dual Color, Break Apart Rearrangement Probe",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_SPLIT_FISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeSolidTumor,
                gene: Gene.findByName("DDIT3"),
                manufacturer: Manufacturer.findByName("Abbot/Vysis"))
            probe.save()

            probe = new Probe(
                name: "FOXO1",
                code: "03N60",
                description: "DNA Probe Kit",
                note: "Previously Vysis LSI FKHR (13q14) Dual Color, Break Apart Rearrangement Probe",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_SPLIT_FISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeSoftTumor,
                gene: Gene.findByName("FOXO1"),
                manufacturer: Manufacturer.findByName("Abbot/Vysis"))
            probe.save()

//            probe = new Probe(
//                name: "chiC",
//                code: "1N7920",
//                description: "DNA Probe Kit",
//                note: "",
//                procedure: Probe.HYBRIDIZATION_PROCEDURE_FISH,
//                available: true,
//                enabled: true,
//                gene: Gene.findByName("chiC"),
//                manufacturer: Manufacturer.findByName("Abbot/Vysis"))
//            probe.save()

            probe = new Probe(
                name: "HER2",
                code: "C-3022",
                description: "DNA Probe Kit",
                note: "ZytoDot 2C SPEC ERBB2/CEN 17 Probe Kit",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_CISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeSolidTumor,
                gene: Gene.findByName("HER2"),
                manufacturer: Manufacturer.findByName("Abbot/Vysis"))
            probe.save()

//            probe = new Probe(
//                name: "HER2",
//                code: "C-3022",
//                description: "DNA Probe Kit",
//                note: "ZytoDot 2C SPEC ERBB2/CEN 17 Probe Kit",
//                procedure: Probe.HYBRIDIZATION_PROCEDURE_CISH,
//                available: true,
//                enabled: true,
//                gene: Gene.findByName("HER2"),
//                manufacturer: Manufacturer.findByName("Abbot/Vysis"))
//            probe.save()

            probe = new Probe(
                name: "C-MET",
                code: "pKBI-10719",
                description: "DNA Probe Kit",
                note: "",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_CISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeSolidTumor,
                gene: Gene.findByName("MET"),
                manufacturer: Manufacturer.findByName("Kreatech"))
            probe.save()

//            probe = new Probe(
//                name: "C-MET",
//                code: "pKBI-10719",
//                description: "DNA Probe",
//                note: "",
//                procedure: Probe.HYBRIDIZATION_PROCEDURE_FISH,
//                available: true,
//                enabled: true,
//                gene: Gene.findByName("MET"),
//                manufacturer: Manufacturer.findByName("Kreatech"))
//            probe.save()

            probe = new Probe(
                name: "SYT",
                code: "pKBI-10713",
                description: "DNA Probe",
                note: "",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_FISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeSoftTumor,
                gene: Gene.findByName("SYT"),
                manufacturer: Manufacturer.findByName("Kreatech"))
            probe.save()

            probe = new Probe(
                name: "MYC/IGH",
                code: "pKBI-10603",
                description: "DNA Probe",
                note: "",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_FISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeHematologicTumor,
                gene: Gene.findByName("MYC"),
                manufacturer: Manufacturer.findByName("Kreatech"))
            probe.save()

            probe = new Probe(
                name: "EGFR",
                code: "pKBI-10702",
                description: "DNA Probe",
                note: "",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_FISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeSoftTumor,
                gene: Gene.findByName("EGFR"),
                manufacturer: Manufacturer.findByName("Kreatech"))
            probe.save()

            probe = new Probe(
                name: "BCR-ABL",
                code: "pKBI-10005",
                description: "DNA Probe",
                note: "",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_FISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeHematologicTumor,
                gene: Gene.findByName("BRC-ABL"),
                manufacturer: Manufacturer.findByName("Kreatech"))
            probe.save()

            probe = new Probe(
                name: "TFE",
                code: "pKBI-10741",
                description: "DNA Probe",
                note: "",
                procedure: Probe.HYBRIDIZATION_PROCEDURE_SPLIT_FISH,
                available: true,
                enabled: true,
                tissueType: tissueTypeSolidTumor,
                gene: Gene.findByName("TFE"),
                manufacturer: Manufacturer.findByName("Kreatech"))
            probe.save()


//            long seed = System.nanoTime()
//            def random = new Random()
//
//            List genes = Gene.list()
//            Collections.shuffle(genes, new Random(seed))
//            println "genes : " + genes.size()
//
//            List tissueTypes = TissueType.list()
//            Collections.shuffle(tissueTypes, new Random(seed))
//            println "tissueTypes : " + tissueTypes.size()
//
//            List manufacturers = Manufacturer.list()
//            Collections.shuffle(manufacturers, new Random(seed))
//            println "manufacturers : " + manufacturers.size()
//
//            int numberOfProbes = 10
//            int numberOfGenes = genes.size()-1
//            Math.min(numberOfGenes, numberOfProbes).times { int count ->
//                Gene geneTemp = genes.pop()
//                TissueType tissueTypeTemp = tissueTypes[(random.nextInt(tissueTypes.size()-1))]
//                Manufacturer manufacturerTemp = manufacturers[(random.nextInt(manufacturers.size()-1))]
//                Probe probe = new Probe(
//                    name: "probe" + count,
//                    code: UUID.randomUUID().toString(),
//                    description: "This is probe$count...",
//                    available: true,
//                    enabled: true,
//                    gene: geneTemp,
//                    tissueType: tissueTypeTemp,
//                    manufacturer: manufacturerTemp)
//                probe.save(flush: true, failOnError: true)
//            }
        }



/*
 * CREATION OF ORDERS
 */
//20.times { int diagOrder ->
//
//        DiagnosticOrder diagnosticOrder = new DiagnosticOrder()
////        diagnosticOrder.user = User.findByRole("AssistantMedicalDoctor")
//	    diagnosticOrder.eNumber = "R-${generator("0123456789", 5)}-15"
//        diagnosticOrder.block = generator("0123456789", 4)
//		diagnosticOrder.supportingInformation = generator("a Deucalione a contrario a bene placito a caelo usque ad centrum a capite ad calcem", 50)
//	    diagnosticOrder.barcode = generator("239w8q39hsdfkaj2039oai", 10)
////	    diagnosticOrder.barcode = "4006381333931"
//		diagnosticOrder.priority = DiagnosticOrder.PRIORITY_ROUTINE
//		diagnosticOrder.status = DiagnosticOrder.STATUS_REVIEW_LABORATORY_PREPARATION
//
//		diagnosticOrder.priority = "routine"
//		diagnosticOrder.status = "reviewLaboratoryPreparation"
//                diagnosticOrder.assistantMedicalDoctor = User.findByRole(User.ASSISTANT_MEDICAL_DOCTOR)
//		diagnosticOrder.save(flush: true)
//
//        int numberOfGenesAvailable = Probe.count()
//        DiagnosticOrderItem diagnosticOrderItem
//
//        def randomListOfProbes = sample(numberOfGenesAvailable)
//    	println "randomListOfProbes " + randomListOfProbes
//    	println "------------"
//
//        List probesIndex = Probe.list().collect{it.id}
//        Collections.shuffle(probesIndex)
//
//        def random = new Random()
//
//        Math.max(1, random.nextInt(3)).each { it ->
//            diagnosticOrderItem = new DiagnosticOrderItem()
////            Probe probe = Collections.shuffle(Probe.list())
////            println probe
////            println "vou pedir o " + it
////            diagnosticOrderItem.probe = Probe.get(it+1)
//
//            diagnosticOrderItem.probe = Probe.get(probesIndex[it+1])
//
//            diagnosticOrderItem.diagnosticOrder = diagnosticOrder
//            diagnosticOrderItem.save(flush: true)
//        }
//    	println "------------"
//}

    }
    def destroy = {
    }
}
