package pathohl7

class SecurityFilters {

//    def filters = {
//        all(controller:'*', action:'*') {
//            before = {
//
//            }
//            after = { Map model ->
//
//            }
//            afterView = { Exception e ->
//
//            }
//        }
//    }

    
	def filters = {
		doLogin(controller:'*' , action:'*' ){
			before = {
				if (!controllerName)
				return true
//				def allowedActions = ['show' , 'index' , 'list' , 'login' , 'validate' ]
//				def allowedActions = ['show' , 'index' , 'list' , 'login' , 'authenticate' ]
				def allowedActions = ['login' , 'authenticate' ]
				if (!session.user && !allowedActions.contains(actionName)){
					redirect(controller:'user' , action:'login' ,
						params:['cName' : controllerName, 'aName' :actionName])
					return false
				}
			}
		}
	}
    
}
