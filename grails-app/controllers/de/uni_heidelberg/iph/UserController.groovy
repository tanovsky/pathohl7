package de.uni_heidelberg.iph

import grails.converters.JSON
import grails.transaction.Transactional

class UserController {

    def workflowService

    def beforeInterceptor = [action: this.&auth, only: ["create", "list", "delete", "edit", "show"]]

    // defined with private scope, so it's not considered an action
    private auth() {
        if (![User.ADMINISTRATOR, User.MANAGER].contains(session?.user?.role)) {
            flash.message = "You must be an administrator to perform that task."
//            redirect(action:"entry", controller: "user")
            redirect(action:"entry", controller: "user")
//            redirect(action:"login", controller: "user")
            return false
        }
    }

    def index() {
        redirect(action:"list", params:params) }

        // the delete, save and update actions only accept POST requests
        static allowedMethods = [delete:'POST', save:'POST', update:'POST']



        def teste() {
            render(view:'/user/teste')
//            render(view:'/user/login');
        }

	def entry() {
            params.max = Math.min( params.max ? params.max.toInteger() : 0,  100)

            //        [ userInstanceList: User.list( params ), userInstanceTotal: User.count() ]
            //		render(view:'/user/login', model:[ userInstanceList: User.list( params ), userInstanceTotal: User.count() ]);
            //[ userInstanceList: User.list( params ), userInstanceTotal: User.count() ]

            Calendar cal=Calendar.getInstance();
            int month = cal.get(Calendar.MONTH);

            def now = new Date()
            def nextLimit = now + 31
            //		def reserv = Reservation.findAllByDateBetween( now, nextLimit )
            //		def reserv = Reservation.findAllByUser7hOrUser8hOrUser9hOrUser10hOrUser11hOrUser12hOrUser13hOrUser14hOrUser15hOrUser16hOrUser17hOrUser18hOrUser19h(session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user);
            //		def reserv = Reservation.findAllByUser7hLike(session?.user);  // funca
            //		def reserv = Reservation.findAllByUser7hOrUser8h(session?.user, session?.user);  // funca
//		def reserv = Reservation.findAllByUser7hOrUser8hOrUser9h(session?.user, session?.user, session?.user);
            //		def reserv = Reservation.findAllByUser7hOrUser8hOrUser9h(session?.user,session?.user,session?.user);

//		def criteria = Reservation.createCriteria()

//            def allDiagnosticOrders = DiagnosticOrder.list(params)
            def allDiagnosticOrders = DiagnosticOrderItem.list()
            List notCompletedOrders = []
            List notCompletedOrderItems = []
            int contador = 0
            allDiagnosticOrders.each {
                switch (session?.user.role) {
                    case User.REQUESTER_MEDICAL_DOCTOR:
                        if (((it.diagnosticOrder.requesterMedicalDoctor?.id == session.user.id)) &&
                            (!(it.status in [DiagnosticOrder.COMPLETED, DiagnosticOrder.CANCELLED]))) {
                                notCompletedOrders.add(it.diagnosticOrder)
                                notCompletedOrderItems.add(it)
                        }
                    break
                    case User.TECHNICAL_ASSISTANT:
                    case User.ASSISTANT_MEDICAL_DOCTOR:
                    case User.MEDICAL_DOCTOR:
                        if ((it.status != DiagnosticOrder.COMPLETED) &
                            (it.status in workflowService.diagnosticOrderItemStatusHandledByRole(session?.user.role))) {
                            notCompletedOrders.add(it.diagnosticOrder)
                            notCompletedOrderItems.add(it)
                        }
                    break
                    case User.ADMINISTRATOR:
                        if ((it.status != DiagnosticOrder.COMPLETED)) {
                            notCompletedOrders.add(it.diagnosticOrder)
                            notCompletedOrderItems.add(it)
                        }
                    break
                }
            }
            notCompletedOrders.unique()
            notCompletedOrderItems.unique()

            def allDiagnosticOrderItemStatusEvents = DiagnosticOrderItemStatusEvent.withCriteria {
                eq('user', session?.user)
                'between'('dateCreated', new Date() - grailsApplication.config.application.fish.history.daysBefore, new Date())
                order("dateCreated", "desc")
            }

            List latestPerformedDiagnosticOrder = []
            List latestPerformedDiagnosticOrderItems = []
            allDiagnosticOrderItemStatusEvents.each {
//                latestPerformedDiagnosticOrder.add(it.diagnosticOrderItem.diagnosticOrder)
                latestPerformedDiagnosticOrderItems.add(it.diagnosticOrderItem)
            }
//            latestPerformedDiagnosticOrder.unique()
            latestPerformedDiagnosticOrderItems.unique()

//            latestPerformedDiagnosticOrderItems.each {
//                it.status = DiagnosticOrderItemService.lastStatusEvent(it)
//            }

            render view:'/user/entry',
                model: [diagnosticOrderItemsInstanceList: notCompletedOrderItems, diagnosticOrderItemsInstanceTotal: notCompletedOrderItems.size(),
                            myLatestDiagnosticOrderItemsInstanceList: latestPerformedDiagnosticOrderItems, latestPerformedDiagnosticOrderItemsTotal: latestPerformedDiagnosticOrderItems.size()]

//            render ([diagnosticOrderInstanceList: notCompletedOrders, diagnosticOrderInstanceTotal: notCompletedOrders.size(),
//                    myLatestDiagnosticOrderInstanceList: latestPerformedDiagnosticOrder, latestPerformedDiagnosticOrderTotal: latestPerformedDiagnosticOrder.size()]) as JSON


//            render ([diagnosticOrderItemsInstanceList: notCompletedOrderItems, diagnosticOrderInstanceTotal: notCompletedOrderItems.size(),
//                            myLatestDiagnosticOrderItemsInstanceList: latestPerformedDiagnosticOrderItems, latestPerformedDiagnosticOrderItemsTotal: latestPerformedDiagnosticOrderItems.size()]) as JSON
	}

	def entryOriginal() {
            params.max = Math.min( params.max ? params.max.toInteger() : 0,  100)

            //        [ userInstanceList: User.list( params ), userInstanceTotal: User.count() ]
            //		render(view:'/user/login', model:[ userInstanceList: User.list( params ), userInstanceTotal: User.count() ]);
            //[ userInstanceList: User.list( params ), userInstanceTotal: User.count() ]

            Calendar cal=Calendar.getInstance();
            int month = cal.get(Calendar.MONTH);


            def now = new Date()
            def nextLimit = now + 31
            //		def reserv = Reservation.findAllByDateBetween( now, nextLimit )
            //		def reserv = Reservation.findAllByUser7hOrUser8hOrUser9hOrUser10hOrUser11hOrUser12hOrUser13hOrUser14hOrUser15hOrUser16hOrUser17hOrUser18hOrUser19h(session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user,session?.user);
            //		def reserv = Reservation.findAllByUser7hLike(session?.user);  // funca
            //		def reserv = Reservation.findAllByUser7hOrUser8h(session?.user, session?.user);  // funca
//		def reserv = Reservation.findAllByUser7hOrUser8hOrUser9h(session?.user, session?.user, session?.user);
            //		def reserv = Reservation.findAllByUser7hOrUser8hOrUser9h(session?.user,session?.user,session?.user);

//		def criteria = Reservation.createCriteria()

//            def allDiagnosticOrders = DiagnosticOrder.list(params)
            def allDiagnosticOrders = DiagnosticOrderItem.list()
            List notCompletedOrders = []
            int contador = 0
            allDiagnosticOrders.each {
                switch (session?.user.role) {
                    case User.REQUESTER_MEDICAL_DOCTOR:
                        if (((it.diagnosticOrder.requesterMedicalDoctor?.id == session.user.id)) &&
                            (!(it.status in [DiagnosticOrder.COMPLETED, DiagnosticOrder.CANCELLED]))) {
                                notCompletedOrders.add(it.diagnosticOrder)
                        }
                    break
                    case User.TECHNICAL_ASSISTANT:
                    case User.ASSISTANT_MEDICAL_DOCTOR:
                    case User.MEDICAL_DOCTOR:
                        if ((it.status != DiagnosticOrder.COMPLETED) &
                            (it.status in workflowService.diagnosticOrderItemStatusHandledByRole(session?.user.role))) {
                            notCompletedOrders.add(it.diagnosticOrder)
                        }
                    break
                    case User.ADMINISTRATOR:
                        if ((it.status != DiagnosticOrder.COMPLETED)) {
                            notCompletedOrders.add(it.diagnosticOrder)
                        }
                    break
                }
            }
            notCompletedOrders.unique()

            def allDiagnosticOrderItemStatusEvents = DiagnosticOrderItemStatusEvent.withCriteria {
                eq('user', session?.user)
                'between'('dateCreated', new Date() - grailsApplication.config.application.fish.history.daysBefore, new Date())
            }

            List latestPerformedDiagnosticOrder = []
            allDiagnosticOrderItemStatusEvents.each {
                latestPerformedDiagnosticOrder.add(it.diagnosticOrderItem.diagnosticOrder)
            }
            latestPerformedDiagnosticOrder.unique()

            return [diagnosticOrderInstanceList: notCompletedOrders, diagnosticOrderInstanceTotal: notCompletedOrders.size(),
                    myLatestDiagnosticOrderInstanceList: latestPerformedDiagnosticOrder, latestPerformedDiagnosticOrderTotal: latestPerformedDiagnosticOrder.size()
            ]
	}

	def authenticate() {
		def user = User.findByLoginAndPassword(params.login, params.password)
		if(user){
			session.user = user
			flash.message = "Hello ${user.name}!"

						if (params.cName) {
							redirect(controller:params.cName, action:params.aName)
						} else {
//			redirect(action:"entry")
                        redirect(action:"entry")
						}
		}else{
			flash.message = "Sorry, ${params.login}. Please try again."
			redirect(action:"login")
		}
	}


    def login() {
        log.debug("ENTREI!?!")

        if (session.user) redirect(action: "entry")
//		if (session.user) redirect(action: "entry")
		//			render(view:'/user/entry');

//		if (params.cName) return [cName:params.cName, aName:params.aName]
//		if (params.cName) redirect(controller: params.cName, action: params.aName)


		//		if (params.login == "admin" && params.password == "pass") {
		//			//			render "login succeed";
		//			flash.message = "login succeed";
		//			//			session.user = "admin";
		//			session.user = User.get(1);
		//		} else {
		//			flash.message = "login failed";
		//		}
		//		redirect(action: 'list');
    }

    def logout() {
		session.user = null;
                session._grails_console_last_code_= null;
		render(view:'/user/login');
    }


    def list() {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ userInstanceList: User.list( params ), userInstanceTotal: User.count() ]
    }

    def show() {
        def userInstance = User.get( params.id )

        if(!userInstance) {
            flash.message = "User not found with id ${params.id}"
            redirect(action: "list")
        }
        else { return [ userInstance : userInstance ] }
    }

    @Transactional
    def delete() {
        println "vou apagar!"
        def userInstance = User.get( params.id )
        if(userInstance) {
            try {
                userInstance.delete()
                flash.message = "User ${params.id} deleted"
                redirect(action:"list")
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "User ${params.id} could not be deleted"
                redirect(action:"show",id:params.id)
            }
        }
        else {
            flash.message = "User not found with id ${params.id}"
            redirect(action:"list")
        }
    }

    def edit() {
        def userInstance = User.get( params.id )

        if(!userInstance) {
            flash.message = "User not found with id ${params.id}"
            redirect(action:"list")
        }
        else {
            return [ userInstance : userInstance ]
        }
    }

    def update() {
        def userInstance = User.get( params.id )
        if(userInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(userInstance.version > version) {

                    userInstance.errors.rejectValue("version", "user.optimistic.locking.failure", "Another user has updated this User while you were editing.")
                    render(view:'edit',model:[userInstance:userInstance])
                    return
                }
            }
            userInstance.properties = params
            if(!userInstance.hasErrors() && userInstance.save()) {
                flash.message = "User ${params.id} updated"
                redirect(action: "show", id:userInstance.id)
            }
            else {
                render(view: 'edit', model:[userInstance:userInstance])
            }
        }
        else {
            flash.message = "User not found with id ${params.id}"
            redirect(action:"edit",id:params.id)
        }
    }

    def create() {
        def userInstance = new User()
        userInstance.properties = params
        return ['userInstance':userInstance]
    }

    def save() {
        def userInstance = new User(params)
        if(!userInstance.hasErrors() && userInstance.save()) {
            flash.message = "User ${userInstance.id} created"
            redirect(action:"show",id:userInstance.id)
        }
        else {
            render(view:'create',model:[userInstance:userInstance])
        }
    }
}
