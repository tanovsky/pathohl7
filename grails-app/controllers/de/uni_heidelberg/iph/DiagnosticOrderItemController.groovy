package de.uni_heidelberg.iph

import grails.converters.*

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import java.awt.image.BufferedImage
import javax.imageio.ImageIO

// TODO REMOVE when solve windows HACK
import java.util.regex.Matcher;

import java.text.SimpleDateFormat

import com.google.zxing.MultiFormatWriter
import com.google.zxing.client.j2se.MatrixToImageWriter
import com.google.zxing.common.BitMatrix
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType

import org.codehaus.groovy.grails.core.io.ResourceLocator
import org.springframework.core.io.Resource
import org.springframework.core.io.ResourceLoader

@Transactional(readOnly = true)
class DiagnosticOrderItemController {

//    def grailsApplication

    ResourceLocator grailsResourceLocator
    ResourceLoader resourceLoader

    def workflowService
    def dateFormatService
    def barcodeGeneratorService
    def diagnosticOrderItemService

    static scaffold = true

//MedicalDoctor
//	setStatusUpdate(Requested)
//	setStatusUpdate(Completed)
//TechnicalAssistant
//	setStatusUpdate(InProgress)
//	setStatusUpdate(ReviewLaboratoryPreparation)
//AssistantMedicalDoctor
//	setStatusUpdate(ReviewDiagnosticEvaluation)
//	setStatusUpdate(Failed)

//proposed | draft | planned | requested | received | accepted | in-progress | review | completed | cancelled | suspended | rejected | failed

    MultiFormatWriter barCodeWriter = new MultiFormatWriter()

    def timeline(DiagnosticOrderItem diagnosticOrderItem) {
        def diagnosticOrderItemStatusEvents = DiagnosticOrderItemStatusEvent.findAllByDiagnosticOrderItem(diagnosticOrderItem, [ sort:"dateCreated", order: 'desc'])
        println "quantos ? " + diagnosticOrderItemStatusEvents.size
        [events: diagnosticOrderItemStatusEvents, diagnosticOrderInstance: diagnosticOrderItem.diagnosticOrder, diagnosticOrderItemInstance: diagnosticOrderItem]
//        render workflowService.diagnosticOrderItemStatusHandledByRole(User.MEDICAL_DOCTOR)
    }

    def completedPDF() {

        DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(params.id)

//        String pathToLogo = g.resource(dir: 'images', file: 'custom/Logo.png'
////            , absolute: true
//        )
//
////        pathToLogo += webRequest.baseUrl
//
//        def filePath = "resources/images/grails_logo.png"
////        def text = ApplicationHolder.application.parentContext.getResource("classpath:$filePath")
//
//
//
//        def absolutePath = request.getSession().getServletContext().getRealPath("/images/grails_logo.png")
//
//        println "abolutePath " + absolutePath

//        println "my pathToLogo " + pathToLogo
//
//        def resource = grailsResourceLocator.findResourceForURI("images/custom/Logo.png")
//
////        final Resource image = grailsResourceLocator.findResourceForURI('/images/grails_logo.png')
////        render file: image.inputStream, contentType: 'image/png'


//        def path = resource.file.path // absolute file path

//        println "my path " + path


        def resourceMinorLogo = grailsResourceLocator.findResourceForURI('Logo.png')
        def resourceMajorLogo = grailsResourceLocator.findResourceForURI('LogoKlinikum.JPG')

//def formattedDate = g.formatDate(date:diagnosticOrderItem.dateCreated, format: 'E, dd MMM yyyy')

//        println "resource.file.path " + resource.file.path

//        println "resource : " + g.resource(dir: 'images', file: 'grails_logo.png', absolute: true)
//        Locale.setDefault(new Locale("de_DE"))

        def myModel = [
//            logoMajorBytes: grailsAttributes.getApplicationContext().getResource("/images/grails_logo.png").getFile().bytes,
            logoMajorBytes: new File(resourceMajorLogo.file.path).bytes,
            logoMinorBytes: new File(resourceMinorLogo.file.path).bytes,

//            eNumber: diagnosticOrderItem.diagnosticOrder.eNumber,
            eNumber: barcodeGeneratorService.format(diagnosticOrderItem.diagnosticOrder.eNumber),

            requester: diagnosticOrderItem.diagnosticOrder.requesterMedicalDoctor.name,
            blockNumber: diagnosticOrderItem.diagnosticOrder.block,
            barcodeBytes: barcodeGeneratorService.barcodeBytes(diagnosticOrderItem.diagnosticOrder.eNumber, 250, 30),
            dateOfReceivedRequest: g.formatDate(date: diagnosticOrderItem.dateCreated,
                format: dateFormatService.longPattern()),
            dateOfRequestCompleted: g.formatDate(date: new Date(),
                format: dateFormatService.shortPattern()),
            change: diagnosticOrderItem.change,
        ]

        switch (diagnosticOrderItem.change) {
            case DiagnosticOrderItem.SPLIT_CHANGE:
            case DiagnosticOrderItem.FUSION_CHANGE:
            case DiagnosticOrderItem.DELETION_CHANGE:
                myModel += [
                    nucleousCount: diagnosticOrderItem.nucleousCount,
                    positiveNucleousCount: diagnosticOrderItem.positiveNucleousCount,
                    percentageNucleousCountPositive: diagnosticOrderItem.percentageNucleousCountPositive,
                    percentageNotCountableNucleous: diagnosticOrderItem.percentageNotCountableNucleous
                ]
            break
            case DiagnosticOrderItem.AMPLIFICATION_CHANGE:
                myModel += [
                    centromerCount: diagnosticOrderItem.centromerCount,
                    locusSpecificProbeCount: diagnosticOrderItem.locusSpecificProbeCount,
                    ratio: diagnosticOrderItem.ratio
                ]
            break
        }

        myModel += [
            result: diagnosticOrderItem.result,
            comment: diagnosticOrderItem.comment,
            dateNow: g.formatDate(date: new Date(),
            format: dateFormatService.shortPattern()),
            medicalDoctor: diagnosticOrderItemService.lastUserWithRoleToPerformActionOnDiagnosticOrderItem(User.MEDICAL_DOCTOR, diagnosticOrderItem).name,
            assistantMedicalDoctor: diagnosticOrderItemService.lastUserWithRoleToPerformActionOnDiagnosticOrderItem(User.ASSISTANT_MEDICAL_DOCTOR, diagnosticOrderItem).name
        ]

        List images = new ArrayList()

        diagnosticOrderItem.uploadedFiles.sort { it.id }*.filepath.each { String filepath ->
            println "filepath : " + filepath
            images.add(new File(filepath).bytes)
        }

        myModel += [images: images]

        String myTemplate = "/diagnosticOrderItem/finalreport"

        renderPdf([template: myTemplate, model: myModel])
    }

    def reviewLaboratoryPreparationPDF() {
        redirect(action: "laboratoryPreparationPDF", params: params)
    }

    // http://www.4answered.com/questions/view/7d452b/Grails-rendering-plugin-does-not-render-images-when-deployed
//    def laboratoryPreparation(String fileName) {
    def laboratoryPreparationPDF() {
            // Data to pass from a Grails controller or service to view layer.
            def columns = [['string', 'Task'], ['number', 'Hours per Day']]
            def data = [['Work', 11], ['Eat', 2], ['Commute', 2], ['Watch TV', 2], ['Sleep', 7]]

            String imagePath = "/Users//tanovsky//pathoHL7imagesRepository//fish-cish////R10000015_HER2_56037030-24d7-43ae-8123-f1e255829c17.jpg"

    //                def paid = new File(ApplicationHolder.application.parentContext.servletContext.getRealPath(imagePath))
            def paid = new File(imagePath)

        DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(params.id)
        def resourceMinorLogo = grailsResourceLocator.findResourceForURI('Logo.png')
        def resourceMajorLogo = grailsResourceLocator.findResourceForURI('LogoKlinikum.JPG')

//        String eNumber = diagnosticOrderItem.diagnosticOrder.eNumber
//        String requester = diagnosticOrderItem.diagnosticOrder.requesterMedicalDoctor.name
//        String blockNumber = diagnosticOrderItem.diagnosticOrder.block
        String probe  = diagnosticOrderItem.probe.name
        String loteNumber = diagnosticOrderItem.probeLoteNumber
//        String loteExpireDate  = "loteExpireDate"
        String manufacturer = diagnosticOrderItem.probe.manufacturer.name
        String dateOfReceivedRequest = diagnosticOrderItem.dateCreated
        String dateOfRequestCompleted = diagnosticOrderItem.lastUpdated
//        byte[] barcodeBytes = barcodeGeneratorService.barcodeBytes(eNumber, 300, 75)

        def myModel = [
            logoMajorBytes: new File(resourceMajorLogo.file.path).bytes,
            logoMinorBytes: new File(resourceMinorLogo.file.path).bytes,
//            eNumber: eNumber,
            rawENumber: diagnosticOrderItem.diagnosticOrder.eNumber,
            eNumber: barcodeGeneratorService.format(diagnosticOrderItem.diagnosticOrder.eNumber),
            requester: diagnosticOrderItem.diagnosticOrder.requesterMedicalDoctor.name,
            blockNumber: diagnosticOrderItem.diagnosticOrder.block,
            barcodeBytes: barcodeGeneratorService.barcodeBytes(diagnosticOrderItem.diagnosticOrder.eNumber, 250, 30),
            dateOfReceivedRequest: g.formatDate(date: diagnosticOrderItem.dateCreated,
                                        format: dateFormatService.longPattern()),
//            dateOfRequestCompleted: dateFormatService.date(diagnosticOrderItem.lastUpdated),
//            blockNumber: blockNumber,
            probe: probe,
            loteNumber: loteNumber,
//            loteExpireDate: loteExpireDate,
            manufacturer: manufacturer,
            laboratoryPreparationComment: diagnosticOrderItem.laboratoryPreparationComment,
            probeLoteNumber: diagnosticOrderItem.probeLoteNumber,
//            probeExpirationDate: diagnosticOrderItem.probeExpirationDate,
            probeExpirationDate: g.formatDate(date: diagnosticOrderItem.probeExpirationDate, format: dateFormatService.shortPattern()),
//            labPreparationTime: diagnosticOrderItem.labPreparationTime,
//            dateOfReceivedRequest: dateOfReceivedRequest,
//            dateOfRequestCompleted: dateOfRequestCompleted,
            dateNow: g.formatDate(date: new Date(),
                format: dateFormatService.shortPattern()),
            repeated: diagnosticOrderItem.repeated,
            technicianName: session.user.name
        ]
        String myTemplate = "/diagnosticOrderItem/labpreparation"

        renderPdf([template: myTemplate, model: myModel])
    }


//def barcodeBytes() {
//
////String formatIPH = "EAN_13"
//String formatIPH = "CODE_128" //  p/20Ju8b9Ey/SRkUbxIxZA
////String data = "4006381333931"
////
////    int width = 200
////    int height = 50
//
//        String fileSeparator = System.getProperty("file.separator")
////        String destinationDirectory = "/Users/tanovsky/wip/pathoHL7/images-repo/"
////        String destinationDirectory = "~/images-repo/"
////        String destinationDirectory = System.getProperty("user.home") + "/pathoHL7imagesRepository/fish-cish/"
//
//        String destinationDirectory = System.getProperty("user.home") + fileSeparator +
//                                      "tmp-barcodes" + fileSeparator
//
//        File temporaryDestinationDirectory = new File(destinationDirectory)
//        temporaryDestinationDirectory.mkdirs()
//
//    BarcodeFormat format = BarcodeFormat.valueOf(formatIPH)
//    File file = File.createTempFile("temp-barcode", ".png", temporaryDestinationDirectory)
//
//
//    String paramsWidth = "234"
//    String paramsHeight = "345"
//    String paramsData = "R12123456"
//
//
//    String path = barcodeGeneratorService.renderImageToFile(file, paramsData, paramsWidth as int, paramsHeight as int, format as BarcodeFormat)
//
//    BufferedImage originalImage = ImageIO.read(new File(path));
//    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//
//    def fileext = path.substring(path.indexOf(".")+1, path.length())
//
//    ImageIO.write( originalImage, fileext, baos );
//    baos.flush();
//
//    byte[] img = baos.toByteArray();
//    return img
//}







//    http://generator.barcoding.com/
//    http://barcode.tec-it.com/en
//    https://www.dlsoft.com/barcode_types/identify_a_barcode.htm
//    http://www.scandit.com/products/barcode-scanner/symbologies/code-128/
//    http://www.barcodefaq.com/BarcodeMatch/
//def barcode(String data, String formate, int width, int height) {
def barcode() {

////String formatIPH = "EAN_13"
//String formatIPH = "CODE_128" //  p/20Ju8b9Ey/SRkUbxIxZA
////String data = "4006381333931"
////
////    int width = 200
////    int height = 50
//
//        String fileSeparator = System.getProperty("file.separator")
////        String destinationDirectory = "/Users/tanovsky/wip/pathoHL7/images-repo/"
////        String destinationDirectory = "~/images-repo/"
////        String destinationDirectory = System.getProperty("user.home") + "/pathoHL7imagesRepository/fish-cish/"
//
//    String destinationDirectory = System.getProperty("user.home") + fileSeparator +
//                                  "tmp-barcodes" + fileSeparator
//
//    File temporaryDestinationDirectory = new File(destinationDirectory)
//    temporaryDestinationDirectory.mkdirs()
//
//    BarcodeFormat format = BarcodeFormat.valueOf(formatIPH)
//    File file = File.createTempFile("temp-barcode", ".png", temporaryDestinationDirectory)
//
//    String path = barcodeGeneratorService.renderImageToFile(file, params.data, params.width as int, params.height as int, format as BarcodeFormat)
//
//    BufferedImage originalImage = ImageIO.read(new File(path));
//    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//
//    def fileext = path.substring(path.indexOf(".")+1, path.length())
//
//    ImageIO.write( originalImage, fileext, baos );
//    baos.flush();
//
//    byte[] img = baos.toByteArray();
//
//    String path = barcodeGeneratorService.renderImageToFile(file, params.data, params.width as int, params.height as int, format as BarcodeFormat)

    byte[] img = barcodeGeneratorService.barcodeBytes(params.data, params.width as int, params.height as int)

    baos.close();
    response.setHeader('Content-length', img.length.toString())
    response.contentType = "image/" + fileext // or the appropriate image content type
    response.outputStream << img
    response.outputStream.flush()
}

def search() {

        println "params " + params

        def taskList = DiagnosticOrder.createCriteria().list () {
            if ( params.query ) {
                println "estou aki?! __-----------------------------"
                String queryValue = params.query.trim()
                or {
                    ilike("eNumber", "%${queryValue}%")
                    ilike("block", "%${queryValue}%")
                    ilike("supportingInformation", "%${queryValue}%")
                    ilike("status", "%${queryValue}%")
                    ilike("priority", "%${queryValue}%")
                }
            }
        }

        println "taskList : " + taskList

//        def taskList = DiagnosticOrder.createCriteria().list (params) {
//            if ( params.query ) {
//                ilike("eNumber", "%${params.query}%")
//            }
//        }

//        def taskList = DiagnosticOrder.findAllByENumber("%${params.query}%")
//        model = [
//            diagnosticOrderInstanceList: taskList,
//            diagnosticOrderInstanceTotal: 10,
//            queryTerm: params.query
//        ]
//        model = [
//            diagnosticOrderInstanceList: DiagnosticOrder.list( params ),
//            diagnosticOrderInstanceTotal: DiagnosticOrder.count()
//        ]

        Map myModel = [
            diagnosticOrderInstanceList: taskList,
            diagnosticOrderInstanceTotal: 10,
            query: params.query
        ]

        return myModel
}

//    def list() {
//        params.max = Math.min(params.max ? params.int('max') : 5, 100)
//
//        def taskList = Task.createCriteria().list (params) {
//            if ( params.query ) {
//                ilike("description", "%${params.query}%")
//            }
//        }
//
//        [taskInstanceList: taskList, taskInstanceTotal: taskList.totalCount]
//    }

def getImage(){
    def path = params.filepath
    //returns an image to display
    BufferedImage originalImage = ImageIO.read(new File(path));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    def fileext = path.substring(path.indexOf(".")+1, path.length())

    ImageIO.write( originalImage, fileext, baos );
    baos.flush();

    byte[] img = baos.toByteArray();
    baos.close();
    response.setHeader('Content-length', img.length.toString())
    response.contentType = "image/"+fileext // or the appropriate image content type
    response.outputStream << img
    response.outputStream.flush()
}

    List<String> nextDiagnosticStatusUpdateAvailable2(String status, String role) {
        String nextStatus = "draft"

//        List<String> availableStatus = DiagnosticOrder.DRAFT
        List<String> availableStatus

//    public static final String REQUESTER_MEDICAL_DOCTOR = "RequesterMedicalDoctor"
//    public static final String TECHNICAL_ASSISTANT = "TechnicalAssistant"
//    public static final String MEDICAL_DOCTOR = "MedicalDoctor"
//    public static final String ASSISTANT_MEDICAL_DOCTOR = "AssistantMedicalDoctor"
//    public static final String MANAGER = "Manager"
//    public static final String ACCOUNTANT = "Accountant"
//    public static final String ADMINISTRATOR = "Administrator"

        switch (role) {
                case User.REQUESTER_MEDICAL_DOCTOR:
                    switch (status) {
                        case DiagnosticOrder.REQUESTED:
                            availableStatus = [
                                DiagnosticOrder.REQUESTED,
                                DiagnosticOrder.CANCELLED]
                        break
                        default:
                            availableStatus = ["draft"]
                    }
                break
                case User.TECHNICAL_ASSISTANT:
                case User.MEDICAL_DOCTOR:
                case User.ASSISTANT_MEDICAL_DOCTOR:
                case User.MANAGER:
                case User.ACCOUNTANT:
                case User.ADMINISTRATOR:
                default:
                    println "none"
                break
        }

//        return ["requested", "canceled"]
        return availableStatus
    }

//    String nextDiagnosticStatusUpdateAvailable(String status, String role) {
//
//        println "status : " + status
//
//        println "role : " + role
//
//        String nextStatus = "noStatusAvailable"
//
//        if (role == "TechnicalAssistant") {
//            if (status == "requested") {
//                nextStatus = "in-progress"
//            } else if (status == "in-progress") {
//                nextStatus = "reviewLaboratoryPreparation"
//            }
//        } else if (role == "AssistantMedicalDoctor") {
//            if (status == "reviewLaboratoryPreparation") {
//                nextStatus = "reviewDiagnosticEvaluation"
//            }
//        } else if (role == "MedicalDoctor") {
//            if (status == "reviewDiagnosticEvaluation") {
//                nextStatus = "completed"
//            }
//        }
//        return nextStatus
//    }

//    String renameFile(String filename) {
//        println "renaming file... ${filename}"
//        return "newFilename"
//    }

//    @Transactional
//    def cuga(DiagnosticOrder diagnosticOrderInstance) {
//        println diagnosticOrderInstance.status
//      def people = []
//      people << [firstName:'John', lastName:'Doe']
//      people << [firstName:'Jane', lastName:'Williams']
//      render people as grails.converters.JSON
//    }


    @Transactional( readOnly = false )
    def delimage() {
        String msg
        String idUpoadedFile = params.id
        def diagnosticOrderItemUploadedFile = DiagnosticOrderItemUploadedFile.get(idUpoadedFile)
        println "diagnosticOrderItemUploadedFile filepath : " + diagnosticOrderItemUploadedFile.filepath
        DiagnosticOrder diagnosticOrder = diagnosticOrderItemUploadedFile.diagnosticOrderItem.diagnosticOrder
        println " estou no delimage!!!!"
        // TODO NOT ALLOWED IF THE diagnosticOrder is completed...
        if (diagnosticOrder.status == DiagnosticOrder.COMPLETED) {
            msg =  "The workflow is completed."
            println msg
            flash.message = msg
            respond diagnosticOrder, view:"show", model: [diagnosticOrder: diagnosticOrder, nextStatus: DiagnosticOrder.COMPLETED]
        }

        boolean fileSuccessfullyDeleted =  new File(diagnosticOrderItemUploadedFile.filepath).delete()

        if (fileSuccessfullyDeleted) {
            diagnosticOrderItemUploadedFile.delete(flush: true)

//        String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrder.status, session.user.role)
//       println "nextStatus -> " + nextStatus


//         TODO WHAT IS THIS!?!?!
         def diagnosticOrder2 = DiagnosticOrder.get(diagnosticOrder.id)

//        render(view: "show", model: [diagnosticOrder: diagnosticOrder2, nextStatus: diagnosticOrder2.status])



//        DiagnosticOrder diagnosticOrder = diagnosticOrderItem.diagnosticOrder
//        List<String> availableStatus = workflowService.diagnosticOrderItemAvailableStatus(diagnosticOrderItem.status, session.user.role)
//        println " availableStatus " + availableStatus
//        respond diagnosticOrder, model: [diagnosticOrderInstance: diagnosticOrder, diagnosticOrderItemInstance: diagnosticOrderItem, availableStatus: availableStatus]

        respond diagnosticOrder, view:"item", model: [diagnosticOrder: diagnosticOrder2, diagnosticOrderItemInstance: diagnosticOrderItemUploadedFile.diagnosticOrderItem, availableStatus: diagnosticOrder2.status]
//        respond diagnosticOrder, view:"item", model: [diagnosticOrder: diagnosticOrder2, nextStatus: diagnosticOrder2.status]

        } else {
            render "ERROR deleting image ${diagnosticOrderItemUploadedFile.filepath}"
        }
//respond diagnosticOrder, view:"show", model: [probeList: selectedProbes, probeCount: selectedProbes.size(), nextStatus: nextStatus]

    }

    // TODO WHY WE NEED THIS READ ONLY FOR POSTGRES
    @Transactional( readOnly = false )
    def testando() {
        println "tou a executar...testando"
        println "______________________"
        println "Params: " + params
        println "______________________"

        def diagnosticOrderItemId = params.itemId.split("_").last()
        println "este é o item id : " + diagnosticOrderItemId
        DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(diagnosticOrderItemId)
        String eNumber = diagnosticOrderItem.diagnosticOrder.eNumber
        String block = diagnosticOrderItem.diagnosticOrder.block
        String geneName = diagnosticOrderItem.probe.gene.name



        println """
eNumber : ${eNumber}
block : ${block}
gene : ${geneName}
            """


        def downloadedFile = params.file

        if(!(downloadedFile instanceof org.springframework.web.multipart.commons.CommonsMultipartFile)){
            return false
        }

//        if ((downloadedFile.getWidth() != grailsApplication.config.application.fish.images.width) &&
//            (downloadedFile.getHeight() != grailsApplication.config.application.fish.images.height)) {
//            flash.message = "File size not correct.."
//            println "WHAT HELL!!!!!!"
////            return false
//        }

        // Check of images size

        println "downloaded file : " + downloadedFile

        String fileSeparator = System.getProperty("file.separator")

//        String destinationDirectory = "/Users/tanovsky/wip/pathoHL7/images-repo/"
//        String destinationDirectory = "~/images-repo/"
//        String destinationDirectory = System.getProperty("user.home") + "/pathoHL7imagesRepository/fish-cish/"

//        String destinationDirectory = System.getProperty("user.home") + fileSeparator +
//                                      "pathoHL7imagesRepository" + fileSeparator +
//                                      "fish-cish" + fileSeparator

        String destinationDirectory = grailsApplication.config.application.fish.images.directory

//        File myFile = new File("/Users/tanovsky/Downloads/apagarGrailsUploadTemp", downloadedFile.originalFilename)

        File outputDir = new File(destinationDirectory)
        outputDir.mkdirs()

        println "destination Directory : " + outputDir.absolutePath
//        String outputFilepath = destinationDirectory + "/" + downloadedFile.originalFilename

        println "passei aki.."
        String fileExtension = "jpg"

        println "e aki vou passar!?"
        String uniqueIdentifier = UUID.randomUUID().toString()
        println "uniqueIdentifier " + uniqueIdentifier
//        String newFilename = "${eNumber}_${block}_${geneName}_${uniqueIdentifier}.${fileExtension}"
        String newFilename = "${eNumber}_${geneName}_${uniqueIdentifier}.${fileExtension}"

        println "newFilename : " + newFilename
        String outputFilepath = destinationDirectory + fileSeparator + newFilename
        // TODO REMOVE THIS HORRIBLE HACK TO MAKE IT WORK ON WINDOWS MACHINE..
        // At the views the enconding of the filepath is not working with single "\" from windows..
        String twoFileSeparators = fileSeparator + fileSeparator
        outputFilepath = outputFilepath.replaceAll(Matcher.quoteReplacement(fileSeparator), Matcher.quoteReplacement(twoFileSeparators))
        println "e agora after hack ? " + outputFilepath

//        println "outputedFilepath : " + outputFilepath
//        File myFile = new File(outputDir, downloadedFile.originalFilename)

        File myFile = new File(destinationDirectory, newFilename)
        downloadedFile.transferTo(myFile)

//        new FileOutputStream(destinationDirectory+"/"+downloadedFile.originalFilename).leftShift(downloadedFile.getInputStream());

//        downloadedFile.transferTo(new File())

        println "gravei ficheiro ! "

//        println "Este é o paramsId do diagnostiOrderItem.." + params.id


        DiagnosticOrderItemUploadedFile diagnosticOrderItemUploadedFile = new DiagnosticOrderItemUploadedFile()
        diagnosticOrderItemUploadedFile.diagnosticOrderItem = diagnosticOrderItem
        diagnosticOrderItemUploadedFile.name = downloadedFile.originalFilename
        diagnosticOrderItemUploadedFile.filepath = outputFilepath   // TODO make this configurable for the base directory
        println "Final PATH : " + outputFilepath
        diagnosticOrderItemUploadedFile.fileType = fileExtension
        diagnosticOrderItemUploadedFile.save(flush:true)

        // TODO should be no action.. reload is done with javascript on the webpage..
//        redirect(action: "show", id:diagnosticOrderItemUploadedFile.diagnosticOrderItem.id)

//        render "batatinha!"
//        respond new DiagnosticOrder(params), model: [probeList: probes]

        render "render Fiz tudo!!!"
        println "fiz tudo!!!"
    }

    @Transactional( readOnly = false )
    def nonconformity(DiagnosticOrderItem diagnosticOrderItem) {

        // TODO remove this to service..
        DiagnosticOrderItemStatusEvent diagnosticOrderItemEvent = new DiagnosticOrderItemStatusEvent()

//        diagnosticOrderItemEvent.status = DiagnosticOrder.NON_CONFORMITY
//        diagnosticOrderItemEvent.description = params.description
////            diagnosticOrderItemEvent.dateTime = getNowUTC()
//        diagnosticOrderItemEvent.diagnosticOrderItem = diagnosticOrderItem
//        diagnosticOrderItemEvent.user = session.user
//        diagnosticOrderItemEvent.save()

        String previousStatus = workflowService.diagnosticOrderItemPreviousStatus(diagnosticOrderItem.status)
        diagnosticOrderItemEvent = new DiagnosticOrderItemStatusEvent()
        diagnosticOrderItemEvent.status = previousStatus
        diagnosticOrderItemEvent.isFromNonconformityOrigin = true
        diagnosticOrderItemEvent.description = params.description
//            diagnosticOrderItemEvent.dateTime = getNowUTC()
        diagnosticOrderItemEvent.diagnosticOrderItem = diagnosticOrderItem
        diagnosticOrderItemEvent.user = session.user
        diagnosticOrderItemEvent.save()

        diagnosticOrderItem.status = previousStatus
        if (diagnosticOrderItem.status == DiagnosticOrderItemStatusEvent.LABORATORY_PREPARATION) {
            diagnosticOrderItem.repeated = true
        }
        diagnosticOrderItem.save()
        flash.message = nonConformityStatusUpdatedMsg(previousStatus)
        redirect(action:"item", id:diagnosticOrderItem.id)
    }

    // TODO check if this method is used or even needed..
    def lastUpdatedStatus(DiagnosticOrderItem diagnosticOrderItem) {
        List<DiagnosticOrderItemStatusEvent> itemEvent = diagnosticOrderItem.itemEvent
    }

    def item(DiagnosticOrderItem diagnosticOrderItem) {
        println "diagnosticOrderItem " + diagnosticOrderItem
        DiagnosticOrder diagnosticOrder = diagnosticOrderItem.diagnosticOrder
        DiagnosticOrderItemStatusEvent diagnosticOrderItemStatusEvent = diagnosticOrderItemService.lastStatusEvent(diagnosticOrderItem)
        List<String> availableStatus = workflowService.diagnosticOrderItemAvailableStatus(diagnosticOrderItem.status, session.user.role)
        respond diagnosticOrder, model: [
            diagnosticOrderInstance: diagnosticOrder,
            diagnosticOrderItemInstance: diagnosticOrderItem,
            availableStatus: availableStatus,
            enoughImagesUploaded: enoughImagesUploaded(diagnosticOrderItem),
            isNonConformity: diagnosticOrderItemStatusEvent.isFromNonconformityOrigin,
            nonConformityMsg: diagnosticOrderItemStatusEvent.description
        ]
    }

//    @Transactional
//    def saveGeneListRequest() {
//
////        render params.probes as JSON
//
////        if (!params?.probes) {
////            redirect(action: "create")
////        }
//
//        println "________________________________"
//        println "saveGeneListRequest"
//        println "Params : " + params
//        println "________________________________"
//
//        DiagnosticOrder diagnosticOrder = new DiagnosticOrder(params)
////        diagnosticOrder.assistantMedicalDoctor = session.user
//        diagnosticOrder.requesterMedicalDoctor = session.user
//        String BARCODE_READER_STARTING_CHAR = "<"
//        println "*${diagnosticOrder.barcode}*"
////        def filteredBarcode = BARCODE_READER_STARTING_CHAR + "123412341234"
////        filteredBarcode = filteredBarcode.replaceFirst(/^${BARCODE_READER_STARTING_CHAR}/,"")​
////        filteredBarcode = filteredBarcode.replace("<", "")​
////        diagnosticOrder.barcode = "123412341234"
//        // TODO Why this does not work?! it works at grails console
////        diagnosticOrder.barcode = diagnosticOrder.barcode.replaceFirst(/^${BARCODE_READER_STARTING_CHAR}/,"")​
//        println ">>>>>>>>>>>>>>>>>>>> :" + diagnosticOrder?.barcode + ":"
//        if (diagnosticOrder?.barcode && diagnosticOrder?.barcode[0]?.equals(BARCODE_READER_STARTING_CHAR)) {
//                diagnosticOrder.barcode = diagnosticOrder.barcode.substring(1)
//        }
//        diagnosticOrder.save(flush:true)
//
//        println "params.probes " + params?.probes
//        println "class params?.probes : " + params?.probes?.class
////        if (params.probes)
//
//        List<Probe> selectedProbes = new ArrayList<Probe>()
//
//        println "params?.probes : " + params?.probes
//        String nextStatus
//        if (!(params?.probes == null)) {
//        def probeIds
//        if (params.probes.class != String) {
////        if (params.probes.indexOf("[") > 0) {
//            probeIds = params.probes as List<Integer>
//        } else {
//            probeIds = [params.probes]
//        }
//
//        println "probeIds : " + probeIds
//        println "probeIds class : " + probeIds.class
//
//        probeIds.each { String probe ->
//            selectedProbes << Probe.get(probe as int)
//        }
//
//        println "sp : " +  selectedProbes
//
////        render selectedProbes as JSON
////        render params.probes
//
////        def selectedProbes = Probe.list()
//
//        println "selected probles : " + selectedProbes
//
////        Request request = new Request()
//
////        diagnosticOrder.requestor = "some doctor"
////        diagnosticOrder.processed = false
//
//println "sucesso request?! " + diagnosticOrder.save(flush:true)
//
//        DiagnosticOrderItem diagnosticOrderItem
//        selectedProbes.each { Probe probe ->
//            println "adding "+ probe.name
//            diagnosticOrderItem = new DiagnosticOrderItem()
//            diagnosticOrderItem.probe = probe
//            diagnosticOrderItem.diagnosticOrder = diagnosticOrder
//            println "sucesso requestITem?! " + diagnosticOrderItem.save(flush:true)
//
//            DiagnosticOrderItemStatusEvent diagnosticOrderItemEvent = new DiagnosticOrderItemStatusEvent()
//            diagnosticOrderItemEvent.status = DiagnosticOrderItemStatusEvent.REQUESTED
//            diagnosticOrderItemEvent.description = "Initial event"
////            diagnosticOrderItemEvent.dateTime = getNowUTC()
//            diagnosticOrderItemEvent.diagnosticOrderItem = diagnosticOrderItem
//            diagnosticOrderItemEvent.save()
//        }
//
//        nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrder.status, session.user.role)
//
////        def res = diagnosticOrder as JSON
////        res.prettyPrint = true
////        render res
//        }
//
//        println "nextStatus -> " + nextStatus
//
//        DiagnosticOrderStatusEvent diagnosticOrderStatusEvent = new DiagnosticOrderStatusEvent()
//        diagnosticOrderStatusEvent.actor = session.user
//        diagnosticOrderStatusEvent.status = DiagnosticOrderStatusEvent.REQUESTED
//        diagnosticOrderStatusEvent.description = "Initial event"
//        diagnosticOrderStatusEvent.dateTime = getNowUTC()
//        diagnosticOrderStatusEvent.diagnosticOrder = diagnosticOrder
//        diagnosticOrderStatusEvent.save()
//
////        respond diagnosticOrder, model: [nextStatus: nextStatus]
//        diagnosticOrder.save(flush:true)
//
//        DiagnosticOrder diagnosticOrderInstance = DiagnosticOrder.get(diagnosticOrder.id)
//
//        println "ha items?! " + diagnosticOrderInstance.items
//
//        println "tem id ? " + diagnosticOrderInstance.id
//
////        respond diagnosticOrderInstance, view:"show", model: [diagnosticOrderItems: diagnosticOrderInstance, probeList: selectedProbes, probeCount: selectedProbes.size(), nextStatus: nextStatus]
//
////        redirect(action: "show", id:diagnosticOrderInstance.id)
//        redirect(action: "items", id:diagnosticOrderInstance.id)
//
//
////        def res = Probe.list(params.probes as List) as JSON
////        res.prettyPrint = true
//
////        render params
////        render params as JSON
////        println params
//    }

//    def show(DiagnosticOrder diagnosticOrder) {
//        if (params?.nextStatus) {
//            println "vou fazer set : " + params.nextStatus
//            diagnosticOrder.status = params.nextStatus
//            diagnosticOrder.save(flush:true)
//        }
//        String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrder.status, session.user.role)
//        println "nextStatus -> " + nextStatus
//
//    println "e assim ? "  + diagnosticOrder.item
//
//        respond diagnosticOrder, model: [nextStatus: nextStatus]
//    }
//
//    def show2(DiagnosticOrder diagnosticOrder) {
//        if (params?.nextStatus) {
//            println "vou fazer set : " + params.nextStatus
//            diagnosticOrder.status = params.nextStatus
//            diagnosticOrder.save(flush:true)
//        }
//        String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrder.status, session.user.role)
//        println "nextStatus -> " + nextStatus
//
//        println "e assim ? "  + diagnosticOrder.item
//
//        respond diagnosticOrder, model: [nextStatus: nextStatus]
//    }
//
//    @Transactional
//    def update(DiagnosticOrder diagnosticOrderInstance) {
//        String msg
//        // TODO NOT ALLOWED IF THE diagnosticOrder is completed...
//        if (diagnosticOrderInstance.status == DiagnosticOrder.COMPLETED) {
//            msg =  "The workflow is completed"
//            println msg
//            flash.message = msg
//            respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
//        }
//        switch (session.user.role)  {
//            case User.REQUESTER_MEDICAL_DOCTOR:
//                println "myParams ; " + params
//
//                String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrder.status, session.user.role)
//                println "nextStatus -> " + nextStatus
//
//                respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
//                return
//            break
//            case User.ASSISTANT_MEDICAL_DOCTOR:
//                diagnosticOrderInstance.assistantMedicalDoctor = session.user
//            break
//            case User.MEDICAL_DOCTOR:
//                diagnosticOrderInstance.medicalDoctor = session.user
//            break
//            default:
//                msg =  "User has to be correct role to perform any update"
//                println "TODO " + msg
//                flash.message = msg
//                respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
//                return
//            break;
//        }
//
////        println "myParams ; " + params
//
////        def diagnosticOrderItemId = params.itemId.split("_").last()
////        println "este é o item id : " + diagnosticOrderItemId
////        DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(diagnosticOrderItemId)
//
//    String separator = "_"
////    String prefix = "nucleousCount"
////params.entrySet().findAll {it.key.startsWith(prefix)}.each {
//println "----------------------------------------------------------------"
//    println params.entrySet()
//println "----------------------------------------------------------------"
//
//List itemsId = params.keySet().findAll {it =~ /^change_[0-9]/}.collect {it.split("_").last()}
//
//itemsId.each { String itemId ->
//    DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//    diagnosticOrderItem.change = params["change_"+itemId]
//
//    if (diagnosticOrderItem.change == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//        diagnosticOrderItem.nucleousCount = 0
//        diagnosticOrderItem.positiveNucleousCount = 0
//        diagnosticOrderItem.percentageNucleousCountPositive = 0
//        diagnosticOrderItem.centromerCount = params["centromerCount_"+itemId] as long
//        diagnosticOrderItem.locusSpecificProbeCount = params["locusSpecificProbeCount_"+itemId] as long
//        diagnosticOrderItem.ratio = params["ratio_"+itemId] as double
//    } else {
//        diagnosticOrderItem.centromerCount = 0
//        diagnosticOrderItem.locusSpecificProbeCount = 0
//        diagnosticOrderItem.ratio = 0
//        diagnosticOrderItem.nucleousCount = params["nucleousCount_"+itemId] as long
//        diagnosticOrderItem.positiveNucleousCount = params["positiveNucleousCount_"+itemId] as long
//        diagnosticOrderItem.percentageNucleousCountPositive = params["percentageNucleousCountPositive_"+itemId] as double
//    }
//    diagnosticOrderItem.percentageNotCountableNucleous = params["percentageNotCountableNucleous_"+itemId] as double
//    diagnosticOrderItem.result = params["result_"+itemId]
//    diagnosticOrderItem.comment = params["comment_"+itemId]
//    diagnosticOrderItem.save()
//}
//
//
////params.entrySet().each {
////        if (it.key.startsWith("nucleousCount")) {
////            int itemId = it.key.split(separator).last() as int
////            if (params."change_${itemId}" != DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
////            int value = it.value as int
////            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
////            diagnosticOrderItem.nucleousCount = value
////            diagnosticOrderItem.save()
////                        }
////        } else if (it.key.startsWith("positiveNucleousCount")) {
////            int itemId = it.key.split(separator).last() as int
////            if (params."change_${itemId}" != DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
////            int value = it.value as int
////            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
////            diagnosticOrderItem.positiveNucleousCount = value
////            diagnosticOrderItem.save()
////            }
////        } else if (it.key.startsWith("change")) {
////            int itemId = it.key.split(separator).last() as int
////            String value = it.value
////            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
////            diagnosticOrderItem.change = value
////            diagnosticOrderItem.save()
////        } else if (it.key.startsWith("percentageNotCountableNucleous")) {
////            int itemId = it.key.split(separator).last() as int
////            int value = it.value as int
////            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
////            diagnosticOrderItem.percentageNotCountableNucleous = value
////            diagnosticOrderItem.save()
////        } else if (it.key.startsWith("comment")) {
////            int itemId = it.key.split(separator).last() as int
////            String value = it.value
////            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
////            diagnosticOrderItem.comment = value
////            diagnosticOrderItem.save()
////        } else if (it.key.startsWith("percentageNucleousCountPositive")) {
////            int itemId = it.key.split(separator).last() as int
////            if (params."change_${itemId}" != DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
////            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
////            diagnosticOrderItem.percentageNucleousCountPositive = it.value as long
////            diagnosticOrderItem.save()
////            }
////        } else if (it.key.startsWith("centromerCount")) {
////            int itemId = it.key.split(separator).last() as int
////            if (params."change_${itemId}" == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
////            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
////            diagnosticOrderItem.centromerCount = it.value as long
////            diagnosticOrderItem.save()
////            }
////        } else if (it.key.startsWith("locusSpecificProbeCount")) {
////            int itemId = it.key.split(separator).last() as int
////            if (params."change_${itemId}" == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
////            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
////            diagnosticOrderItem.locusSpecificProbeCount = it.value as long
////            diagnosticOrderItem.save()
////            }
////        } else if (it.key.startsWith("ratio")) {
////            int itemId = it.key.split(separator).last() as int
////            if (params."change_${itemId}" == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
////            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
////            diagnosticOrderItem.ratio = it.value as long
////            diagnosticOrderItem.save()
////            }
////        } else if (it.key.startsWith("detected")) {
////            println "detected => '"   + it.value + "'"
////            int itemId = it.key.split(separator).last() as int
////            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
////            diagnosticOrderItem.detected = it.value.toBoolean()
////            println "it.value " + it.value
////            println "it.value as boolean " + (it.value.toBoolean())
////            println "diagnosticOrderItem.detected : " + diagnosticOrderItem.detected
////            diagnosticOrderItem.save()
////        }
////
////   println "param $it.key = $it.value"
////}
//
////        def res = params as JSON
////        res.prettyPrint = true
////        render res
//
////        if (diagnosticOrderInstance == null) {
////            notFound()
////            return
////        }
//
//        if (params?.nextStatus) {
//            println "vou fazer set : " + params.nextStatus
//            diagnosticOrderInstance.status = params.nextStatus
//            diagnosticOrderInstance.save(flush:true)
//        } else {
//            println "No change at status..."
//        }
//
//        String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrderInstance.status, session.user.role)
//        println "nextStatus -> " + nextStatus
//
//    respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: nextStatus]
//
////        if (diagnosticOrderInstance.hasErrors()) {
////            respond diagnosticOrderInstance.errors, view:'edit'
////            return
////        }
//
////        diagnosticOrderInstance.save flush:true
////
////        request.withFormat {
////            form multipartForm {
////                flash.message = message(code: 'default.updated.message', args: [message(code: 'DiagnosticOrder.label', default: 'DiagnosticOrder'), diagnosticOrderInstance.id])
////                redirect diagnosticOrderInstance
////            }
////            '*'{ respond diagnosticOrderInstance, [status: OK] }
////        }
//    }

    String statusUpdatedMsg(String status) {
//        return "Status updated to ${status}"
        return g.message(code : "default.workflow.status.updated.msg", args : [status])
    }

    String nonConformityStatusUpdatedMsg(String status) {
//        return "Non Conformity created and item status updated to '${status}'"
        return g.message(code : "default.workflow.status.nonConformity.updated.msg", args : [status])
    }

    boolean enoughImagesUploaded(DiagnosticOrderItem diagnosticOrderItem) {
//        def doi = DiagnosticOrderItem.get(153)

        boolean isEnough = false
        switch(diagnosticOrderItem.probe.procedure) {
            case { it.contains(Probe.HYBRIDIZATION_PROCEDURE_CISH) }:
                println "cish procedure"
                isEnough = true
            break
            default:
                println "non cish procedure"
                isEnough = (DiagnosticOrderItemUploadedFile.count()>0 && DiagnosticOrderItemUploadedFile.where {
                            diagnosticOrderItem == diagnosticOrderItem}?.list()?.size() >= 3)
            break
        }
        return isEnough

//        return (DiagnosticOrderItemUploadedFile.count()>0 && DiagnosticOrderItemUploadedFile.where {
//            diagnosticOrderItem == diagnosticOrderItem
//        }?.list()?.size() >= 3)
    }

    @Transactional
    def updateDOI(DiagnosticOrderItem diagnosticOrderItemInstance) {
        println "->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        println "updateDOI"
        println "params : " + params
        println "object Item status : " + diagnosticOrderItemInstance.status

//        diagnosticOrderItemInstance.probeExpirationDate = new Date().parse('dd/MM/yyyyy', params.probeExpirationDate)
//        diagnosticOrderItemInstance.probeExpirationDate = new Date(params.probeExpirationDate)

//        SimpleDateFormat sdf = new SimpleDateFormat('dd/mm/yyyy')
//        diagnosticOrderItemInstance.probeExpirationDate = sdf.parse(params.probeExpirationDate)

//        println sdf.parse("01/02/2003")​

        println "vou imprimir erros: "
        if (!diagnosticOrderItemInstance.save()) {
            diagnosticOrderItemInstance.errors.allErrors.each { println it }
        }

        diagnosticOrderItemInstance.save()

        DiagnosticOrder diagnosticOrderInstance = diagnosticOrderItemInstance.diagnosticOrder
        String msg
        // TODO NOT ALLOWED IF THE diagnosticOrder is completed...
        if (diagnosticOrderItemInstance.status == DiagnosticOrderItemStatusEvent.COMPLETED) {
            msg =  "The workflow is completed"
            println msg
            flash.message = msg
            respond diagnosticOrderItemInstance, view:"item", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
        }

        String lastStatusEvent = DiagnosticOrderItemStatusEvent.findAllByDiagnosticOrderItem(diagnosticOrderItemInstance).last().status

        // TODO remove this to service..
        if (diagnosticOrderItemInstance.status != lastStatusEvent) {
            DiagnosticOrderItemStatusEvent diagnosticOrderItemEvent = new DiagnosticOrderItemStatusEvent()
            diagnosticOrderItemEvent.status = diagnosticOrderItemInstance.status
    //        diagnosticOrderItemEvent.description = "Event occurred at : " + new Date()
    //            diagnosticOrderItemEvent.dateTime = getNowUTC()
            diagnosticOrderItemEvent.diagnosticOrderItem = diagnosticOrderItemInstance
            diagnosticOrderItemEvent.user = session.user
            diagnosticOrderItemEvent.save()
//        if ()
//        diagnosticOrderItemInstance.diagnosticOrder.status
        }
        println ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "
        println "diagnosticOrder before status : " + diagnosticOrderItemInstance.diagnosticOrder.status
        if (diagnosticOrderItemInstance.status == DiagnosticOrder.COMPLETED) {
            boolean allItemsAreCompleted = true
            diagnosticOrderItemInstance.diagnosticOrder.items.each {
                allItemsAreCompleted = allItemsAreCompleted & (it.status == DiagnosticOrder.COMPLETED)
            }
            println "allItemsAreCompleted " + allItemsAreCompleted
            if (allItemsAreCompleted) {
                println "All items are completed!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                diagnosticOrderItemInstance.diagnosticOrder.status = DiagnosticOrder.COMPLETED
                diagnosticOrderItemInstance.diagnosticOrder.save()
            }
        }
        println "diagnosticOrder after status : " + diagnosticOrderItemInstance.diagnosticOrder.status

        switch (session.user.role)  {
            case User.REQUESTER_MEDICAL_DOCTOR:
                println "myParams ; " + params
//                String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrder.status, session.user.role)
//                println "nextStatus -> " + nextStatus
                List<String> availableStatus = workflowService.diagnosticOrderItemAvailableStatus(diagnosticOrderItemInstance.status, session.user.role)
//                flash.message = statusUpdatedMsg(diagnosticOrderInstance.status)
                flash.message = statusUpdatedMsg(params.status)

                respond diagnosticOrderItemInstance, view:"item", model: [diagnosticOrder: diagnosticOrderInstance, availableStatus: availableStatus]
                return
            break
            case User.TECHNICAL_ASSISTANT:
                println "TECHNICAL_ASSISTANT ; "
                List<String> availableStatus = workflowService.diagnosticOrderItemAvailableStatus(diagnosticOrderItemInstance.status, session.user.role)
//                flash.message = statusUpdatedMsg(diagnosticOrderInstance.status)
                flash.message = statusUpdatedMsg(params.status)
                respond diagnosticOrderItemInstance, view:"item", model: [diagnosticOrderInstance: diagnosticOrderInstance, availableStatus: availableStatus]
                println "o gajo passa daki?!"
                return
            break
            case User.ASSISTANT_MEDICAL_DOCTOR:
//                if (enoughImagesUploaded(diagnosticOrderItemInstance)) {
                    List<String> availableStatus = workflowService.diagnosticOrderItemAvailableStatus(diagnosticOrderItemInstance.status, session.user.role)
                    flash.message = statusUpdatedMsg(params.status)
                    respond diagnosticOrderItemInstance, view:"item", model: [diagnosticOrderInstance: diagnosticOrderInstance, availableStatus: availableStatus, enoughImagesUploaded: enoughImagesUploaded(diagnosticOrderItemInstance)]
//                } else {
//                    flash.message = g.message(code : "default.not.enough.images.uploaded", args : ['3'])
//                    println " Qual é o status ?! : " + diagnosticOrderItemInstance.status
//                    diagnosticOrderItemInstance.status = "reviewLaboratoryPreparation"
//                    diagnosticOrderItemInstance.save(flush: true)
//                    redirect(action: "item", id: diagnosticOrderItemInstance.id)
//                    return
//                }

//                    List<String> availableStatus = workflowService.diagnosticOrderItemAvailableStatus(diagnosticOrderItemInstance.status, session.user.role)
//                    flash.message = statusUpdatedMsg(params.status)
//                    respond diagnosticOrderItemInstance, view:"item", model: [diagnosticOrderInstance: diagnosticOrderInstance, availableStatus: availableStatus]

            break
            case User.MEDICAL_DOCTOR:
                List<String> availableStatus = workflowService.diagnosticOrderItemAvailableStatus(diagnosticOrderItemInstance.status, session.user.role)
//                flash.message = statusUpdatedMsg(diagnosticOrderInstance.status)
                flash.message = statusUpdatedMsg(params.status)
                respond diagnosticOrderItemInstance, view:"item", model: [diagnosticOrderInstance: diagnosticOrderInstance, availableStatus: availableStatus]
                return
            break
            default:
                println "WTF!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                msg =  "User has to be correct role to perform any update"
                println "TODO " + msg
                flash.message = msg
                respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
                return
            break;
        }

//        println "myParams ; " + params

//        def diagnosticOrderItemId = params.itemId.split("_").last()
//        println "este é o item id : " + diagnosticOrderItemId
//        DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(diagnosticOrderItemId)

    String separator = "_"
//    String prefix = "nucleousCount"
//params.entrySet().findAll {it.key.startsWith(prefix)}.each {
println "----------------------------------------------------------------"
    println params.entrySet()
println "----------------------------------------------------------------"

List itemsId = params.keySet().findAll {it =~ /^change_[0-9]/}.collect {it.split("_").last()}

itemsId.each { String itemId ->
    DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
    diagnosticOrderItem.change = params["change_"+itemId]

    if (diagnosticOrderItem.change == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
        diagnosticOrderItem.nucleousCount = 0
        diagnosticOrderItem.positiveNucleousCount = 0
        diagnosticOrderItem.percentageNucleousCountPositive = 0
        diagnosticOrderItem.centromerCount = params["centromerCount_"+itemId] as long
        diagnosticOrderItem.locusSpecificProbeCount = params["locusSpecificProbeCount_"+itemId] as long
        diagnosticOrderItem.ratio = params["ratio_"+itemId] as double
    } else {
        diagnosticOrderItem.centromerCount = 0
        diagnosticOrderItem.locusSpecificProbeCount = 0
        diagnosticOrderItem.ratio = 0
        diagnosticOrderItem.nucleousCount = params["nucleousCount_"+itemId] as long
        diagnosticOrderItem.positiveNucleousCount = params["positiveNucleousCount_"+itemId] as long
        diagnosticOrderItem.percentageNucleousCountPositive = params["percentageNucleousCountPositive_"+itemId] as double
    }
    diagnosticOrderItem.percentageNotCountableNucleous = params["percentageNotCountableNucleous_"+itemId] as double
    diagnosticOrderItem.result = params["result_"+itemId]
    diagnosticOrderItem.comment = params["comment_"+itemId]
    diagnosticOrderItem.save()
}


//params.entrySet().each {
//        if (it.key.startsWith("nucleousCount")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" != DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            int value = it.value as int
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.nucleousCount = value
//            diagnosticOrderItem.save()
//                        }
//        } else if (it.key.startsWith("positiveNucleousCount")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" != DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            int value = it.value as int
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.positiveNucleousCount = value
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("change")) {
//            int itemId = it.key.split(separator).last() as int
//            String value = it.value
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.change = value
//            diagnosticOrderItem.save()
//        } else if (it.key.startsWith("percentageNotCountableNucleous")) {
//            int itemId = it.key.split(separator).last() as int
//            int value = it.value as int
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.percentageNotCountableNucleous = value
//            diagnosticOrderItem.save()
//        } else if (it.key.startsWith("comment")) {
//            int itemId = it.key.split(separator).last() as int
//            String value = it.value
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.comment = value
//            diagnosticOrderItem.save()
//        } else if (it.key.startsWith("percentageNucleousCountPositive")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" != DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.percentageNucleousCountPositive = it.value as long
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("centromerCount")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.centromerCount = it.value as long
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("locusSpecificProbeCount")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.locusSpecificProbeCount = it.value as long
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("ratio")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.ratio = it.value as long
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("detected")) {
//            println "detected => '"   + it.value + "'"
//            int itemId = it.key.split(separator).last() as int
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.detected = it.value.toBoolean()
//            println "it.value " + it.value
//            println "it.value as boolean " + (it.value.toBoolean())
//            println "diagnosticOrderItem.detected : " + diagnosticOrderItem.detected
//            diagnosticOrderItem.save()
//        }
//
//   println "param $it.key = $it.value"
//}

//        def res = params as JSON
//        res.prettyPrint = true
//        render res

//        if (diagnosticOrderInstance == null) {
//            notFound()
//            return
//        }

        if (params?.nextStatus) {
            println "vou fazer set : " + params.nextStatus
            diagnosticOrderItemInstance.status = params.nextStatus
            diagnosticOrderItemInstance.save(flush:true)
        } else {
            println "No change at status..."
        }
        List<String> availableStatus = workflowService.diagnosticOrderItemAvailableStatus(diagnosticOrderItemInstance.status, session.user.role)
//        String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrderInstance.status, session.user.role)
        println "availableStatus -> " + availableStatus

//    respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, availableStatus: availableStatus]
    respond diagnosticOrderInstance, view:"item", model: [diagnosticOrder: diagnosticOrderInstance, availableStatus: availableStatus]

//        if (diagnosticOrderInstance.hasErrors()) {
//            respond diagnosticOrderInstance.errors, view:'edit'
//            return
//        }

//        diagnosticOrderInstance.save flush:true
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.updated.message', args: [message(code: 'DiagnosticOrder.label', default: 'DiagnosticOrder'), diagnosticOrderInstance.id])
//                redirect diagnosticOrderInstance
//            }
//            '*'{ respond diagnosticOrderInstance, [status: OK] }
//        }
    }

    @Transactional
    def delete() {
        def diagnosticOrderItem = DiagnosticOrderItem.get( params.id )
//        int diagnosticOrderId = diagnosticOrderItem.diagnosticOrder.id as Integer
        // TODO with this it crashes?!?!
//        def diagnosticOrder = diagnosticOrderItem.diagnosticOrder
        println "diagnosticOrderItem : " + params.id
        if(diagnosticOrderItem) {
            try {
                boolean allFilesSuccessfullyDeleted = true
                diagnosticOrderItem.uploadedFiles.each { DiagnosticOrderItemUploadedFile uploadedFile ->
                    println "file: " + uploadedFile.filepath
                    File fileToDelete = new File(uploadedFile.filepath)
                    if (fileToDelete.exists()) {
                        allFilesSuccessfullyDeleted = allFilesSuccessfullyDeleted && (fileToDelete.delete())
                    }
                }
                if (allFilesSuccessfullyDeleted) {
                    diagnosticOrderItem.delete(flush: true)
                    println "funca o delte??!"
                    flash.message = "Diagnostic Order item ${params.id} deleted"
//                    println "diagnosticOrderId : " + diagnosticOrderId
//                    redirect(controller: "diagnosticOrder", action:"items", id: diagnosticOrderId
//                        redirect(controller: "diagnosticOrder", action:"items", id: diagnosticOrderItem.diagnosticOrder)
                        redirect(controller: "diagnosticOrder")
//render "ohoh"
                    return
                }
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Diagnostic Order item ${params.id} could not be deleted"
//                redirect(action:"item",id:params.id)
                    render "ahah"
                    return

            }
        }
        else {
            flash.message = "Diagnostic Order item not found with id ${params.id}"
            redirect(action:"items")
        }
    }










//    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
//
//    def index(Integer max) {
//        params.max = Math.min(max ?: 10, 100)
//        respond DiagnosticOrder.list(params), model:[diagnosticOrderInstanceCount: DiagnosticOrder.count()]
//    }
//
//    def show(DiagnosticOrder diagnosticOrderInstance) {
//        respond diagnosticOrderInstance
//    }
//
//    def create() {
//        respond new DiagnosticOrder(params)
//    }
//
//    @Transactional
//    def save(DiagnosticOrder diagnosticOrderInstance) {
//        if (diagnosticOrderInstance == null) {
//            notFound()
//            return
//        }
//
//        if (diagnosticOrderInstance.hasErrors()) {
//            respond diagnosticOrderInstance.errors, view:'create'
//            return
//        }
//
//        diagnosticOrderInstance.save flush:true
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.created.message', args: [message(code: 'diagnosticOrder.label', default: 'DiagnosticOrder'), diagnosticOrderInstance.id])
//                redirect diagnosticOrderInstance
//            }
//            '*' { respond diagnosticOrderInstance, [status: CREATED] }
//        }
//    }
//
//    def edit(DiagnosticOrder diagnosticOrderInstance) {
//        respond diagnosticOrderInstance
//    }
//
//    @Transactional
//    def update(DiagnosticOrder diagnosticOrderInstance) {
//        if (diagnosticOrderInstance == null) {
//            notFound()
//            return
//        }
//
//        if (diagnosticOrderInstance.hasErrors()) {
//            respond diagnosticOrderInstance.errors, view:'edit'
//            return
//        }
//
//        diagnosticOrderInstance.save flush:true
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.updated.message', args: [message(code: 'DiagnosticOrder.label', default: 'DiagnosticOrder'), diagnosticOrderInstance.id])
//                redirect diagnosticOrderInstance
//            }
//            '*'{ respond diagnosticOrderInstance, [status: OK] }
//        }
//    }
//
//    @Transactional
//    def delete(DiagnosticOrder diagnosticOrderInstance) {
//
//        if (diagnosticOrderInstance == null) {
//            notFound()
//            return
//        }
//
//        diagnosticOrderInstance.delete flush:true
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DiagnosticOrder.label', default: 'DiagnosticOrder'), diagnosticOrderInstance.id])
//                redirect action:"index", method:"GET"
//            }
//            '*'{ render status: NO_CONTENT }
//        }
//    }

//    @Transactional
//    def delete() {
//        def userInstance = DiagnosticOrder.get( params.id )
//        if(userInstance) {
//            try {
//                userInstance.delete()
//                flash.message = "User ${params.id} deleted"
//                redirect(action:"list")
//            }
//            catch(org.springframework.dao.DataIntegrityViolationException e) {
//                flash.message = "User ${params.id} could not be deleted"
//                redirect(action:"show",id:params.id)
//            }
//        }
//        else {
//            flash.message = "User not found with id ${params.id}"
//            redirect(action:"list")
//        }
//    }


//
//    protected void notFound() {
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.not.found.message', args: [message(code: 'diagnosticOrder.label', default: 'DiagnosticOrder'), params.id])
//                redirect action: "index", method: "GET"
//            }
//            '*'{ render status: NOT_FOUND }
//        }
//    }
}
