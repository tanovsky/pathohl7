package de.uni_heidelberg.iph

import grails.converters.*

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import java.awt.image.BufferedImage
import javax.imageio.ImageIO

// TODO REMOVE when solve windows HACK
import java.util.regex.Matcher;

import com.google.zxing.MultiFormatWriter
import com.google.zxing.client.j2se.MatrixToImageWriter
import com.google.zxing.common.BitMatrix
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType


@Transactional(readOnly = true)
class DiagnosticOrderController {

    def workflowService
    def mailNotificationService

    static scaffold = true

//MedicalDoctor
//	setStatusUpdate(Requested)
//	setStatusUpdate(Completed)
//TechnicalAssistant
//	setStatusUpdate(InProgress)
//	setStatusUpdate(ReviewLaboratoryPreparation)
//AssistantMedicalDoctor
//	setStatusUpdate(ReviewDiagnosticEvaluation)
//	setStatusUpdate(Failed)

//proposed | draft | planned | requested | received | accepted | in-progress | review | completed | cancelled | suspended | rejected | failed


    MultiFormatWriter barCodeWriter = new MultiFormatWriter()

    def test() {
        mailNotificationService.sendMail("cristiano_manuel@hotmail.com", "sera?!", "dsfsdfsdafsdafsdfsdf")
        render "yes"
    }

    void renderImage(response, String data, int width, int height, BarcodeFormat format = BarcodeFormat.QR_CODE) {
        Hashtable hints = [(EncodeHintType.CHARACTER_SET): 'UTF8']
        BitMatrix bitMatrix = barCodeWriter.encode(data, format, width, height, hints)
        MatrixToImageWriter.writeToStream(bitMatrix, "png", response.outputStream)
    }

    private File renderImageToFile(File file, String data, int width, int height, BarcodeFormat format = BarcodeFormat.QR_CODE) {
        Hashtable hints = [(EncodeHintType.CHARACTER_SET): 'UTF8']
        BitMatrix bitMatrix = barCodeWriter.encode(data, format, width, height, hints)
        MatrixToImageWriter.writeToFile(bitMatrix, "png", file)
        return file
    }

//    http://generator.barcoding.com/
//    http://barcode.tec-it.com/en
//    https://www.dlsoft.com/barcode_types/identify_a_barcode.htm
//    http://www.scandit.com/products/barcode-scanner/symbologies/code-128/
//    http://www.barcodefaq.com/BarcodeMatch/
//def barcode(String data, String formate, int width, int height) {
def barcode() {

//String formatIPH = "EAN_13"
String formatIPH = "CODE_128" //  p/20Ju8b9Ey/SRkUbxIxZA
//String data = "4006381333931"
//
//    int width = 200
//    int height = 50

        String fileSeparator = System.getProperty("file.separator")
//        String destinationDirectory = "/Users/tanovsky/wip/pathoHL7/images-repo/"
//        String destinationDirectory = "~/images-repo/"
//        String destinationDirectory = System.getProperty("user.home") + "/pathoHL7imagesRepository/fish-cish/"

        String destinationDirectory = System.getProperty("user.home") + fileSeparator +
                                      "tmp-barcodes" + fileSeparator

        File temporaryDestinationDirectory = new File(destinationDirectory)
        temporaryDestinationDirectory.mkdirs()

    BarcodeFormat format = BarcodeFormat.valueOf(formatIPH)
    File file = File.createTempFile("temp-barcode", ".png", temporaryDestinationDirectory)

    String path = renderImageToFile(file, params.data, params.width as int, params.height as int, format as BarcodeFormat)

    BufferedImage originalImage = ImageIO.read(new File(path));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    def fileext = path.substring(path.indexOf(".")+1, path.length())

    ImageIO.write( originalImage, fileext, baos );
    baos.flush();

    byte[] img = baos.toByteArray();
    baos.close();
    response.setHeader('Content-length', img.length.toString())
    response.contentType = "image/" + fileext // or the appropriate image content type
    response.outputStream << img
    response.outputStream.flush()
}

def search() {

        println "params " + params

        def taskList = DiagnosticOrder.createCriteria().list () {
            if ( params.query ) {
                println "estou aki?! __-----------------------------"
                String queryValue = params.query.trim()
                or {
                    ilike("eNumber", "%${queryValue}%")
                    ilike("block", "%${queryValue}%")
                    ilike("supportingInformation", "%${queryValue}%")
                    ilike("status", "%${queryValue}%")
                    ilike("priority", "%${queryValue}%")
                }
            }
        }

        println "taskList : " + taskList

//        def taskList = DiagnosticOrder.createCriteria().list (params) {
//            if ( params.query ) {
//                ilike("eNumber", "%${params.query}%")
//            }
//        }

//        def taskList = DiagnosticOrder.findAllByENumber("%${params.query}%")
//        model = [
//            diagnosticOrderInstanceList: taskList,
//            diagnosticOrderInstanceTotal: 10,
//            queryTerm: params.query
//        ]

//        model = [
//            diagnosticOrderInstanceList: DiagnosticOrder.list( params ),
//            diagnosticOrderInstanceTotal: DiagnosticOrder.count()
//        ]

        Map myModel = [
            diagnosticOrderInstanceList: taskList,
            diagnosticOrderInstanceTotal: 10,
            query: params.query
        ]

        return myModel
}

//    def list() {
//        params.max = Math.min(params.max ? params.int('max') : 5, 100)
//
//        def taskList = Task.createCriteria().list (params) {
//            if ( params.query ) {
//                ilike("description", "%${params.query}%")
//            }
//        }
//
//        [taskInstanceList: taskList, taskInstanceTotal: taskList.totalCount]
//    }
//

def getImage(){
    def path = params.filepath
    //returns an image to display
    BufferedImage originalImage = ImageIO.read(new File(path));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    def fileext = path.substring(path.indexOf(".")+1, path.length())

    ImageIO.write( originalImage, fileext, baos );
    baos.flush();

    byte[] img = baos.toByteArray();
    baos.close();
    response.setHeader('Content-length', img.length.toString())
    response.contentType = "image/"+fileext // or the appropriate image content type
    response.outputStream << img
    response.outputStream.flush()
}

    List<String> nextDiagnosticStatusUpdateAvailable2(String status, String role) {
        String nextStatus = "draft"

//        List<String> availableStatus = DiagnosticOrder.DRAFT
        List<String> availableStatus

//    public static final String REQUESTER_MEDICAL_DOCTOR = "RequesterMedicalDoctor"
//    public static final String TECHNICAL_ASSISTANT = "TechnicalAssistant"
//    public static final String MEDICAL_DOCTOR = "MedicalDoctor"
//    public static final String ASSISTANT_MEDICAL_DOCTOR = "AssistantMedicalDoctor"
//    public static final String MANAGER = "Manager"
//    public static final String ACCOUNTANT = "Accountant"
//    public static final String ADMINISTRATOR = "Administrator"

        switch (role) {
                case User.REQUESTER_MEDICAL_DOCTOR:
                    switch (status) {
                        case DiagnosticOrder.REQUESTED:
                            availableStatus = [
                                DiagnosticOrder.REQUESTED,
                                DiagnosticOrder.CANCELLED]
                        break
                        default:
                            availableStatus = ["draft"]
                    }
                break
                case User.TECHNICAL_ASSISTANT:
                case User.MEDICAL_DOCTOR:
                case User.ASSISTANT_MEDICAL_DOCTOR:
                case User.MANAGER:
                case User.ACCOUNTANT:
                case User.ADMINISTRATOR:
                default:
                    println "none"
                break
        }

//        return ["requested", "canceled"]
        return availableStatus
    }

    String nextDiagnosticStatusUpdateAvailable(String status, String role) {

        println "status : " + status

        println "role : " + role

        String nextStatus = "noStatusAvailable"

        if (role == "TechnicalAssistant") {
            if (status == "requested") {
                nextStatus = "in-progress"
            } else if (status == "in-progress") {
                nextStatus = "reviewLaboratoryPreparation"
            }
        } else if (role == "AssistantMedicalDoctor") {
            if (status == "reviewLaboratoryPreparation") {
                nextStatus = "reviewDiagnosticEvaluation"
            }
        } else if (role == "MedicalDoctor") {
            if (status == "reviewDiagnosticEvaluation") {
                nextStatus = "completed"
            }
        }
        return nextStatus
    }

    String renameFile(String filename) {
        println "renaming file... ${filename}"
        return "newFilename"
    }

    def Date getNowUTC() {
        Calendar cal = Calendar.getInstance()
        cal.setTimeZone(TimeZone.getTimeZone("UTC"))
        System.out.println(cal.toString())
        Date now = cal.getTime()
        return now
      }

        @Transactional
    def updateDOStatus(DiagnosticOrder diagnosticOrderInstance) {
        println diagnosticOrderInstance

//        def res = params as JSON
//        res.prettyPrint = true
//        render res
        diagnosticOrderInstance.eNumber = diagnosticOrderInstance.eNumber.replaceAll("/","")
        diagnosticOrderInstance.save()

        // TODO implement this at a service
//        DiagnosticOrderStatusEvent dose = new DiagnosticOrderStatusEvent()
//        dose.status = params.status
//        dose.actor = session.user
//        dose.description = "some descriptiosn.."
//        dose.dateTime = getNowUTC()
//        dose.diagnosticOrder = diagnosticOrderInstance
//
//        if (!dose.save()) {
//            dose.errors.allErrors.each {
//                println it
//            }
//        }

        List <String> availableStatus = workflowService.diagnosticOrderItemAvailableStatus(diagnosticOrderInstance.status, session.user.role)

        // TODO pass this to a service
        if (diagnosticOrderInstance.status == DiagnosticOrder.CANCELLED) {
            String msg =  "The Diagnostic Order is cancelled"

            diagnosticOrderInstance.items.each {

                DiagnosticOrderItemStatusEvent diagnosticOrderItemEvent = new DiagnosticOrderItemStatusEvent()
                diagnosticOrderItemEvent.status = DiagnosticOrder.CANCELLED
        //        diagnosticOrderItemEvent.description = "Event occurred at : " + new Date()
        //            diagnosticOrderItemEvent.dateTime = getNowUTC()
                diagnosticOrderItemEvent.diagnosticOrderItem = it
                diagnosticOrderItemEvent.user = session.user
                diagnosticOrderItemEvent.save()

                it.status = DiagnosticOrder.CANCELLED
                it.save()
            }

            println msg
            flash.message = msg
            respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
        }

        respond diagnosticOrderInstance, view:"items", model: [diagnosticOrder: diagnosticOrderInstance, availableStatus: availableStatus]



//        String msg
//        // TODO NOT ALLOWED IF THE diagnosticOrder is completed...
//        if (diagnosticOrderInstance.status == DiagnosticOrder.COMPLETED) {
//            msg =  "The workflow is completed"
//            println msg
//            flash.message = msg
//            respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
//        }
//        switch (session.user.role) {
//            case User.REQUEST_MEDICAR_DOCTOR:
//            break;
//            case User.ASSISTANT_MEDICAL_DOCTOR:
//                diagnosticOrderInstance.assistantMedicalDoctor = session.user
//            break
//            case User.MEDICAL_DOCTOR:
//                diagnosticOrderInstance.medicalDoctor = session.user
//            break
//            default:
//                msg =  "User has to be correct role to perform any update"
//                println "TODO " + msg
//                flash.message = msg
//                respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
//                return
//            break;
//        }
//
//
//    respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: nextStatus]
    }

    @Transactional( readOnly = false )
    def delimage() {
        String msg
        String idUpoadedFile = params.id
        def diagnosticOrderItemUploadedFile = DiagnosticOrderItemUploadedFile.get(idUpoadedFile)
        DiagnosticOrder diagnosticOrder = diagnosticOrderItemUploadedFile.diagnosticOrderItem.diagnosticOrder
        println " estou no delimage!!!!"
        // TODO NOT ALLOWED IF THE diagnosticOrder is completed...
        if (diagnosticOrder.status == DiagnosticOrder.COMPLETED) {
            msg =  "The workflow is completed."
            println msg
            flash.message = msg
            respond diagnosticOrder, view:"show", model: [diagnosticOrder: diagnosticOrder, nextStatus: DiagnosticOrder.COMPLETED]
        }

        boolean fileSuccessfullyDeleted =  new File(diagnosticOrderItemUploadedFile.filepath).delete()

        if (fileSuccessfullyDeleted) {
            diagnosticOrderItemUploadedFile.delete(flush: true)

//        String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrder.status, session.user.role)
//       println "nextStatus -> " + nextStatus


//         TODO WHAT IS THIS!?!?!
         def diagnosticOrder2 = DiagnosticOrder.get(diagnosticOrder.id)

//        render(view: "show", model: [diagnosticOrder: diagnosticOrder2, nextStatus: diagnosticOrder2.status])





        respond diagnosticOrder, view:"show", model: [diagnosticOrder: diagnosticOrder2, nextStatus: diagnosticOrder2.status]

        } else {
            render "ERROR deleting image ${diagnosticOrderItemUploadedFile.filepath}"
        }
//respond diagnosticOrder, view:"show", model: [probeList: selectedProbes, probeCount: selectedProbes.size(), nextStatus: nextStatus]

    }

    // TODO WHY WE NEED THIS READ ONLY FOR POSTGRES
    @Transactional( readOnly = false )
    def testando() {
        println "tou a executar...testando"
        println "______________________"
        println "Params: " + params
        println "______________________"

        def diagnosticOrderItemId = params.itemId.split("_").last()
        println "este é o item id : " + diagnosticOrderItemId
        DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(diagnosticOrderItemId)
        String eNumber = diagnosticOrderItem.diagnosticOrder.eNumber
        String block = diagnosticOrderItem.diagnosticOrder.block
        String geneName = diagnosticOrderItem.probe.gene.name



        println """
eNumber : ${eNumber}
block : ${block}
gene : ${geneName}
            """


        def downloadedFile = params.file

        if(!(downloadedFile instanceof org.springframework.web.multipart.commons.CommonsMultipartFile)){
            return false
        }

        println "downloaded file : " + downloadedFile

        String fileSeparator = System.getProperty("file.separator")

//        String destinationDirectory = "/Users/tanovsky/wip/pathoHL7/images-repo/"
//        String destinationDirectory = "~/images-repo/"
//        String destinationDirectory = System.getProperty("user.home") + "/pathoHL7imagesRepository/fish-cish/"

        String destinationDirectory = System.getProperty("user.home") + fileSeparator +
                                      "pathoHL7imagesRepository" + fileSeparator +
                                      "fish-cish" + fileSeparator

//        File myFile = new File("/Users/tanovsky/Downloads/apagarGrailsUploadTemp", downloadedFile.originalFilename)

        File outputDir = new File(destinationDirectory)
        outputDir.mkdirs()

        println "destination Directory : " + outputDir.absolutePath
//        String outputFilepath = destinationDirectory + "/" + downloadedFile.originalFilename

        println "passei aki.."
        String fileExtension = "jpg"

        println "e aki vou passar!?"
        String uniqueIdentifier = UUID.randomUUID().toString()
        println "uniqueIdentifier " + uniqueIdentifier
//        String newFilename = "${eNumber}_${block}_${geneName}_${uniqueIdentifier}.${fileExtension}"
        String newFilename = "${eNumber}_${geneName}_${uniqueIdentifier}.${fileExtension}"

        println "newFilename : " + newFilename
        String outputFilepath = destinationDirectory + fileSeparator + newFilename
        // TODO REMOVE THIS HORRIBLE HACK TO MAKE IT WORK ON WINDOWS MACHINE..
        // At the views the enconding of the filepath is not working with single "\" from windows..
        String twoFileSeparators = fileSeparator + fileSeparator
        outputFilepath = outputFilepath.replaceAll(Matcher.quoteReplacement(fileSeparator), Matcher.quoteReplacement(twoFileSeparators))
        println "e agora after hack ? " + outputFilepath

//        println "outputedFilepath : " + outputFilepath
//        File myFile = new File(outputDir, downloadedFile.originalFilename)

        File myFile = new File(destinationDirectory, newFilename)
        downloadedFile.transferTo(myFile)

//        new FileOutputStream(destinationDirectory+"/"+downloadedFile.originalFilename).leftShift(downloadedFile.getInputStream());

//        downloadedFile.transferTo(new File())

        println "gravei ficheiro ! "

//        println "Este é o paramsId do diagnostiOrderItem.." + params.id


        DiagnosticOrderItemUploadedFile diagnosticOrderItemUploadedFile = new DiagnosticOrderItemUploadedFile()
        diagnosticOrderItemUploadedFile.diagnosticOrderItem = diagnosticOrderItem
        diagnosticOrderItemUploadedFile.name = downloadedFile.originalFilename
        diagnosticOrderItemUploadedFile.filepath = outputFilepath   // TODO make this configurable for the base directory
        println "Final PATH : " + outputFilepath
        diagnosticOrderItemUploadedFile.fileType = fileExtension
        diagnosticOrderItemUploadedFile.save(flush:true)

        // TODO should be no action.. reload is done with javascript on the webpage..
//        redirect(action: "show", id:diagnosticOrderItemUploadedFile.diagnosticOrderItem.id)

//        render "batatinha!"
//        respond new DiagnosticOrder(params), model: [probeList: probes]

        render "render Fiz tudo!!!"
        println "fiz tudo!!!"
    }

    def create() {
//        def probes = Probe.list()

        def query = Probe.where {
            enabled == true
        }

        def probes = query.findAll()

        println "probes : " + probes

        respond new DiagnosticOrder(params), model: [probeList: probes]
    }

//{
//  "class": "de.uni_heidelberg.iph.DiagnosticOrder",
//  "id": 52,
//  "barcode": "b5sFCggFvE2KtMQShzJpTw",
//  "block": "asdfasdf",
//  "eNumber": "R-12345-16",
//  "item":
//  [
//    {
//      "class": "de.uni_heidelberg.iph.DiagnosticOrderItem",
//      "id": 54
//    },
//    {
//      "class": "de.uni_heidelberg.iph.DiagnosticOrderItem",
//      "id": 53
//    }
//  ],
//  "note": " ",
//  "priority": "routine",
//  "reason": "",
//  "requesterMedicalDoctor": {
//    "class": "de.uni_heidelberg.iph.User",
//    "id": 2
//  },
//  "status": "SOME",
//  "supportingInformation": "sdafsdfsdf"
//}

    def items(DiagnosticOrder diagnosticOrder) {

        if (!diagnosticOrder) {
            flash.message = "Diagnostic Order with id ${params.id} not found."
            redirect(action:"index")
            return
        } else {
            println "sfdfsdafsd"
        }

        println ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> diagnosticOrder " + diagnosticOrder
        List <String> availableStatus = workflowService.diagnosticOrderItemsAvailableStatus(diagnosticOrder.status, session.user.role)
//        List <String> availableStatus = nextDiagnosticStatusUpdateAvailable2(diagnosticOrder.status, session.user.role)
//        respond diagnosticOrder, model: [diagnosticOrderInstance: diagnosticOrder, nextStatus: "REQUESTED"]
        respond diagnosticOrder, model: [diagnosticOrderInstance: diagnosticOrder, availableStatus: availableStatus]

        println "events : " + diagnosticOrder.events.status

//        def res = diagnosticOrder as JSON
//        res.prettyPrint = true
//        render res

        /*
        def query = Probe.where {
            enabled == true
        }

        def probes = query.findAll()

        println "probes : " + probes

        respond new DiagnosticOrder(params), model: [probeList: probes]
        */
    }

    // TODO check if this method is used or even needed..
    def lastUpdatedStatus(DiagnosticOrderItem diagnosticOrderItem) {
        List<DiagnosticOrderItemStatusEvent> itemEvent = diagnosticOrderItem.itemEvent
    }

    def item(DiagnosticOrderItem diagnosticOrderItem) {
        DiagnosticOrder diagnosticOrder = diagnosticOrderItem.diagnosticOrder

        // TODO order by DATE (this should also be done at some service class.. )
        // may be a more smart to do this would be to write the last updated status to the DiagnosticOrderItem object..
        // so no need to query the list of events every time..
        List<DiagnosticOrderItemStatusEvent> diagnosticOrderItemStatusEventList = DiagnosticOrderItemStatusEvent.findAllWhere(diagnosticOrderItem: diagnosticOrderItem)
        String diagnosticOrderItemLastStatus = diagnosticOrderItemStatusEventList.last().status

//        println ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> diagnosticOrderItem.status : " + diagnosticOrderItem.status
        List <String> availableStatus = workflowService.diagnosticOrderItemAvailableStatus(diagnosticOrderItemLastStatus, session.user.role)

        respond diagnosticOrder, model: [diagnosticOrderInstance: diagnosticOrder, diagnosticOrderItemInstance: diagnosticOrderItem, availableStatus: availableStatus]
    }

    @Transactional
    def saveGeneListRequest() {

//        render params.probes as JSON

//        if (!params?.probes) {
//            redirect(action: "create")
//        }

        println "________________________________"
        println "Those are my paramssera!!!"
        println "Params : " + params
        println "________________________________"

        params.eNumber = params.eNumber.replaceAll("/","")
        DiagnosticOrder diagnosticOrder = new DiagnosticOrder(params)
//        diagnosticOrder.assistantMedicalDoctor = session.user
        diagnosticOrder.requesterMedicalDoctor = session.user
        String BARCODE_READER_STARTING_CHAR = "<"
        println "*${diagnosticOrder.barcode}*"
//        def filteredBarcode = BARCODE_READER_STARTING_CHAR + "123412341234"
//        filteredBarcode = filteredBarcode.replaceFirst(/^${BARCODE_READER_STARTING_CHAR}/,"")​
//        filteredBarcode = filteredBarcode.replace("<", "")​
//        diagnosticOrder.barcode = "123412341234"
        // TODO Why this does not work?! it works at grails console
//        diagnosticOrder.barcode = diagnosticOrder.barcode.replaceFirst(/^${BARCODE_READER_STARTING_CHAR}/,"")​
        println ">>>>>>>>>>>>>>>>>>>> :" + diagnosticOrder?.barcode + ":"
        if (diagnosticOrder?.barcode && diagnosticOrder?.barcode[0]?.equals(BARCODE_READER_STARTING_CHAR)) {
                diagnosticOrder.barcode = diagnosticOrder.barcode.substring(1)
        }
        diagnosticOrder.save(flush:true)

        println "params.probes " + params?.probes
        println "class params?.probes : " + params?.probes?.class
//        if (params.probes)

        List<Probe> selectedProbes = new ArrayList<Probe>()

        println "params?.probes : " + params?.probes
        String nextStatus
        if (!(params?.probes == null)) {
        def probeIds
        if (params.probes.class != String) {
//        if (params.probes.indexOf("[") > 0) {
            probeIds = params.probes as List<Integer>
        } else {
            probeIds = [params.probes]
        }

        println "probeIds : " + probeIds
        println "probeIds class : " + probeIds.class

        probeIds.each { String probe ->
            selectedProbes << Probe.get(probe as int)
        }

        println "sp : " +  selectedProbes

//        render selectedProbes as JSON
//        render params.probes

//        def selectedProbes = Probe.list()

        println "selected probles : " + selectedProbes

//        Request request = new Request()

//        diagnosticOrder.requestor = "some doctor"
//        diagnosticOrder.processed = false

println "sucesso request?! " + diagnosticOrder.save(flush:true)

        DiagnosticOrderItem diagnosticOrderItem
        selectedProbes.each { Probe probe ->
            println "adding "+ probe.name
            diagnosticOrderItem = new DiagnosticOrderItem()
            diagnosticOrderItem.probe = probe
            diagnosticOrderItem.status = DiagnosticOrderItemStatusEvent.REQUESTED
            diagnosticOrderItem.diagnosticOrder = diagnosticOrder
            println "sucesso requestITem?! " + diagnosticOrderItem.save(flush:true)

            DiagnosticOrderItemStatusEvent diagnosticOrderItemEvent = new DiagnosticOrderItemStatusEvent()
            diagnosticOrderItemEvent.status = DiagnosticOrderItemStatusEvent.REQUESTED
            diagnosticOrderItemEvent.description = "Initial event"
//            diagnosticOrderItemEvent.dateTime = getNowUTC()
            diagnosticOrderItemEvent.diagnosticOrderItem = diagnosticOrderItem
            diagnosticOrderItemEvent.user = session.user
            diagnosticOrderItemEvent.save()
        }

//        nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrder.status, session.user.role)

//        def res = diagnosticOrder as JSON
//        res.prettyPrint = true
//        render res
        }

        println "nextStatus -> " + nextStatus
        // TODO pass this to a service
//        DiagnosticOrderStatusEvent diagnosticOrderStatusEvent = new DiagnosticOrderStatusEvent()
//        diagnosticOrderStatusEvent.actor = session.user
//        diagnosticOrderStatusEvent.status = DiagnosticOrderStatusEvent.REQUESTED
//        diagnosticOrderStatusEvent.description = "Initial event"
//        diagnosticOrderStatusEvent.dateTime = getNowUTC()
//        diagnosticOrderStatusEvent.diagnosticOrder = diagnosticOrder
//        diagnosticOrderStatusEvent.save()

//        respond diagnosticOrder, model: [nextStatus: nextStatus]
        diagnosticOrder.save(flush:true)

        DiagnosticOrder diagnosticOrderInstance = DiagnosticOrder.get(diagnosticOrder.id)

        println "ha items?! " + diagnosticOrderInstance.items

        println "tem id ? " + diagnosticOrderInstance.id

//        respond diagnosticOrderInstance, view:"show", model: [diagnosticOrderItems: diagnosticOrderInstance, probeList: selectedProbes, probeCount: selectedProbes.size(), nextStatus: nextStatus]

//        redirect(action: "show", id:diagnosticOrderInstance.id)
        redirect(action: "items", id:diagnosticOrderInstance.id)

    }

//    def show(DiagnosticOrder diagnosticOrder) {
//        if (params?.nextStatus) {
//            println "vou fazer set : " + params.nextStatus
//            diagnosticOrder.status = params.nextStatus
//            diagnosticOrder.save(flush:true)
//        }
//        String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrder.status, session.user.role)
//        println "nextStatus -> " + nextStatus
//
//    println "e assim ? "  + diagnosticOrder.item
//
//        respond diagnosticOrder, model: [nextStatus: nextStatus]
//    }
//
//    def show2(DiagnosticOrder diagnosticOrder) {
//        if (params?.nextStatus) {
//            println "vou fazer set : " + params.nextStatus
//            diagnosticOrder.status = params.nextStatus
//            diagnosticOrder.save(flush:true)
//        }
//        String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrder.status, session.user.role)
//        println "nextStatus -> " + nextStatus
//
//        println "e assim ? "  + diagnosticOrder.item
//
//        respond diagnosticOrder, model: [nextStatus: nextStatus]
//    }

    @Transactional
    def update(DiagnosticOrder diagnosticOrderInstance) {
        String msg
        // TODO NOT ALLOWED IF THE diagnosticOrder is completed...
        if (diagnosticOrderInstance.status == DiagnosticOrder.COMPLETED) {
            msg =  "The workflow is completed"
            println msg
            flash.message = msg
            respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
        }
        switch (session.user.role)  {
            case User.REQUESTER_MEDICAL_DOCTOR:
                println "myParams ; " + params

                String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrder.status, session.user.role)
                println "nextStatus -> " + nextStatus

                respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
                return
            break
            case User.ASSISTANT_MEDICAL_DOCTOR:
                diagnosticOrderInstance.assistantMedicalDoctor = session.user
            break
            case User.MEDICAL_DOCTOR:
                diagnosticOrderInstance.medicalDoctor = session.user
            break
            default:
                msg =  "User has to be correct role to perform any update"
                println "TODO " + msg
                flash.message = msg
                respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
                return
            break;
        }

//        println "myParams ; " + params

//        def diagnosticOrderItemId = params.itemId.split("_").last()
//        println "este é o item id : " + diagnosticOrderItemId
//        DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(diagnosticOrderItemId)

    String separator = "_"
//    String prefix = "nucleousCount"
//params.entrySet().findAll {it.key.startsWith(prefix)}.each {
println "----------------------------------------------------------------"
    println params.entrySet()
println "----------------------------------------------------------------"

List itemsId = params.keySet().findAll {it =~ /^change_[0-9]/}.collect {it.split("_").last()}

itemsId.each { String itemId ->
    DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
    diagnosticOrderItem.change = params["change_"+itemId]

    if (diagnosticOrderItem.change == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
        diagnosticOrderItem.nucleousCount = 0
        diagnosticOrderItem.positiveNucleousCount = 0
        diagnosticOrderItem.percentageNucleousCountPositive = 0
        diagnosticOrderItem.centromerCount = params["centromerCount_"+itemId] as long
        diagnosticOrderItem.locusSpecificProbeCount = params["locusSpecificProbeCount_"+itemId] as long
        diagnosticOrderItem.ratio = params["ratio_"+itemId] as double
    } else {
        diagnosticOrderItem.centromerCount = 0
        diagnosticOrderItem.locusSpecificProbeCount = 0
        diagnosticOrderItem.ratio = 0
        diagnosticOrderItem.nucleousCount = params["nucleousCount_"+itemId] as long
        diagnosticOrderItem.positiveNucleousCount = params["positiveNucleousCount_"+itemId] as long
        diagnosticOrderItem.percentageNucleousCountPositive = params["percentageNucleousCountPositive_"+itemId] as double
    }
    diagnosticOrderItem.percentageNotCountableNucleous = params["percentageNotCountableNucleous_"+itemId] as double
    diagnosticOrderItem.result = params["result_"+itemId]
    diagnosticOrderItem.comment = params["comment_"+itemId]
    diagnosticOrderItem.save()
}


//params.entrySet().each {
//        if (it.key.startsWith("nucleousCount")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" != DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            int value = it.value as int
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.nucleousCount = value
//            diagnosticOrderItem.save()
//                        }
//        } else if (it.key.startsWith("positiveNucleousCount")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" != DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            int value = it.value as int
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.positiveNucleousCount = value
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("change")) {
//            int itemId = it.key.split(separator).last() as int
//            String value = it.value
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.change = value
//            diagnosticOrderItem.save()
//        } else if (it.key.startsWith("percentageNotCountableNucleous")) {
//            int itemId = it.key.split(separator).last() as int
//            int value = it.value as int
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.percentageNotCountableNucleous = value
//            diagnosticOrderItem.save()
//        } else if (it.key.startsWith("comment")) {
//            int itemId = it.key.split(separator).last() as int
//            String value = it.value
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.comment = value
//            diagnosticOrderItem.save()
//        } else if (it.key.startsWith("percentageNucleousCountPositive")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" != DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.percentageNucleousCountPositive = it.value as long
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("centromerCount")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.centromerCount = it.value as long
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("locusSpecificProbeCount")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.locusSpecificProbeCount = it.value as long
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("ratio")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.ratio = it.value as long
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("detected")) {
//            println "detected => '"   + it.value + "'"
//            int itemId = it.key.split(separator).last() as int
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.detected = it.value.toBoolean()
//            println "it.value " + it.value
//            println "it.value as boolean " + (it.value.toBoolean())
//            println "diagnosticOrderItem.detected : " + diagnosticOrderItem.detected
//            diagnosticOrderItem.save()
//        }
//
//   println "param $it.key = $it.value"
//}

//        def res = params as JSON
//        res.prettyPrint = true
//        render res

//        if (diagnosticOrderInstance == null) {
//            notFound()
//            return
//        }

        if (params?.nextStatus) {
            println "vou fazer set : " + params.nextStatus
            diagnosticOrderInstance.status = params.nextStatus
            diagnosticOrderInstance.save(flush:true)
        } else {
            println "No change at status..."
        }

        String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrderInstance.status, session.user.role)
        println "nextStatus -> " + nextStatus

    respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: nextStatus]

//        if (diagnosticOrderInstance.hasErrors()) {
//            respond diagnosticOrderInstance.errors, view:'edit'
//            return
//        }

//        diagnosticOrderInstance.save flush:true
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.updated.message', args: [message(code: 'DiagnosticOrder.label', default: 'DiagnosticOrder'), diagnosticOrderInstance.id])
//                redirect diagnosticOrderInstance
//            }
//            '*'{ respond diagnosticOrderInstance, [status: OK] }
//        }
    }

    @Transactional
    def updateDOI(DiagnosticOrderItem diagnosticOrderItemInstance) {
//        println "object : " + diagnosticOrderInstance

        println " params : " + params
        println "object Item : " + diagnosticOrderItemInstance

        DiagnosticOrder diagnosticOrderInstance = diagnosticOrderItemInstance.diagnosticOrder
        String msg
        // TODO NOT ALLOWED IF THE diagnosticOrder is completed...
        if (diagnosticOrderItemInstance.status == DiagnosticOrderItemStatusEvent.COMPLETED) {
            msg =  "The workflow is completed"
            println msg
            flash.message = msg
            respond diagnosticOrderItemInstance, view:"item", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
        }
        switch (session.user.role)  {
            case User.REQUESTER_MEDICAL_DOCTOR:
                println "myParams ; " + params
                println "estou A!!!!"
                List<String> availableStatus = workflowService.diagnosticOrderItemAvailableStatus(diagnosticOrderInstance.status, session.user.role)
//                String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrder.status, session.user.role)
//                println "nextStatus -> " + nextStatus

                respond diagnosticOrderItemInstance, view:"item", model: [diagnosticOrder: diagnosticOrderInstance, availableStatus: availableStatus]
                return
            break
            case User.ASSISTANT_MEDICAL_DOCTOR:
                diagnosticOrderInstance.assistantMedicalDoctor = session.user
            break
            case User.MEDICAL_DOCTOR:
                diagnosticOrderInstance.medicalDoctor = session.user
            break
            default:
                msg =  "User has to be correct role to perform any update"
                println "TODO " + msg
                flash.message = msg
                respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: diagnosticOrderInstance.status]
                return
            break;
        }

//        println "myParams ; " + params

//        def diagnosticOrderItemId = params.itemId.split("_").last()
//        println "este é o item id : " + diagnosticOrderItemId
//        DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(diagnosticOrderItemId)

    String separator = "_"
//    String prefix = "nucleousCount"
//params.entrySet().findAll {it.key.startsWith(prefix)}.each {
println "----------------------------------------------------------------"
    println params.entrySet()
println "----------------------------------------------------------------"

List itemsId = params.keySet().findAll {it =~ /^change_[0-9]/}.collect {it.split("_").last()}

itemsId.each { String itemId ->
    DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
    diagnosticOrderItem.change = params["change_"+itemId]

    if (diagnosticOrderItem.change == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
        diagnosticOrderItem.nucleousCount = 0
        diagnosticOrderItem.positiveNucleousCount = 0
        diagnosticOrderItem.percentageNucleousCountPositive = 0
        diagnosticOrderItem.centromerCount = params["centromerCount_"+itemId] as long
        diagnosticOrderItem.locusSpecificProbeCount = params["locusSpecificProbeCount_"+itemId] as long
        diagnosticOrderItem.ratio = params["ratio_"+itemId] as double
    } else {
        diagnosticOrderItem.centromerCount = 0
        diagnosticOrderItem.locusSpecificProbeCount = 0
        diagnosticOrderItem.ratio = 0
        diagnosticOrderItem.nucleousCount = params["nucleousCount_"+itemId] as long
        diagnosticOrderItem.positiveNucleousCount = params["positiveNucleousCount_"+itemId] as long
        diagnosticOrderItem.percentageNucleousCountPositive = params["percentageNucleousCountPositive_"+itemId] as double
    }
    diagnosticOrderItem.percentageNotCountableNucleous = params["percentageNotCountableNucleous_"+itemId] as double
    diagnosticOrderItem.result = params["result_"+itemId]
    diagnosticOrderItem.comment = params["comment_"+itemId]
    diagnosticOrderItem.save()
}


//params.entrySet().each {
//        if (it.key.startsWith("nucleousCount")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" != DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            int value = it.value as int
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.nucleousCount = value
//            diagnosticOrderItem.save()
//                        }
//        } else if (it.key.startsWith("positiveNucleousCount")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" != DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            int value = it.value as int
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.positiveNucleousCount = value
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("change")) {
//            int itemId = it.key.split(separator).last() as int
//            String value = it.value
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.change = value
//            diagnosticOrderItem.save()
//        } else if (it.key.startsWith("percentageNotCountableNucleous")) {
//            int itemId = it.key.split(separator).last() as int
//            int value = it.value as int
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.percentageNotCountableNucleous = value
//            diagnosticOrderItem.save()
//        } else if (it.key.startsWith("comment")) {
//            int itemId = it.key.split(separator).last() as int
//            String value = it.value
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.comment = value
//            diagnosticOrderItem.save()
//        } else if (it.key.startsWith("percentageNucleousCountPositive")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" != DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.percentageNucleousCountPositive = it.value as long
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("centromerCount")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.centromerCount = it.value as long
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("locusSpecificProbeCount")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.locusSpecificProbeCount = it.value as long
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("ratio")) {
//            int itemId = it.key.split(separator).last() as int
//            if (params."change_${itemId}" == DiagnosticOrderItem.AMPLIFICATION_CHANGE) {
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.ratio = it.value as long
//            diagnosticOrderItem.save()
//            }
//        } else if (it.key.startsWith("detected")) {
//            println "detected => '"   + it.value + "'"
//            int itemId = it.key.split(separator).last() as int
//            DiagnosticOrderItem diagnosticOrderItem = DiagnosticOrderItem.get(itemId)
//            diagnosticOrderItem.detected = it.value.toBoolean()
//            println "it.value " + it.value
//            println "it.value as boolean " + (it.value.toBoolean())
//            println "diagnosticOrderItem.detected : " + diagnosticOrderItem.detected
//            diagnosticOrderItem.save()
//        }
//
//   println "param $it.key = $it.value"
//}

//        def res = params as JSON
//        res.prettyPrint = true
//        render res

//        if (diagnosticOrderInstance == null) {
//            notFound()
//            return
//        }

        if (params?.nextStatus) {
            println "vou fazer set : " + params.nextStatus
            diagnosticOrderInstance.status = params.nextStatus
            diagnosticOrderInstance.save(flush:true)
        } else {
            println "No change at status..."
        }

        String nextStatus = nextDiagnosticStatusUpdateAvailable(diagnosticOrderInstance.status, session.user.role)
        println "nextStatus -> " + nextStatus

    respond diagnosticOrderInstance, view:"show", model: [diagnosticOrder: diagnosticOrderInstance, nextStatus: nextStatus]

//        if (diagnosticOrderInstance.hasErrors()) {
//            respond diagnosticOrderInstance.errors, view:'edit'
//            return
//        }

//        diagnosticOrderInstance.save flush:true
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.updated.message', args: [message(code: 'DiagnosticOrder.label', default: 'DiagnosticOrder'), diagnosticOrderInstance.id])
//                redirect diagnosticOrderInstance
//            }
//            '*'{ respond diagnosticOrderInstance, [status: OK] }
//        }
    }

    @Transactional
    def delete() {
        def diagnosticOrder = DiagnosticOrder.get( params.id )
        println "Deleting DiagnosticOrder : " + params.id
        if (diagnosticOrder) {
            try {

//            def res = diagnosticOrder.events*.delete()
//            diagnosticOrder.events.clear()

//            println "res : " + res

            boolean allFilesSuccessfullyDeleted = true

            diagnosticOrder.items.each { DiagnosticOrderItem diagnosticOrderItem ->

//events: DiagnosticOrderStatusEvent
//events: DiagnosticOrderItemStatusEvent

//                diagnosticOrderItem.events*.delete()
//                diagnosticOrderItem.events.clear()

                    diagnosticOrderItem.uploadedFiles.each { DiagnosticOrderItemUploadedFile uploadedFile ->
                        println "file: " + uploadedFile.filepath
                        File fileToDelete = new File(uploadedFile.filepath)
                        if (fileToDelete.exists()) {
                            allFilesSuccessfullyDeleted = allFilesSuccessfullyDeleted && (fileToDelete.delete())
                        }
                    }
                }
                if (allFilesSuccessfullyDeleted) {
                        diagnosticOrder.delete()
                        println "funca o delte??!"
                        flash.message = "Diagnostic Order ${params.id} deleted"
                        redirect(action:"index")
                }
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Diagnostic Order ${params.id} could not be deleted"
                redirect(action:"show",id:params.id)
            }
        }
        else {
            flash.message = "Diagnostic Order with id ${params.id} not found."
            redirect(action:"list")
        }
    }


//    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
//
//    def index(Integer max) {
//        params.max = Math.min(max ?: 10, 100)
//        respond DiagnosticOrder.list(params), model:[diagnosticOrderInstanceCount: DiagnosticOrder.count()]
//    }
//
//    def show(DiagnosticOrder diagnosticOrderInstance) {
//        respond diagnosticOrderInstance
//    }
//
//    def create() {
//        respond new DiagnosticOrder(params)
//    }
//
//    @Transactional
//    def save(DiagnosticOrder diagnosticOrderInstance) {
//        if (diagnosticOrderInstance == null) {
//            notFound()
//            return
//        }
//
//        if (diagnosticOrderInstance.hasErrors()) {
//            respond diagnosticOrderInstance.errors, view:'create'
//            return
//        }
//
//        diagnosticOrderInstance.save flush:true
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.created.message', args: [message(code: 'diagnosticOrder.label', default: 'DiagnosticOrder'), diagnosticOrderInstance.id])
//                redirect diagnosticOrderInstance
//            }
//            '*' { respond diagnosticOrderInstance, [status: CREATED] }
//        }
//    }
//
//    def edit(DiagnosticOrder diagnosticOrderInstance) {
//        respond diagnosticOrderInstance
//    }
//
//    @Transactional
//    def update(DiagnosticOrder diagnosticOrderInstance) {
//        if (diagnosticOrderInstance == null) {
//            notFound()
//            return
//        }
//
//        if (diagnosticOrderInstance.hasErrors()) {
//            respond diagnosticOrderInstance.errors, view:'edit'
//            return
//        }
//
//        diagnosticOrderInstance.save flush:true
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.updated.message', args: [message(code: 'DiagnosticOrder.label', default: 'DiagnosticOrder'), diagnosticOrderInstance.id])
//                redirect diagnosticOrderInstance
//            }
//            '*'{ respond diagnosticOrderInstance, [status: OK] }
//        }
//    }
//
//    @Transactional
//    def delete(DiagnosticOrder diagnosticOrderInstance) {
//
//        if (diagnosticOrderInstance == null) {
//            notFound()
//            return
//        }
//
//        diagnosticOrderInstance.delete flush:true
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DiagnosticOrder.label', default: 'DiagnosticOrder'), diagnosticOrderInstance.id])
//                redirect action:"index", method:"GET"
//            }
//            '*'{ render status: NO_CONTENT }
//        }
//    }

//    @Transactional
//    def delete() {
//        def userInstance = DiagnosticOrder.get( params.id )
//        if(userInstance) {
//            try {
//                userInstance.delete()
//                flash.message = "User ${params.id} deleted"
//                redirect(action:"list")
//            }
//            catch(org.springframework.dao.DataIntegrityViolationException e) {
//                flash.message = "User ${params.id} could not be deleted"
//                redirect(action:"show",id:params.id)
//            }
//        }
//        else {
//            flash.message = "User not found with id ${params.id}"
//            redirect(action:"list")
//        }
//    }


//
//    protected void notFound() {
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.not.found.message', args: [message(code: 'diagnosticOrder.label', default: 'DiagnosticOrder'), params.id])
//                redirect action: "index", method: "GET"
//            }
//            '*'{ render status: NOT_FOUND }
//        }
//    }
}
