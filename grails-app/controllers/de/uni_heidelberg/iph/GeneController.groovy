package de.uni_heidelberg.iph

class GeneController {

    static scaffold = true

//    def beforeInterceptor = [action: this.&auth, except: 'login']
    def beforeInterceptor = [action: this.&auth]

    // defined with private scope, so it's not considered an action
  private auth() {
    if (![User.ADMINISTRATOR, User.MANAGER].contains(session?.user?.role)) {
      flash.message = "You must be an administrator to perform that task."
      redirect(action:"entry", controller: "user")
      return false
    }
  }
}
